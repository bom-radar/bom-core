/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "colour_map.h"
#include <bom/math_utils.h>
#include <bom/trace.h>
#include <bom/unit_test.h>
#include <algorithm>

using namespace bom;

BOM_DEFINE_ENUM_TRAITS(colour_map::interpolation_mode);

colour_map::colour_map()
  : missing_{standard_colours::clear}
  , underflow_{standard_colours::clear}
  , overflow_{standard_colours::clear}
  , table_{2}
  , mode_{interpolation_mode::gradient}
{
  table_[0] = {0.0f, standard_colours::black};
  table_[1] = {1.0f, standard_colours::white};
}

colour_map::colour_map(io::configuration const& config)
  : missing_{config["missing"]}
  , underflow_{config["underflow"]}
  , overflow_{config["overflow"]}
  , mode_{config["mode"]}
{
  auto& config_colours = config["colours"];
  table_.resize(config_colours.size());
  if (config_colours.type() == io::configuration::node_type::array)
  {
    float min = config["value_min"];
    float max = config["value_max"];
    float delta = (max - min) / (mode_ == interpolation_mode::gradient ? table_.size() - 1 : table_.size());
    auto& vals = config_colours.array();
    for (size_t i = 0; i < table_.size(); ++i)
    {
      table_[i].first = min + i * delta;
      table_[i].second = from_string<colour>(vals[i]);
    }
  }
  else
  {
    auto& vals = config_colours.object();
    for (size_t i = 0; i < table_.size(); ++i)
    {
      table_[i].first = from_string<float>(vals[i].first);
      table_[i].second = from_string<colour>(vals[i].second);
    }

    // for a thresholded table the top color is redundant to the overflow colour
    // if they differ issue a warning and prefer the explicit overflow colour
    if (mode_ == interpolation_mode::thresholds && table_[table_.size() - 1].second != overflow_)
    {
      trace::warning(
            "overflow colour of '{}' does not match top level in colour table '{}' (impossible to observe)"
          , overflow_
          , table_[table_.size() - 1].second);
    }
  }

  if (table_.empty())
    throw std::runtime_error{"Insufficient entries in colour table"};
}

auto colour_map::evaluate(float value) const -> colour
{
  if (is_nan(value))
    return missing_;

  auto i = std::upper_bound(table_.begin(), table_.end(), value, [](float l, pair<float, colour> const& r) { return l < r.first; });
  if (i == table_.begin())
    return underflow_;

  if (mode_ == interpolation_mode::gradient)
  {
    if (i == table_.end())
      return value == (i - 1)->first ? (i - 1)->second : overflow_;

    return colour::lerp_rgb((i - 1)->second, i->second, (value - (i - 1)->first) / (i->first - (i - 1)->first));
  }

  return (i - 1)->second;
}

// LCOV_EXCL_START
TEST_CASE("colour_map")
{
  SUBCASE("basic")
  {
    auto cm  = colour_map{};

    CHECK(cm.missing() == standard_colours::clear);
    CHECK(cm.underflow() == standard_colours::clear);
    CHECK(cm.overflow() == standard_colours::clear);
    CHECK(cm.colours().size() == 2);
    CHECK(cm.mode() == colour_map::interpolation_mode::gradient);
  }
}
// LCOV_EXCL_STOP

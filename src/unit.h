/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2017 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <bom/span.h>
#include <bom/string_utils.h>

namespace bom
{
  /// Unit representation used to transform between various related quantities
  /** This class is a wrapper around the UDUNITS2 library, so instanciating it is not necessarily a light weight
   *  operation.  Upon the first construction of a unit object the XML unit database used by UDUNITS will be loaded
   *  into memory.
   *
   *  It is strongly advised that you only instanciate this class if you know you will want to perform transforms
   *  with the unit.  Unit metadata is probably best captured using a string which is only used to create a unit
   *  object when tranformation capabilities are needed. */
  class unit
  {
  public:
    class transform;

  public:
    /// Initialize a new unit
    explicit unit(string definition);

    unit(unit const& rhs);
    unit(unit&& rhs) noexcept;

    auto operator=(unit const& rhs) -> unit&;
    auto operator=(unit&& rhs) noexcept -> unit&;

    ~unit();

  private:
    string  definition_;
    void*   handle_;
  };

  /// Object used to transform values from one unit to another
  /** It is safe to create a transform from temporary units.  Once created the transform object no longer knows
   *  or cares about the original units that were used to initialize it. */
  class unit::transform
  {
  public:
    transform(unit const& from, unit const& to);

    transform(transform const& rhs) = delete;
    transform(transform&& rhs) noexcept;

    auto operator=(transform const& rhs) -> transform& = delete;
    auto operator=(transform&& rhs) noexcept -> transform&;

    ~transform();

    auto operator()(float val) const -> float;
    auto operator()(double val) const -> double;

    auto operator()(span<float> vals) const -> void;
    auto operator()(span<double> vals) const -> void;

  private:
    void* handle_;
  };
}

/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2017 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "unit.h"
#include <bom/unit_test.h>
#include <udunits2.h>
#include <mutex>

using namespace bom;

// we rely on UT_SUCCESS being 0 to allow us to use the convenient "if (auto status = ut_xxx())" syntax
static_assert(UT_SUCCESS == 0, "expected UT_SUCCESS to be defined as 0!");

using system_raii = std::unique_ptr<ut_system, void(*)(ut_system*)>;

/* UDUNITS requires external synchronization of all calls.  Before you go checking again, it is still not thread
 * safe even if you create multiple ut_systems as contexts and use each one in a different thread.  Under the
 * hood many functions use globals - such as a global status variable.  Last checked at UDUNITS 2.2.25. */
static std::mutex   mut_udunits;
static system_raii  ut_sys{nullptr, ut_free_system};
static string       ut_err;

static auto custom_error_handler(char const* fmt, va_list args) -> int
{
  char buf[512];
  if (vsnprintf(buf, 512, fmt, args) != -1)
    ut_err.assign(buf);
  return 0;
}

static auto get_and_clear_error() -> string
{
  auto ret = ut_err;
  ut_err.clear();
  return ret;
}

unit::unit(string definition)
  : definition_{std::move(definition)}
{
  std::unique_lock<std::mutex> lock{mut_udunits};

  // initialize the library
  if (!ut_sys)
  {
    // setup the error handler - we don't want it spewing errors everywhere
    ut_set_error_message_handler(custom_error_handler);

    // load up the units library
    // use default database, but users can still overwrite with UDUNITS2_XML_PATH environment variable
    ut_sys = system_raii{ut_read_xml(nullptr), ut_free_system};
    if (!ut_sys)
      throw std::runtime_error{fmt::format("failed to initialize UDUNITS: {}", get_and_clear_error())};
  }

  // initialize the unit
  handle_ = ut_parse(ut_sys.get(), definition_.c_str(), UT_UTF8);
  if (!handle_)
    throw std::runtime_error{fmt::format("failed to initialize unit '{}': {}", definition_, get_and_clear_error())};
}

unit::unit(unit const& rhs)
  : definition_{rhs.definition_}
{
  std::unique_lock<std::mutex> lock{mut_udunits};

  handle_ = ut_clone(reinterpret_cast<ut_unit*>(rhs.handle_));
  if (!handle_)
    throw std::runtime_error{fmt::format("failed to clone unit '{}': {}", definition_, get_and_clear_error())};
}

unit::unit(unit&& rhs) noexcept
  : definition_{std::move(rhs.definition_)}
  , handle_{rhs.handle_}
{
  rhs.handle_ = nullptr;
}

auto unit::operator=(unit const& rhs) -> unit&
{
  auto copy{rhs};
  std::swap(definition_, copy.definition_);
  std::swap(handle_, copy.handle_);
  return *this;
}

auto unit::operator=(unit&& rhs) noexcept -> unit&
{
  std::swap(definition_, rhs.definition_);
  std::swap(handle_, rhs.handle_);
  return *this;
}

unit::~unit()
{
  std::unique_lock<std::mutex> lock{mut_udunits};
  ut_free(reinterpret_cast<ut_unit*>(handle_));
}

unit::transform::transform(unit const& from, unit const& to)
{
  std::unique_lock<std::mutex> lock{mut_udunits};

  handle_ = ut_get_converter(static_cast<ut_unit*>(from.handle_), static_cast<ut_unit*>(to.handle_));
  if (!handle_)
    throw std::runtime_error{fmt::format(
              "failed to build transform from '{}' to '{}': {}"
            , from.definition_
            , to.definition_
            , get_and_clear_error())};
}

unit::transform::transform(transform&& rhs) noexcept
  : handle_{rhs.handle_}
{
  rhs.handle_ = nullptr;
}

auto unit::transform::operator=(transform&& rhs) noexcept -> transform&
{
  std::swap(handle_, rhs.handle_);
  return *this;
}

unit::transform::~transform()
{
  std::unique_lock<std::mutex> lock{mut_udunits};
  cv_free(reinterpret_cast<cv_converter*>(handle_));
}

auto unit::transform::operator()(float val) const -> float
{
  std::unique_lock<std::mutex> lock{mut_udunits};
  return cv_convert_float(reinterpret_cast<cv_converter*>(handle_), val);
}

auto unit::transform::operator()(double val) const -> double
{
  std::unique_lock<std::mutex> lock{mut_udunits};
  return cv_convert_double(reinterpret_cast<cv_converter*>(handle_), val);
}

auto unit::transform::operator()(span<float> vals) const -> void
{
  std::unique_lock<std::mutex> lock{mut_udunits};
  cv_convert_floats(reinterpret_cast<cv_converter*>(handle_), vals.data(), vals.size(), vals.data());
}

auto unit::transform::operator()(span<double> vals) const -> void
{
  std::unique_lock<std::mutex> lock{mut_udunits};
  cv_convert_doubles(reinterpret_cast<cv_converter*>(handle_), vals.data(), vals.size(), vals.data());
}

// LCOV_EXCL_START
TEST_CASE("unit")
{
  // fail with a bad udunits xml path
  setenv("UDUNITS2_XML_PATH", "/bad/path", 1);
  CHECK_THROWS(unit{"m"s});
  unsetenv("UDUNITS2_XML_PATH");

  // fail with bad unit spec
  CHECK_THROWS(unit{"badbad"s});

  // basic construction
  auto u1 = unit{"m"s};
  auto u2 = unit{"km"s};
  auto u3 = unit{"s"s};
  auto u4 = unit{"hours"s};

  // move and copy
  auto u_m = std::move(u1);
  auto u_km = u2;
  auto u_s = u2;
  auto u_hr = u2;
  u_s = std::move(u3);
  u_hr = u4;

  // bad transform (incompatible units)
  CHECK_THROWS(unit::transform(u_m, u_hr));

  // basic construction
  auto xf1 = unit::transform{u_m, u_km};
  auto xf2 = unit::transform{u_km, u_m};
  auto xf3 = unit::transform{u_s, u_hr};
  auto xf4 = unit::transform{u_hr, u_s};

  // move and copy
  auto m_to_km = std::move(xf1);
  auto km_to_m = std::move(xf2);
  auto s_to_hr = std::move(xf3);
  auto hr_to_s = unit::transform{u_m, u_km};
  hr_to_s = std::move(xf4);

  // scalar conversions
  CHECK(m_to_km(1000.0) == approx(1.0));
  CHECK(km_to_m(5.0f) == approx(5000.0f));
  CHECK(s_to_hr(3600.0) == approx(1.0));
  CHECK(hr_to_s(5.0f) == approx(18000.0f));

  // span conversions
  std::array<float, 5> data{1.0f, 10.0f, 100.0f, 1000.0f, 10000.0f};
  CHECK_NOTHROW(m_to_km(make_span(data)));
  CHECK(data[0] == approx(0.001f));
  CHECK(data[1] == approx(0.010f));
  CHECK(data[2] == approx(0.100f));
  CHECK(data[3] == approx(1.000f));
  CHECK(data[4] == approx(10.00f));

  std::array<double, 3> data2{1.0, 2.0, 24.0};
  CHECK_NOTHROW(hr_to_s(make_span(data2)));
  CHECK(data2[0] == approx(3600.0));
  CHECK(data2[1] == approx(7200.0));
  CHECK(data2[2] == approx(86400.0));
}
// LCOV_EXCL_STOP

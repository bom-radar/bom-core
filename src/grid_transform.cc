/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "grid_transform.h"
#include "unit.h"
#include <bom/ellipsoid.h>
#include <bom/file_utils.h>
#include <bom/trace.h>

using namespace bom;

BOM_DEFINE_ENUM_TRAITS(grid_transform::merge_policy);

// magic value output at start of file to verify identity
constexpr size_t lut_magic = 0x616e63677474626c;

// maximum memory to use for tranforming coordinates at a time
constexpr size_t max_buffer = 32 * 1024 * 1024;

namespace
{
  struct lut_header
  {
    size_t magic;
    size_t uid_size;
    size_t lut_size;
    size_t entry_size;
  };
}

grid_transform::grid_transform(
      map_projection const& from_proj
    , grid_coordinates const& from_coords
    , map_projection const& to_proj
    , grid_coordinates const& to_coords
    , int subsamples
    , string const& cache_dir_path)
  : size_from_{from_coords.size()}
  , size_to_{to_coords.size()}
{
  // use the fastmesh algorithm?
  if (subsamples < 0)
  {
    build_fastmesh(from_proj, from_coords, to_proj, to_coords, std::abs(subsamples));
    return;
  }

  // short circuit the cache stuff if we don't want to use it
  if (cache_dir_path.empty())
  {
    if (subsamples == 0)
      build_lut_bilinear(from_proj, from_coords, to_proj, to_coords);
    else
      build_lut_subsample(from_proj, from_coords, to_proj, to_coords, subsamples);
  }
  else
  {
    // build a unique identifier string for the transformation
    // the string _must_ fully uniquely identify the transform!
    auto uid = fmt::format(
          "{} {} {} {} {} {} {} {} {} {} {} {} {} {} {}"
        , lut_magic
        , map_projection::proj_version()
        , from_proj.definition()
        , from_coords.size()
        , from_coords.bounds().min
        , from_coords.bounds().max
        , from_coords.col_units()
        , from_coords.row_units()
        , to_proj.definition()
        , to_coords.size()
        , to_coords.bounds().min
        , to_coords.bounds().max
        , to_coords.col_units()
        , to_coords.row_units()
        , subsamples);

    // try to load an existing lookup table from disk
    auto res = load_lut(uid, cache_dir_path);
    if (!res.first)
    {
      if (subsamples == 0)
        build_lut_bilinear(from_proj, from_coords, to_proj, to_coords);
      else
        build_lut_subsample(from_proj, from_coords, to_proj, to_coords, subsamples);
      save_lut(uid, res.second);
    }
  }
}

auto grid_transform::determine_limits(
      map_projection const& from_proj
    , grid_coordinates const& from_coords
    , map_projection const& to_proj
    , grid_coordinates const& to_coords
    ) const -> box2z
{
  // pre-transform the 'from' grid coordinates to the units required by the 'from' projection
  array1d from_row_edges{from_coords.row_edges()};
  if (from_coords.row_units() != from_proj.units())
    unit::transform{unit{from_coords.row_units()}, unit{from_proj.units()}}(from_row_edges);

  array1d from_col_edges{from_coords.col_edges()};
  if (from_coords.col_units() != from_proj.units())
    unit::transform{unit{from_coords.col_units()}, unit{from_proj.units()}}(from_col_edges);

  // build an array of coordinates that represent the outline of our 'from' grid (using actual edges not centers)
  auto count = from_row_edges.size() * 2 + from_col_edges.size() * 2;
  array1d cy{count}, cx{count};
  auto iout = 0;
  for (size_t i = 0; i < from_col_edges.size(); ++i, ++iout) // top row
    cy[iout] = from_row_edges[0], cx[iout] = from_col_edges[i];
  for (size_t i = 0; i < from_row_edges.size(); ++i, ++iout) // left column
    cy[iout] = from_row_edges[i], cx[iout] = from_col_edges[0];
  for (size_t i = 0; i < from_row_edges.size(); ++i, ++iout) // right column
    cy[iout] = from_row_edges[i], cx[iout] = from_col_edges[from_col_edges.size() - 1];
  for (size_t i = 0; i < from_col_edges.size(); ++i, ++iout) // bottom row
    cy[iout] = from_row_edges[from_row_edges.size() - 1], cx[iout] = from_col_edges[i];

  // transform the coordinates into the 'to' domain
  map_projection::transform(from_proj, to_proj, cx, cy);

  // transform the coordinates into the units required by the 'to' grid
  if (to_coords.row_units() != to_proj.units())
    unit::transform{unit{to_proj.units()}, unit{to_coords.row_units()}}(cy);
  if (to_coords.col_units() != to_proj.units())
    unit::transform{unit{to_proj.units()}, unit{to_coords.col_units()}}(cx);

  // determine a bounding box of our 'from' subset in the native 'to' grid coordinates
  // note: edge arrays contain size().x/y + 1 entries, so no need for a -1 in max indexing below
  auto bbox_subset = box2d::empty_box();
  for (size_t i = 0; i < cy.size(); ++i)
    bbox_subset.expand(vec2d{cx[i], cy[i]});

  // make a bounding box for the entire 'to' grid (used for comparison to our subset)
  auto bbox_full = box2d::empty_box();
  bbox_full.expand(vec2d{to_coords.col_edges()[0], to_coords.row_edges()[0]});
  bbox_full.expand(vec2d{to_coords.col_edges()[to_coords.size().x], to_coords.row_edges()[to_coords.size().y]});

  // is the subset entirely outside the 'to' grid?
  if (   bbox_subset.max.x < bbox_full.min.x || bbox_subset.min.x > bbox_full.max.x
      || bbox_subset.max.y < bbox_full.min.y || bbox_subset.min.y > bbox_full.max.y)
    return { to_coords.size(), to_coords.size() };

  // clamp the min/max coordiantes to the 'to' grid bounds
  // clamping to cell centers here to be doubly sure that cell_containing gives the correct result (no precision problems)
  if (to_coords.ascending().x)
  {
    bbox_subset.min.x = std::max(bbox_subset.min.x, 0.5 * (to_coords.col_edges()[0] + to_coords.col_edges()[1]));
    bbox_subset.max.x = std::min(bbox_subset.max.x, 0.5 * (to_coords.col_edges()[to_coords.size().x-1] + to_coords.col_edges()[to_coords.size().x]));
  }
  else
  {
    bbox_subset.min.x = std::max(bbox_subset.min.x, 0.5 * (to_coords.col_edges()[to_coords.size().x-1] + to_coords.col_edges()[to_coords.size().x]));
    bbox_subset.max.x = std::min(bbox_subset.max.x, 0.5 * (to_coords.col_edges()[0] + to_coords.col_edges()[1]));
  }
  if (to_coords.ascending().y)
  {
    bbox_subset.min.y = std::max(bbox_subset.min.y, 0.5 * (to_coords.row_edges()[0] + to_coords.row_edges()[1]));
    bbox_subset.max.y = std::min(bbox_subset.max.y, 0.5 * (to_coords.row_edges()[to_coords.size().y-1] + to_coords.row_edges()[to_coords.size().y]));
  }
  else
  {
    bbox_subset.min.y = std::max(bbox_subset.min.y, 0.5 * (to_coords.row_edges()[to_coords.size().y-1] + to_coords.row_edges()[to_coords.size().y]));
    bbox_subset.max.y = std::min(bbox_subset.max.y, 0.5 * (to_coords.row_edges()[0] + to_coords.row_edges()[1]));
  }

  // convert coordinates to cell indexes
  auto bbox_cells = box2z::empty_box();
  bbox_cells.expand(to_coords.cell_containing(bbox_subset.min));
  bbox_cells.expand(to_coords.cell_containing(bbox_subset.max));

  // +1 to the max coordinate since we treat it like an '.end()'
  bbox_cells.max.x++;
  bbox_cells.max.y++;

  return bbox_cells;
}

auto grid_transform::build_lut_bilinear(
      map_projection const& from_proj
    , grid_coordinates const& from_coords
    , map_projection const& to_proj
    , grid_coordinates const& to_coords
    ) -> void
{
  #if 0 // Can't enable this check because it fails due to 'degrees_north' != 'degrees_east'
  // sanity check
  if (to_coords.row_units() != to_coords.col_units() || from_coords.row_units() != from_coords.col_units())
  {
    throw std::runtime_error{"grid transform requires identical row and col units"};
  }
  #endif

  auto& lut = xform_.emplace<lutdata>();

  // calculate 'from' coordinates for center of each cell in 'to' grid
  auto from_xy = array2<vec2d>{size_to_};
  for (size_t row = 0; row < size_to_.y; ++row)
    for (size_t col = 0; col < size_to_.x; ++col)
      from_xy[row][col] = to_coords.center_of_cell(vec2z{col, row});
  if (to_coords.row_units() != to_proj.units())
    unit::transform{unit{to_coords.row_units()}, unit{to_proj.units()}}(span{reinterpret_cast<double*>(from_xy.data()), long(from_xy.size() * 2)});
  map_projection::transform(to_proj, from_proj, from_xy);
  if (from_coords.row_units() != from_proj.units())
    unit::transform{unit{from_proj.units()}, unit{from_coords.row_units()}}(span{reinterpret_cast<double*>(from_xy.data()), long(from_xy.size() * 2)});

  // save a few of the early reallocations of the vector
  lut.reserve(1024);

  // build the LUT
  size_t skip = 0;
  for (size_t row = 0; row < size_to_.y; row++)
  {
    for (size_t col = 0; col < size_to_.x; ++col)
    {
      // target point in 'from' coordinates
      auto xy = from_xy[row][col];

      // skip cells that are not contained by 'from' grid
      if (!from_coords.contains(xy))
      {
        ++skip;
        continue;
      }

      // determine the cell in 'from' grid that contains target point
      auto cellc = from_coords.cell_containing(xy);

      // sanity check (not technically needed - but cheap)
      if (cellc.x >= size_from_.x || cellc.y >= size_from_.y)
      {
        ++skip;
        continue;
      }

      // get center of containing cell in 'from' coordinates
      auto centc = from_coords.center_of_cell(cellc);

      // identify the 'neighboring cell' in the 'from' grid (diagnoal offset)
      /* this works by checking if the containing cell center and each edge are on the same side of x.  if they are on
       * different sides (different result of less than comparison) then we know the target point is on that side. */
      auto celln = cellc;
      if (celln.x > 0 && (xy.x < centc.x) != (xy.x < from_coords.col_edges()[cellc.x]))
        celln.x--;
      else if (celln.x < size_from_.x - 1 && (xy.x < centc.x) != (xy.x < from_coords.col_edges()[cellc.x+1]))
        celln.x++;
      if (celln.y > 0 && (xy.y < centc.y) != (xy.y < from_coords.row_edges()[cellc.y]))
        celln.y--;
      else if (celln.y < size_from_.y - 1 && (xy.y < centc.y) != (xy.y < from_coords.row_edges()[cellc.y+1]))
        celln.y++;

      // if we've previously skipped some cells we need to output the RLE entry now
      if (skip > 0)
      {
        lut.push_back({0, 0, skip});
        skip = 0;
      }

      // get center of 'neighboring cell' in 'from' coordinates
      auto centn = from_coords.center_of_cell(celln);

      // determine bilinear interpolation weights
      if (celln.x != cellc.x && celln.y != cellc.y)
      {
        // bilinear interpolation
        auto fx = (xy.x - centc.x) / (centn.x - centc.x);
        auto fy = (xy.y - centc.y) / (centn.y - centc.y);
        auto wcc = unsigned(std::clamp(int((1.0 - fx) * (1.0 - fy) * 128), 0, 128));
        auto wnc = unsigned(std::clamp(int(fx * (1.0 - fy) * 128), 0, 128));
        auto wcn = unsigned(std::clamp(int((1.0 - fx) * fy * 128), 0, 128));
        auto wnn = unsigned(std::clamp(int(fx * fy * 128), 0, 128));
        if (wcc > 0)
          lut.push_back({wcc, 0, cellc.y * size_from_.x + cellc.x});
        if (wnc > 0)
          lut.push_back({wnc, 0, cellc.y * size_from_.x + celln.x});
        if (wcn > 0)
          lut.push_back({wcn, 0, celln.y * size_from_.x + cellc.x});
        if (wnn > 0)
          lut.push_back({wnn, 0, celln.y * size_from_.x + celln.x});
        lut.back().final = 1;
      }
      else if (celln.x != cellc.x)
      {
        // at top or bottom edge - linearly interpolate in x
        auto fx = (xy.x - centc.x) / (centn.x - centc.x);
        auto wc = unsigned(std::clamp(int((1.0 - fx) * 128), 0, 128));
        auto wn = unsigned(std::clamp(int(fx * 128), 0, 128));
        if (wc > 0)
          lut.push_back({wc, 0, cellc.y * size_from_.x + cellc.x});
        if (wn > 0)
          lut.push_back({wn, 0, cellc.y * size_from_.x + celln.x});
        lut.back().final = 1;
      }
      else if (celln.y != cellc.y)
      {
        // at left or right edge - linearly interpolate in y
        auto fy = (xy.y - centc.y) / (centn.y - centc.y);
        auto wc = unsigned(std::clamp(int((1.0 - fy) * 128), 0, 128));
        auto wn = unsigned(std::clamp(int(fy * 128), 0, 128));
        if (wc > 0)
          lut.push_back({wc, 0, cellc.y * size_from_.x + cellc.x});
        if (wn > 0)
          lut.push_back({wn, 0, celln.y * size_from_.x + cellc.x});
        lut.back().final = 1;
      }
      else
      {
        // corner or degenerate case - output containing cell directly
        lut.push_back({1, 1, cellc.y * size_from_.x + cellc.x});
      }
    }
  }

  // if we've skipped some cells, output a skip entry to the LUT
  if (skip > 0)
    lut.push_back({0, 0, skip});
}

auto grid_transform::build_lut_subsample(
      map_projection const& from_proj
    , grid_coordinates const& from_coords
    , map_projection const& to_proj
    , grid_coordinates const& to_coords
    , size_t subsamples
    ) -> void
{
  // sanity check
  if (subsamples > 1 && (!from_coords.regular() || !to_coords.regular()))
    throw std::runtime_error{"grid tranform subsampling not supported on irregular grids"};

  auto& lut = xform_.emplace<lutdata>();

  // pre-transform the 'to' grid coordinates to the units required by the 'to' projection
  array1d to_row_edges{to_coords.row_edges()};
  if (to_coords.row_units() != to_proj.units())
    unit::transform{unit{to_coords.row_units()}, unit{to_proj.units()}}(to_row_edges);

  array1d to_col_edges{to_coords.col_edges()};
  if (to_coords.col_units() != to_proj.units())
    unit::transform{unit{to_coords.col_units()}, unit{to_proj.units()}}(to_col_edges);

  // create transforms to convert the 'from' projection coordinates to the units required by the 'from' grid
  optional<unit::transform> ut_from_y;
  if (from_proj.units() != from_coords.row_units())
    ut_from_y.emplace(unit{from_proj.units()}, unit{from_coords.row_units()});

  optional<unit::transform> ut_from_x;
  if (from_proj.units() != from_coords.col_units())
    ut_from_x.emplace(unit{from_proj.units()}, unit{from_coords.col_units()});

  // generate sub-sample offsets
  auto row_delta = to_row_edges[1] - to_row_edges[0];
  auto col_delta = to_col_edges[1] - to_col_edges[0];
  auto sub_deltas = array1d{subsamples * 2};
  for (size_t i = 0; i < subsamples; ++i)
    sub_deltas[i] = (i + 1) * row_delta / (subsamples + 1);
  for (size_t i = 0; i < subsamples; ++i)
    sub_deltas[i + subsamples] = (i + 1) * col_delta / (subsamples + 1);
  auto rds = sub_deltas.data();
  auto cds = sub_deltas.data() + subsamples;

  // determine the limits of the 'to' grid that we actually need to convert
  /* this allows us to optimize for the case where the 'from' domain only maps to a very small subset of the
   * 'to' domain - such as when mosaicing */
  auto limits = determine_limits(from_proj, from_coords, to_proj, to_coords);
  trace::debug("projection limits are {} to {}", limits.min, limits.max);

  // handle non-overlapping domains by creating a null LUT that just skips the grid
  if (limits.max.x <= limits.min.x || limits.max.y <= limits.min.y)
  {
    trace::debug("null lut generated");
    lut.push_back({0, 0, size_to_.y * size_to_.x});
    return;
  }

  auto size_subset = vec2z{limits.max.x - limits.min.x, limits.max.y - limits.min.y};
  auto row_samples = subsamples * subsamples * size_subset.x;
  auto chunk_delta = max_buffer / (sizeof(lut_entry) * row_samples);

  // save a few of the early reallocations of the vector
  lut.reserve(1024);
  array1d coords_y{chunk_delta * row_samples};
  array1d coords_x{chunk_delta * row_samples};

  // add RLE entries to the LUT to skip until we hit first row of subset (i.e. chunk_start)
  if (limits.min.y > 0)
    lut.push_back({0, 0, limits.min.y * size_to_.x});

  // do the processing in chunks of this many lines
  for (size_t chunk_start = limits.min.y; chunk_start < limits.max.y; chunk_start += chunk_delta)
  {
    auto chunk_end = std::min(chunk_start + chunk_delta, limits.max.y);

    // collect all the coordinates for this chunk
    size_t crd = 0;
    for (size_t row = chunk_start; row < chunk_end; ++row)
    {
      for (size_t col = limits.min.x; col < limits.max.x; ++col)
      {
        for (size_t i = 0; i < subsamples; ++i)
        {
          for (size_t j = 0; j < subsamples; ++j)
          {
            coords_y[crd] = to_row_edges[row] + rds[i];
            coords_x[crd] = to_col_edges[col] + cds[j];
            ++crd;
          }
        }
      }
    }

    // for convenience
    auto span_y = make_span(coords_y.data(), crd);
    auto span_x = make_span(coords_x.data(), crd);

    // use the projection to transform all the coordinates
    map_projection::transform(to_proj, from_proj, span_x, span_y);

    // transform the coordinates into the units required by the 'from' grid
    if (ut_from_y)
      (*ut_from_y)(span_y);
    if (ut_from_x)
      (*ut_from_x)(span_x);

    // convert the coordinates into lookup table entries
    crd = 0;
    for (size_t row = chunk_start; row < chunk_end; ++row)
    {
      // insert RLE entries to the LUT to skip until start of row
      if (limits.min.x > 0)
        lut.push_back({0, 0, limits.min.x});

      for (size_t col = limits.min.x; col < limits.max.x; ++col)
      {
        // create lut entries for each distinct 'from' pixel that applies
        auto lut_start = lut.size();
        for (size_t i = 0; i < subsamples * subsamples; ++i)
        {
          auto c = vec2d{coords_x[crd], coords_y[crd]};
          ++crd;

          if (!from_coords.contains(c))
            continue;

          auto cell = from_coords.cell_containing(c);
          auto index = cell.y * size_from_.x + cell.x;

          // have we already seen this index?
          for (size_t j = lut_start; j < lut.size(); ++j)
          {
            if (lut[j].index == index)
            {
              lut[j].weight++;
              goto index_found;
            }
          }

          // is this a new index
          lut.push_back({1, 0, index});

        index_found:
          continue;
        }

        if (lut_start == lut.size())
        {
          // no valid entries, start or extend an RLE entry
          if (lut.back().weight == 0)
            lut.back().index++;
          else
            lut.push_back({0, 0, 1});
        }
        else
        {
          // finalize the pixel
          lut.back().final = 1;
        }
      }

      // insert RLE entries to the LUT to skip until end of row
      if (limits.max.x < size_to_.x)
        lut.push_back({0, 0, size_to_.x - limits.max.x});
    }
  }

  // add RLE entries to the LUT to skip until end of grid
  if (limits.max.y < size_to_.y)
    lut.push_back({0, 0, (size_to_.y - limits.max.y) * size_to_.x});

  trace::debug("lut size: {} ({})", lut.size(), fmt_bytes(lut.size() * sizeof(lut_entry)));
}

auto grid_transform::load_lut(string const& uid, string const& cache_dir_path) -> pair<bool, string>
{
  // build the base path for the cache file
  string lut_path{cache_dir_path};
  lut_path.append(1, '/').append(to_string(std::hash<string>{}(uid))).append(1, '_');
  auto path_cidx_pos = lut_path.size();

  // loop through any cache objects that match the hash
  for (size_t cidx = 0; true; ++cidx)
  {
    try
    {
      // finish building the path
      char buf[32];
      snprintf(buf, sizeof(buf), "%zu", cidx);
      lut_path.resize(path_cidx_pos);
      lut_path.append(buf);

      // try to open this file
      auto file = std::ifstream{lut_path, std::ifstream::binary};
      if (!file)
        break;

      // read the header
      lut_header hdr;
      if (!file.read(reinterpret_cast<char*>(&hdr), sizeof(lut_header)))
        throw std::runtime_error{"bad header"};

      // verify the magic ID and entry size
      if (hdr.magic != lut_magic)
        throw std::runtime_error{"invalid magic token"};
      if (hdr.entry_size != sizeof(lut_entry))
        throw std::runtime_error{"entry size mismatch"};

      // if UID is different size we must be a conflicting hash, so skip the file
      if (hdr.uid_size != uid.size())
        continue;

      // read the UID from the file
      std::unique_ptr<char[]> file_uid{new char[hdr.uid_size + 1]};
      if (!file.read(file_uid.get(), hdr.uid_size))
        throw std::runtime_error{"read error (uid)"};
      file_uid[hdr.uid_size] = '\0';

      // if the UID is different we must be a conflicting hash, so skip the file
      if (uid != file_uid.get())
        continue;

      // we've passed all the tests so read in the table!
      auto& lut = xform_.emplace<lutdata>();
      lut.resize(hdr.lut_size);
      file.read(reinterpret_cast<char*>(lut.data()), sizeof(lut_entry) * hdr.lut_size);

      trace::debug("loaded lut: {}", lut_path);

      return {true, std::move(lut_path)};
    }
    catch (std::exception& err)
    {
      /* we simply skip tables that are corrupt or can't be read. this allows us to keep searching and even write
       * a valid table at the next available index. helpful if someone forgets to clean out the cache. */
      trace::error(
            "grid transform lut read failed:\n"
            "   path: {}\n"
            "  error: {}"
          , lut_path
          , format_exception(err, 9));
    }
  }

  return {false, std::move(lut_path)};
}

auto grid_transform::save_lut(const std::string& uid, const std::string& lut_path) const -> void
{
  try
  {
    auto& lut = std::get<lutdata>(xform_);

    // ensure the cache directory is created
    make_directory(lut_path, file_options::parents_only::yes);

    // open the file
    temporary_file tmp_path{lut_path};
    std::ofstream file{tmp_path.get(), std::ofstream::binary | std::ofstream::trunc};

    // write the file
    lut_header hdr{lut_magic, uid.size(), lut.size(), sizeof(lut_entry)};
    file.write(reinterpret_cast<const char*>(&hdr), sizeof(lut_header));
    file.write(uid.c_str(), uid.size());
    file.write(reinterpret_cast<const char*>(lut.data()), sizeof(lut_entry) * lut.size());

    // move ensures atomic update to file
    move_file(tmp_path.get(), lut_path, file_options::require_atomic::yes);
    tmp_path.release();

    trace::debug("saved lut: {}", lut_path);
  }
  catch (std::exception& err)
  {
    trace::error(
          "grid transform lut write failed:\n"
          "   path: {}\n"
          "  error: {}"
        , lut_path
        , format_exception(err, 9));
  }
}

auto grid_transform::build_fastmesh(
      map_projection const& from_proj
    , grid_coordinates const& from_coords
    , map_projection const& to_proj
    , grid_coordinates const& to_coords
    , int cells_x // mesh cells in x dimension
    ) -> void
{
  // sanity checks
  if (cells_x < 1)
    throw std::runtime_error{"grid transform fastmesh invalid parameters"};
  if (!from_coords.regular() || !to_coords.regular())
    throw std::runtime_error{"grid transform fastmesh technique not supported on irregular grids"};

  auto& fm = xform_.emplace<fastmesh>();

  // determine divisions in y dimension by retaining approximately the same aspect ratio
  auto cells_y = std::max(1, int(std::round(cells_x * float(size_to_.y) / size_to_.x)));

  // calculate 'from' coordinates of a coarse mesh overlaying our 'to' grid
  fm.to_ix.resize(cells_x + 1);
  fm.to_iy.resize(cells_y + 1);
  fm.from_xy.resize(vec2z{fm.to_ix.size(), fm.to_iy.size()});
  for (size_t row = 0; row < fm.to_iy.size(); ++row)
  {
    auto frac = float(row) / cells_y;
    fm.to_iy[row] = frac * size_to_.y;
    for (size_t col = 0; col < fm.to_ix.size(); ++col)
      fm.from_xy[row][col].y = lerp(to_coords.row_edges()[0], to_coords.row_edges()[to_coords.size().y], frac);
  }
  for (size_t col = 0; col < fm.to_ix.size(); ++col)
  {
    auto frac = float(col) / cells_x;
    fm.to_ix[col] = frac * size_to_.x;
    for (size_t row = 0; row < fm.to_iy.size(); ++row)
      fm.from_xy[row][col].x = lerp(to_coords.col_edges()[0], to_coords.col_edges()[to_coords.size().x], frac);
  }

  if (to_coords.row_units() != to_proj.units())
    unit::transform{unit{to_coords.row_units()}, unit{to_proj.units()}}(span{reinterpret_cast<double*>(fm.from_xy.data()), long(fm.from_xy.size() * 2)});
  map_projection::transform(to_proj, from_proj, fm.from_xy);
  if (from_coords.row_units() != from_proj.units())
    unit::transform{unit{from_proj.units()}, unit{from_coords.row_units()}}(span{reinterpret_cast<double*>(fm.from_xy.data()), long(fm.from_xy.size() * 2)});

  // convert the from_xy's into actual pixel indexes
  for (auto& xy : fm.from_xy)
  {
    xy.x = (xy.x - from_coords.col_edges()[0]) / (from_coords.col_edges()[1] - from_coords.col_edges()[0]);
    xy.y = (xy.y - from_coords.row_edges()[0]) / (from_coords.row_edges()[1] - from_coords.row_edges()[0]);
  }
}

auto bom::determine_geodetic_coordinates(map_projection const& proj, grid_coordinates const& coords) -> array2<latlon>
{
  // transform the grid coordinates to the units required by the projection
  array1d row_edges{coords.row_edges()};
  if (coords.row_units() != proj.units())
    unit::transform{unit{coords.row_units()}, unit{proj.units()}}(row_edges);

  array1d col_edges{coords.col_edges()};
  if (coords.col_units() != proj.units())
    unit::transform{unit{coords.col_units()}, unit{proj.units()}}(col_edges);

  // generate the center coordiantes for each cell
  array2<vec2d> xy{coords.size()};
  for (size_t x = 0; x < coords.size().x; ++x)
  {
    auto val = (col_edges[x] + col_edges[x + 1]) * 0.5;
    for (size_t y = 0; y < coords.size().y; ++y)
      xy[y][x].x = val;
  }
  for (size_t y = 0; y < coords.size().y; ++y)
  {
    auto val = (row_edges[y] + row_edges[y + 1]) * 0.5;
    for (size_t x = 0; x < coords.size().x; ++x)
      xy[y][x].y = val;
  }

  auto proj_ll = map_projection::make_wgs84_projection(proj.context());

  map_projection::transform(proj, proj_ll, xy);

  // sanity check - we know (and rely) the latlon projection always producing radians
  if (proj_ll.units() != "radians")
    throw std::logic_error{"expected latlong projection to use radians units"};

  /* transform from radians in x,y into lat/lons
   * note: we can't just abuse the type system and case the pointers to lat/lon into vec2 when doing the
   * transform because vec2 stores x then y, while latlon stores y (lat) then x (lon) */
  array2<latlon> ll{coords.size()};
  for (size_t y = 0; y < coords.size().y; ++y)
  {
    for (size_t x = 0; x < coords.size().x; ++x)
    {
      ll[y][x].lat = xy[y][x].y * 1_rad;
      ll[y][x].lon = xy[y][x].x * 1_rad;
    }
  }

  return ll;
}

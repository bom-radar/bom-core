/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "map_projection.h"
#include <bom/ellipsoid.h>
#include <bom/trace.h>
#include <bom/unit_test.h>
#include <cstddef>
#include <stdexcept>

#if BOM_CORE_PROJ_LEGACY_API
  #define ACCEPT_USE_OF_DEPRECATED_PROJ_API_H
  #include <proj_api.h>
  static_assert(std::is_same<void*, projPJ>::value, "projPJ is not void*");
  static_assert(std::is_same<void*, projCtx>::value, "projCtx is not void*");
#else
  #include <proj.h>
  static_assert(sizeof(void*) == sizeof(PJ*));
#endif

using namespace bom;

namespace
{
  struct dummy2 { vec2d foo[2]; };
  struct dummy3 { vec3d foo[2]; };

  // offsets between elements of arrays of vec2d and vec3d
  constexpr int offset2 = (offsetof(dummy2, foo[1]) - offsetof(dummy2, foo[0]));
  constexpr int offset3 = (offsetof(dummy3, foo[1]) - offsetof(dummy3, foo[0]));

#if BOM_CORE_PROJ_LEGACY_API
  /* ensure that vec2 and vec3 array elements align evenly with the size of a double as needed for the transform
   * functions accepting these types. this is because the interleaved array model in proj4 wants stride offsets in
   * doubles rather than bytes. */
  static_assert(offset2 % sizeof(double) == 0, "vec2 array does not align with double!");
  static_assert(offset3 % sizeof(double) == 0, "vec3 array does not align with double!");
#endif
}

/* UDUNITS names that correspond to known proj.4 unit names
 * units which have no known UDUNITS equivalent are omitted from this list and will be passed through directly
 * with a warning printed to traces */
static constexpr pair<char const*, char const*> unit_lookup[] =
{
    { "km",     "km" }
  , { "m",      "m" }
  , { "dm",     "dm" }
  , { "cm",     "cm" }
  , { "mm",     "mm" }
  , { "kmi",    "nmiles" }
  , { "in",     "in" }
  , { "ft",     "ft" }
  , { "yd",     "yd" }
  , { "mi",     "mi" }
  , { "fath",   "fathom" }
  , { "ch",     "chain" }
  //, { "link",   nullptr }
  //, { "us-in",  nullptr }
  , { "us-ft",  "US_survey_feet" }
  , { "us-yd",  "US_survey_yards" }
  //, { "us-ch",  nullptr }
  , { "us-mi",  "US_survey_miles" }
  //, { "ind-yd", nullptr }
  //, { "ind-ft", nullptr }
  //, { "ind-ch", nullptr }
  , { nullptr, nullptr }
};

/// Context for creation and use of projection objects
class map_projection::transform_context
{
public:
  transform_context()
#if BOM_CORE_PROJ_LEGACY_API
    : handle_{pj_ctx_alloc()}
#else
    : handle_{proj_context_create()}
#endif
  {
    if (!handle_)
      throw std::runtime_error{"failed to initialize proj context"};
  }

  ~transform_context()
  {
#if BOM_CORE_PROJ_LEGACY_API
    pj_ctx_free(handle_);
#else
    proj_context_destroy(handle_);
#endif
  }

  transform_context(transform_context const&) = delete;
  transform_context(transform_context&&) noexcept = delete;
  auto operator=(transform_context const&) -> transform_context& = delete;
  auto operator=(transform_context&&) noexcept -> transform_context& = delete;

private:
#if BOM_CORE_PROJ_LEGACY_API
  projCtx     handle_;
#else
  PJ_CONTEXT* handle_;
#endif
  std::mutex  mut_;
  friend class map_projection;

  // this helper reduces verbosity of having to check for non-default context handle everywhere
#if BOM_CORE_PROJ_LEGACY_API
  friend inline auto to_ctx(map_projection::context_hnd ctx) { return ctx ? ctx->handle_ : pj_get_default_ctx(); }
#else
  friend inline auto to_ctx(map_projection::context_hnd ctx) { return ctx ? ctx->handle_ : nullptr; }
#endif
};

static std::mutex mut_default_context_;

#if BOM_CORE_PROJ_LEGACY_API
#else
// this helper reduces verbosity of having reinterpret_casts everywhere
// we use a void* in the class and reinterpret_cast back to a PJ* because it avoid needing to expose the user to the
// proj.h header when they use the map_projection class
inline auto to_pj(void* handle) { return reinterpret_cast<PJ*>(handle); }
#endif

map_projection::error::error(string definition, context_hnd context)
  : std::runtime_error{fmt::format(
            "map_projection failure: {} definition: {}"
#if BOM_CORE_PROJ_LEGACY_API
          , pj_strerrno(pj_ctx_get_errno(to_ctx(context)))
#elif PROJ_VERSION_MAJOR < 8
          , proj_errno_string(proj_context_errno(to_ctx(context)))
#else
          , proj_context_errno_string(to_ctx(context), proj_context_errno(to_ctx(context)))
#endif
          , definition)}
{ }

map_projection::error::error(string definition_from, string definition_to, context_hnd context)
  : std::runtime_error{fmt::format(
            "map_projection failure: {} definition_from: {} definition_to: {}"
#if BOM_CORE_PROJ_LEGACY_API
          , pj_strerrno(pj_ctx_get_errno(to_ctx(context)))
#elif PROJ_VERSION_MAJOR < 8
          , proj_errno_string(proj_context_errno(to_ctx(context)))
#else
          , proj_context_errno_string(to_ctx(context), proj_context_errno(to_ctx(context)))
#endif
          , definition_from
          , definition_to)}
{ }

map_projection::error::error(string definition_from, string definition_to, int err)
  : std::runtime_error{fmt::format(
            "map_projection failure: {} definition_from: {} definition_to: {}"
#if BOM_CORE_PROJ_LEGACY_API
          , pj_strerrno(err)
#elif PROJ_VERSION_MAJOR < 8
          , proj_errno_string(proj_context_errno(nullptr))
#else
          , proj_context_errno_string(nullptr, err) // should never be called (only legacy api uses this constructor)
#endif
          , definition_from
          , definition_to)}
{ }


auto map_projection::proj_version() -> int
{
#if BOM_CORE_PROJ_LEGACY_API
  return PJ_VERSION;
#else
  // proj 8 has a macro for this calucation, but not proj 6 :(
  return PROJ_VERSION_MAJOR * 10000 + PROJ_VERSION_MINOR * 100 + PROJ_VERSION_PATCH;
#endif
}

auto map_projection::create_context() -> context_hnd
{
  return make_shared<transform_context>();
}

auto map_projection::make_wgs84_projection(context_hnd context) -> map_projection
{
  return {std::move(context), fmt::format("+proj=latlong +a={} +b={}", wgs84.semi_major_axis(), wgs84.semi_minor_axis())};
}

map_projection::map_projection(context_hnd context, string definition, string wkt)
  : context_{std::move(context)}
  , definition_{std::move(definition)}
  , wkt_{std::move(wkt)}
#if BOM_CORE_PROJ_LEGACY_API
  , handle_{pj_init_plus_ctx(to_ctx(context_), definition_.c_str())}
#else
  , handle_{proj_create(to_ctx(context_), definition_.c_str())}
#endif
{
  if (!handle_)
    throw error{definition_, context_};

  // determine the correct units string (in UDUNITS2 format)
  // note: proj4 API expects lat/lons to be in radians
#if BOM_CORE_PROJ_LEGACY_API
  if (pj_is_latlong(handle_))
#else
  if (proj_angular_input(to_pj(handle_), PJ_IDENT)) // NOT 100% sure this is correct under v6 API (was pj_is_latlong)
#endif
    units_ = "radians";
  else
  {
    // find the '+units=' parameter
    auto pos = definition_.find("+units=");
    if (pos == string::npos)
      units_ = "m";
    else
    {
      // extract the actual unit as written
      pos += 7;
      auto end = definition_.find_first_of(" \t\r\n", pos);
      units_.assign(definition_, pos, end == string::npos ? end : end - pos);

      // convert to the equivalent UDUNITS unit
      auto lut = unit_lookup;
      while (lut->first)
      {
        if (units_ == lut->first)
          break;
        ++lut;
      }
      if (lut->first)
        units_.assign(lut->second);
      else
        trace::warning("proj.4 unit '{}' has no known UDUNITS compatible equivalent", units_);
    }
  }
}

map_projection::map_projection(map_projection const& rhs)
  : context_{rhs.context_}
  , definition_{rhs.definition_}
  , wkt_{rhs.wkt_}
  , units_{rhs.units_}
#if BOM_CORE_PROJ_LEGACY_API
  , handle_{pj_init_plus_ctx(pj_get_ctx(rhs.handle_), definition_.c_str())}
#else
  , handle_{proj_create(to_ctx(context_), definition_.c_str())}
#endif
{
  if (!handle_)
    throw error{definition_, context_};
}

map_projection::map_projection(map_projection&& rhs) noexcept
  : context_{std::move(rhs.context_)}
  , definition_{std::move(rhs.definition_)}
  , wkt_{std::move(rhs.wkt_)}
  , units_{std::move(rhs.units_)}
  , handle_{rhs.handle_}
{
  rhs.handle_ = nullptr;
}

auto map_projection::operator=(map_projection const& rhs) -> map_projection&
{
#if BOM_CORE_PROJ_LEGACY_API
  auto handle = pj_init_plus_ctx(pj_get_ctx(rhs.handle_), rhs.definition_.c_str());
#else
  auto handle = proj_create(to_ctx(rhs.context_), rhs.definition_.c_str());
#endif
  if (!handle)
    throw error{rhs.definition_, rhs.context_};

  context_ = rhs.context_;
  definition_ = rhs.definition_;
  wkt_ = rhs.wkt_;
  units_ = rhs.units_;
  handle_ = rhs.handle_;

  return *this;
}

auto map_projection::operator=(map_projection&& rhs) noexcept -> map_projection&
{
  context_ = std::move(rhs.context_);
  definition_ = std::move(rhs.definition_);
  wkt_ = std::move(rhs.wkt_);
  units_ = std::move(rhs.units_);
  std::swap(handle_, rhs.handle_);
  return *this;
}

map_projection::~map_projection()
{
#if BOM_CORE_PROJ_LEGACY_API
  pj_free(handle_);
#else
  proj_destroy(to_pj(handle_));
#endif
}

auto map_projection::transform(map_projection const& from, map_projection const& to, span<double> x, span<double> y) -> void
{
  if (x.size() != y.size())
    throw std::logic_error{"map_projection input coordinate arrays size mismatch"};

  if (from.context() != to.context())
    throw std::logic_error{"map_projection input projections for transform have different contexts"};

  auto context = from.context();
  std::lock_guard<std::mutex> lock{context ? context->mut_ : mut_default_context_};

#if BOM_CORE_PROJ_LEGACY_API
  if (auto err = pj_transform(from.handle_, to.handle_, x.size(), 1, x.data(), y.data(), nullptr))
    throw map_projection::error{from.definition_, to.definition_, err};
#else
  // create the transformation projection
  /* since proj4, we now have these as a separate object.  this means we could rework our map_projection concept to provide
   * a dedicated 'map_transform' or 'map_reprojection' class which directly transforms from CRS to another without needing
   * a static function where we pass in the two individual map_projections.  this might be more efficient but is a big
   * change to our long established API.
   *
   * NOTE: we currently don't use the individual PJ from the map projections at all!!! initializing them is a complete
   * waste of resources!!!  we really should rework this class to a 'crs_transform' class or something like it.
   * */
  #if 0
  auto hnd_xform = proj_create_crs_to_crs_from_pj(to_ctx(context), to_pj(from.handle_), to_pj(to.handle_), 0, nullptr);
  #else
  auto hnd_xform = proj_create_crs_to_crs(to_ctx(context), from.definition_.c_str(), to.definition_.c_str(), nullptr);
  #endif
  if (!hnd_xform)
    throw map_projection::error{from.definition_, to.definition_, context};

  // do we need to convert angle units?
  // proj 4 expected radians, proj 6+ api expects degrees
  // this test would be better as proj_degree_input() but that's only available from proj 7 onwards
  if (from.units_ == "radians" && !proj_angular_input(hnd_xform, PJ_FWD))
  {
    for (auto& v : x)
      v *= angle::rad_to_deg;
    for (auto& v : y)
      v *= angle::rad_to_deg;
  }

  // TODO - do we need proj_normalize_for_visualization() here?

  // transform the coordinates
  auto count = proj_trans_generic(
        hnd_xform, PJ_FWD
      , x.data(), sizeof(double), x.size()
      , y.data(), sizeof(double), y.size()
      , nullptr, 0, 0
      , nullptr, 0, 0);
  if (count != x.size())
  {
    auto err = map_projection::error{from.definition_, to.definition_, context};
    proj_destroy(hnd_xform);
    throw err;
  }

  if (to.units_ == "radians" && !proj_angular_output(hnd_xform, PJ_FWD))
  {
    for (auto& v : x)
      v *= angle::deg_to_rad;
    for (auto& v : y)
      v *= angle::deg_to_rad;
  }

  // cleanup the transformation projection
  proj_destroy(hnd_xform);
#endif
}

auto map_projection::transform(map_projection const& from, map_projection const& to, span<double> x, span<double> y, span<double> z) -> void
{
  if (x.size() != y.size() || x.size() != z.size())
    throw std::logic_error{"map_projection input coordinate arrays size mismatch"};

  if (from.context() != to.context())
    throw std::logic_error{"map_projection input projections for transform have different contexts"};

  auto context = from.context();
  std::lock_guard<std::mutex> lock{context ? context->mut_ : mut_default_context_};

#if BOM_CORE_PROJ_LEGACY_API
  if (auto err = pj_transform(from.handle_, to.handle_, x.size(), 1, x.data(), y.data(), z.data()))
    throw map_projection::error{from.definition_, to.definition_, err};
#else
  //auto hnd_xform = proj_create_crs_to_crs_from_pj(to_ctx(context), to_pj(from.handle_), to_pj(to.handle_), 0, nullptr);
  auto hnd_xform = proj_create_crs_to_crs(to_ctx(context), from.definition_.c_str(), to.definition_.c_str(), nullptr);
  if (!hnd_xform)
    throw map_projection::error{from.definition_, to.definition_, context};

  if (from.units_ == "radians" && !proj_angular_input(hnd_xform, PJ_FWD))
  {
    for (auto& v : x)
      v *= angle::rad_to_deg;
    for (auto& v : y)
      v *= angle::rad_to_deg;
    for (auto& v : z)
      v *= angle::rad_to_deg;
  }

  auto count = proj_trans_generic(
        hnd_xform, PJ_FWD
      , x.data(), sizeof(double), x.size()
      , y.data(), sizeof(double), y.size()
      , z.data(), sizeof(double), z.size()
      , nullptr, 0, 0);
  if (count != x.size())
  {
    auto err = map_projection::error{from.definition_, to.definition_, context};
    proj_destroy(hnd_xform);
    throw err;
  }

  if (to.units_ == "radians" && !proj_angular_output(hnd_xform, PJ_FWD))
  {
    for (auto& v : x)
      v *= angle::deg_to_rad;
    for (auto& v : y)
      v *= angle::deg_to_rad;
    for (auto& v : z)
      v *= angle::deg_to_rad;
  }

  proj_destroy(hnd_xform);
#endif
}

auto map_projection::transform(map_projection const& from, map_projection const& to, span<vec2d> xy) -> void
{
  if (from.context() != to.context())
    throw std::logic_error{"map_projection input projections for transform have different contexts"};

  auto context = from.context();
  std::lock_guard<std::mutex> lock{context ? context->mut_ : mut_default_context_};

#if BOM_CORE_PROJ_LEGACY_API
  if (auto err = pj_transform(from.handle_, to.handle_, xy.size(), offset2 / sizeof(double), &xy[0].x, &xy[0].y, nullptr))
    throw map_projection::error{from.definition_, to.definition_, err};
#else
  //auto hnd_xform = proj_create_crs_to_crs_from_pj(to_ctx(context), to_pj(from.handle_), to_pj(to.handle_), 0, nullptr);
  auto hnd_xform = proj_create_crs_to_crs(to_ctx(context), from.definition_.c_str(), to.definition_.c_str(), nullptr);
  if (!hnd_xform)
    throw map_projection::error{from.definition_, to.definition_, context};

  if (from.units_ == "radians" && !proj_angular_input(hnd_xform, PJ_FWD))
  {
    for (auto& v : xy)
    {
      v.x *= angle::rad_to_deg;
      v.y *= angle::rad_to_deg;
    }
  }

  auto count = proj_trans_generic(
        hnd_xform, PJ_FWD
      , &xy[0].x, offset2, xy.size()
      , &xy[0].y, offset2, xy.size()
      , nullptr, 0, 0
      , nullptr, 0, 0);
  if (count != xy.size())
  {
    auto err = map_projection::error{from.definition_, to.definition_, context};
    proj_destroy(hnd_xform);
    throw err;
  }

  if (to.units_ == "radians" && !proj_angular_output(hnd_xform, PJ_FWD))
  {
    for (auto& v : xy)
      v *= angle::deg_to_rad;
  }

  proj_destroy(hnd_xform);
#endif
}

auto map_projection::transform(map_projection const& from, map_projection const& to, span<vec3d> xyz) -> void
{
  if (from.context() != to.context())
    throw std::logic_error{"map_projection input projections for transform have different contexts"};

  auto context = from.context();
  std::lock_guard<std::mutex> lock{context ? context->mut_ : mut_default_context_};

#if BOM_CORE_PROJ_LEGACY_API
  if (auto err = pj_transform(from.handle_, to.handle_, xyz.size(), offset3 / sizeof(double), &xyz[0].x, &xyz[0].y, &xyz[0].z))
    throw map_projection::error{from.definition_, to.definition_, err};
#else
  //auto hnd_xform = proj_create_crs_to_crs_from_pj(to_ctx(context), to_pj(from.handle_), to_pj(to.handle_), 0, nullptr);
  auto hnd_xform = proj_create_crs_to_crs(to_ctx(context), from.definition_.c_str(), to.definition_.c_str(), nullptr);
  if (!hnd_xform)
    throw map_projection::error{from.definition_, to.definition_, context};

  if (from.units_ == "radians" && !proj_angular_input(hnd_xform, PJ_FWD))
  {
    for (auto& v : xyz)
    {
      v.x *= angle::rad_to_deg;
      v.y *= angle::rad_to_deg;
      v.z *= angle::rad_to_deg;
    }
  }

  auto count = proj_trans_generic(
        hnd_xform, PJ_FWD
      , &xyz[0].x, offset3, xyz.size()
      , &xyz[0].y, offset3, xyz.size()
      , &xyz[0].z, offset3, xyz.size()
      , nullptr, 0, 0);
  if (count != xyz.size())
  {
    auto err = map_projection::error{from.definition_, to.definition_, context};
    proj_destroy(hnd_xform);
    throw err;
  }

  if (to.units_ == "radians" && !proj_angular_output(hnd_xform, PJ_FWD))
  {
    for (auto& v : xyz)
      v *= angle::deg_to_rad;
  }

  proj_destroy(hnd_xform);
#endif
}


TEST_CASE("map_projection")
{
  auto defn_aea = "+proj=aea +lat_1=-36.3 +lat_2=-39.4 +lon_0=144.752 +lat_0=-37.852 +a=6378137 +b=6356752.31414 +units=km";
  auto proj_aea = map_projection{map_projection::default_context, defn_aea};
  auto proj_ll = map_projection::make_wgs84_projection(map_projection::default_context);

  auto test_points_xy = std::vector<vec2d>
  {
      {    0.0,    0.0 }
    , { -100.0,   40.0 }
    , {   30.0, -200.0 }
  };
  auto test_points_ll = std::vector<vec2d>
  {
      { 144.752000, -37.852000 }
    , { 143.620895, -37.486283 }
    , { 145.101504, -39.652759 }
  };

  // transform cartesian to latlon
  {
    auto buf = test_points_xy;
    map_projection::transform(proj_aea, proj_ll, buf);
    for (size_t i = 0; i < buf.size(); ++i)
    {
      CHECK(buf[i].x * angle::rad_to_deg == approx(test_points_ll[i].x));
      CHECK(buf[i].y * angle::rad_to_deg == approx(test_points_ll[i].y));
    }
  }

  // transform latlon to cartesian
  {
    auto buf = test_points_ll;
    for (auto& v : buf)
      v *= angle::deg_to_rad;

    map_projection::transform(proj_ll, proj_aea, buf);
    for (size_t i = 0; i < buf.size(); ++i)
    {
      CHECK(buf[i].x == approx(test_points_xy[i].x));
      CHECK(buf[i].y == approx(test_points_xy[i].y));
    }
  }
}

/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "map_projection.h"
#include <bom/array2.h>
#include <bom/latlon.h>
#include <bom/grid_coordinates.h>
#include <variant>

namespace bom
{
  struct default_is_missing
  {
    auto operator()(float val) const { return std::isnan(val); }
  };
  struct default_merge_missing
  {
    auto operator()(float v1, float v2) const { return v1; }
  };

  /// Transform used to regrid fields between different projections
  class grid_transform
  {
  public:
    /// Policy on how to handle missing values during grid transformation
    enum class missing_policy
    {
        ignore      ///< Ignore missing values encountered during subsampling (unless all subsamples are missing)
      , replace     ///< Replace missing values encountered during subsampling with a fixed value
    };

    /// Policy on how to merge transformed grid cells into the existing grid
    enum class merge_policy
    {
        overwrite   ///< Overwrite the existing grid cell
      , minimum     ///< Take the minimum of the existing and transformed cell
      , maximum     ///< Take the maximum of the existing and transformed cell
    };

  public:
    /// Initialize a grid transform
    /** The value of 'subsamples' determines the regridding method:
     *  -   0 = bilinear interpolation
     *  -   1 = nearest neighbor
     *  - > 1 = subsampling, with the value determining the number of samples in x and y direction
     *          for example, a value of 3 results in 9 regularly distributed subsamples within each output grid cell
     *  - < 0 = "fastmesh" algorithm.  this algorithm creates a regular mesh overlaying the output domain with the
     *          absolute of this value determining the number of mesh cells in x and y.  the mesh verticies are transformed
     *          into the input domain.  bilinear interpolation of the input/output coodinates in this mesh is used to determine
     *          the transform mapping.  the regular recangular mesh of the output is effectively "warped" into the input
     *          domain.  this method allows for extremely fast transforms due to the vastly reduced number of expensive
     *          "true" coordinate transforms required.  higher mesh cell counts may be used to increase positional accuracy.
     *          for example, a value of "-4" will result in a 4x4 coarse grid being used, so only 16 coordiante transforms
     *          are required to regrid the entire domain.  this method is very useful for regridding a smaller domain
     *          (e.g. radar) into a larger one (e.g. national).
     */
    grid_transform(
          map_projection const& from_proj       ///< Map projection for input domain
        , grid_coordinates const& from_coords   ///< Grid coordinates for input domain
        , map_projection const& to_proj         ///< Map projection for output domain
        , grid_coordinates const& to_coords     ///< Grid coordinate for output domain
        , int subsamples = 3                    ///< Grid transform mode - see above
        , string const& cache_dir_path = ""     ///< Path to lookup table cache (directory)
        );

    /// Apply the grid transformation to an array
    template <typename IsMissing = default_is_missing>
    auto transform(
          array2f const& from                   ///< Input array (must be consistent with input grid)
        , array2f& to                           ///< Output array (must be conssitent with output grid)
        , missing_policy policy                 ///< Policy for handling missing data in input array
        , float replace_value = nan<float>()    ///< Value used to replace missing data when using 'replace' policy
        , IsMissing is_missing = {}             ///< Functor to detect missing data (default to isnan())
        ) const -> void;

    /// Apply the grid transformation to an array but merge the transformed grid into the existing array
    template <typename IsMissing = default_is_missing, typename MergeMissing = default_merge_missing>
    auto transform_merge(
          array2f const& from                   ///< Input array (must be consistent with input grid)
        , array2f& to                           ///< Output array (must be conssitent with output grid)
        , merge_policy policy_merge             ///< Policy for merging transformed grid into existing 'to' array
        , missing_policy policy_missing         ///< Policy for handling missing data in input array
        , float replace_value = nan<float>()    ///< Value used to replace missing data when using 'replace' missing policy
        , IsMissing is_missing = {}             ///< Functor to detect missing data (default to isnan())
        , MergeMissing merge_missing = {}       ///< Functor to merge missing data (default to replicate first argument)
        ) const -> void;

  private:
    struct lut_entry
    {
      unsigned  weight : 7;    // relative weight of 'from' pixel, 0 indicates RLE offset
      unsigned  final  : 1;    // indicates whether to advance to next output pixel
      size_t    index  : 48;   // index of 'from' pixel, or number of 'to' pixels to skip (RLE)
    };
    using lutdata = vector<lut_entry>;
    struct fastmesh
    {
      array1f       to_iy;
      array1f       to_ix;
      array2<vec2d> from_xy;
    };
    using xform_data = std::variant<lutdata, fastmesh>;

  private:
    auto determine_limits(
          map_projection const& from_proj
        , grid_coordinates const& from_coords
        , map_projection const& to_proj
        , grid_coordinates const& to_coords
        ) const -> box2z;
    auto build_lut_bilinear(
          map_projection const& from_proj
        , grid_coordinates const& from_coords
        , map_projection const& to_proj
        , grid_coordinates const& to_coords
        ) -> void;
    auto build_lut_subsample(
          map_projection const& from_proj
        , grid_coordinates const& from_coords
        , map_projection const& to_proj
        , grid_coordinates const& to_coords
        , size_t subsamples
        ) -> void;
    auto load_lut(string const& uid, string const& cache_dir_path) -> pair<bool, string>;
    auto save_lut(string const& uid, string const& lut_path) const -> void;

    auto build_fastmesh(
          map_projection const& from_proj
        , grid_coordinates const& from_coords
        , map_projection const& to_proj
        , grid_coordinates const& to_coords
        , int divisions
        ) -> void;

    template <typename IsMissing>
    auto transform_fastmesh(
          array2f const& from
        , array2f& to
        , missing_policy policy
        , float replace_value
        , IsMissing is_missing
        ) const -> void;

  private:
    vec2z             size_from_;
    vec2z             size_to_;
    xform_data        xform_;
  };

  /// Build an array of latlons that corresponds to the center of each grid cell in a domain
  auto determine_geodetic_coordinates(map_projection const& proj, grid_coordinates const& coords) -> array2<latlon>;

  BOM_DECLARE_ENUM_TRAITS(grid_transform::merge_policy, overwrite, minimum, maximum);

  inline auto smoothstep(float x)
  {
    x = std::clamp(x, 0.0f, 1.0f);
    return x * x * (3.0f - 2.0f * x);
  }

  inline auto smootherstep(float x)
  {
    x = std::clamp(x, 0.0f, 1.0f);
    return x * x * x * (x * (6.0f * x - 15.0f) + 10.0f);
  }

  template <typename IsMissing>
  auto grid_transform::transform_fastmesh(
        array2f const& from
      , array2f& to
      , missing_policy policy
      , float replace_value
      , IsMissing is_missing
      ) const -> void
  {
    auto& fm = std::get<fastmesh>(xform_);

    struct mesh_frac
    {
      int mesh;
      float frac;
    };
    auto pcx = array1<mesh_frac>{to.extents().x};
    for (size_t x = 0; x < to.extents().x; ++x)
    {
      auto real_x = ((x + 0.5f) * (fm.to_ix.size() - 1) / to.extents().x);
      pcx[x].mesh = int(real_x);
      pcx[x].frac = real_x - pcx[x].mesh;
    }

    for (size_t y = 0; y < to.extents().y; ++y)
    {
      // TODO - precalculate these and store as part of fastmesh structure
      auto real_y = ((y + 0.5f) * (fm.to_iy.size() - 1) / to.extents().y);
      auto mesh_y = int(real_y);
      auto frac_y = real_y - mesh_y;

      for (size_t x = 0; x < to.extents().x; ++x)
      {
        auto mesh_x = pcx[x].mesh;
        auto frac_x = pcx[x].frac;

        // okay, lerp the mesh to generate our 'from' coordinates
        auto row1 = lerp(fm.from_xy[mesh_y+0][mesh_x+0], fm.from_xy[mesh_y+0][mesh_x+1], frac_x);
        auto row2 = lerp(fm.from_xy[mesh_y+1][mesh_x+0], fm.from_xy[mesh_y+1][mesh_x+1], frac_x);
        auto xy = lerp(row1, row2, frac_y);

        // outside of bounds?
        if (xy.x < 0 || xy.x >= size_from_.x || xy.y < 0 || xy.y >= size_from_.y)
        {
          to[y][x] = nan<float>();
          continue;
        }

        // determine 4 grid cells to use for bilinear interpolation in from grid
        auto x0 = int(xy.x - 0.5f);
        auto fx = (xy.x - 0.5f) - x0;
        auto x1 = x0 + 1;
        auto y0 = int(xy.y - 0.5f);
        auto fy = (xy.y - 0.5f) - y0;
        auto y1 = y0 + 1;
        x0 = std::max(x0, 0);
        x1 = std::min(x1, int(size_from_.x - 1));
        y0 = std::max(y0, 0);
        y1 = std::min(y1, int(size_from_.y - 1));

        // optional - apply a smoothstep!
        fx = smoothstep(fx);
        fy = smoothstep(fy);

        // get the corner values for our interpolation
        auto v00 = from[y0][x0];
        auto v01 = from[y0][x1];
        auto v10 = from[y1][x0];
        auto v11 = from[y1][x1];

        /* if either side of a lerp is nan, then just take the value that our interpolation point is closest to.  this
         * means at a nan/non-nan boundary we will use the non-nan value until half way then the nan for the other half.
         * this preserves nice crisp boundaries at the nan/non-nan boundaries. */
        auto vrow0 = (is_missing(v00) || is_missing(v01)) ? (fx < 0.5 ? v00 : v01) : lerp(v00, v01, fx);
        auto vrow1 = (is_missing(v10) || is_missing(v11)) ? (fx < 0.5 ? v10 : v11) : lerp(v10, v11, fx);
        to[y][x] = (is_missing(vrow0) || is_missing(vrow1)) ? (fy < 0.5 ? vrow0 : vrow1) : lerp(vrow0, vrow1, fy);
      }
    }
  }

  template <typename IsMissing>
  auto grid_transform::transform(
        array2f const& from
      , array2f& to
      , missing_policy policy
      , float replace_value
      , IsMissing is_missing
      ) const -> void
  {
    if (size_from_ != from.extents())
      throw std::runtime_error{fmt::format("grid transform size mismatch ({}, expected {})", from.extents(), size_from_)};
    if (size_to_ != to.extents())
      throw std::runtime_error{fmt::format("grid transform size mismatch ({}, expected {})", to.extents(), size_to_)};

    if (std::holds_alternative<fastmesh>(xform_))
    {
      transform_fastmesh(from, to, policy, replace_value, is_missing);
      return;
    }

    auto& lut = std::get<lutdata>(xform_);

    size_t l = 0;
    for (size_t i = 0; i < to.size(); ++i)
    {
      // RLE of missing data?
      if (lut[l].weight == 0)
      {
        for (size_t end = i + lut[l].index - 1; i != end; ++i)
          to.data()[i] = nan<float>();
        to.data()[i] = nan<float>();
        ++l;
        continue;
      }

      // Normal LUT entries
      auto value = 0.0f;
      auto weight = 0;
      while (true)
      {
        auto val = from.data()[lut[l].index];
        if (is_missing(val))
        {
          if (policy == missing_policy::replace)
          {
            value += replace_value * lut[l].weight;
            weight += lut[l].weight;
          }
        }
        else
        {
          value += val * lut[l].weight;
          weight += lut[l].weight;
        }

        if (lut[l].final)
        {
          /* if the total weight is 0, then all pixels were invalid so just propogate the final invalid pixel
           * we saw.  this is better than just outputting a default nan since we will at least preserve the
           * characteristics of the invalid pixel (e.g. nan payload or special flag value). */
          to.data()[i] = weight > 0 ? (value / weight) : val;
          ++l;
          break;
        }

        ++l;
      }
    }
    if (l != lut.size())
      throw std::runtime_error{"grid transform detected bad LUT"};
  }

  template <typename IsMissing, typename MergeMissing>
  auto grid_transform::transform_merge(
        array2f const& from
      , array2f& to
      , merge_policy policy_merge
      , missing_policy policy_missing
      , float replace_value
      , IsMissing is_missing
      , MergeMissing merge_missing
      ) const -> void
  {
    if (size_from_ != from.extents())
      throw std::runtime_error{fmt::format("grid transform size mismatch ({}, expected {})", from.extents(), size_from_)};
    if (size_to_ != to.extents())
      throw std::runtime_error{fmt::format("grid transform size mismatch ({}, expected {})", to.extents(), size_to_)};

    if (std::holds_alternative<fastmesh>(xform_))
      throw std::logic_error{"Unimplemented"};

    auto& lut = std::get<lutdata>(xform_);

    size_t l = 0;
    for (size_t i = 0; i < to.size(); ++i)
    {
      // RLE of missing data?
      if (lut[l].weight == 0)
      {
        i += lut[l].index - 1;
        ++l;
        continue;
      }

      // Normal LUT entries
      auto value = 0.0f;
      auto weight = 0;
      while (true)
      {
        auto val = from.data()[lut[l].index];
        if (is_missing(val))
        {
          if (policy_missing == missing_policy::replace)
          {
            value += replace_value * lut[l].weight;
            weight += lut[l].weight;
          }
        }
        else
        {
          value += val * lut[l].weight;
          weight += lut[l].weight;
        }

        if (lut[l].final)
        {
          if (weight > 0)
          {
            value /= weight;
            switch (policy_merge)
            {
            case merge_policy::overwrite:
              to.data()[i] = value;
              break;
            case merge_policy::minimum:
              to.data()[i] = is_missing(to.data()[i]) ? value : std::min(value, to.data()[i]);
              break;
            case merge_policy::maximum:
              to.data()[i] = is_missing(to.data()[i]) ? value : std::max(value, to.data()[i]);
              break;
            }
          }
          /* if the total weight is 0, then all pixels were invalid.  if the destination already has a valid
           * valid then just leave it be.  however if it doesn't yet have a valid value then propogate the
           * invalid value from the final invalid pixel we saw.  this is better than just leaving the original
           * nan since we will at least preserve the characteristics of the invalid pixel (e.g. nan payload or
           * special flag value). hmm TODO - we should prioritize missing data flags */
          else if (is_missing(to.data()[i]))
          {
            to.data()[i] = merge_missing(to.data()[i], val);
          }
          ++l;
          break;
        }

        ++l;
      }
    }
    if (l != lut.size())
      throw std::runtime_error{"grid transform detected bad LUT"};
  }
}

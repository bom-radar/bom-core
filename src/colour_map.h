/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <bom/array1.h>
#include <bom/colour.h>
#include <bom/io/configuration.h>
#include <bom/io/json.h>

namespace bom
{
  class colour_map
  {
  public:
    enum class interpolation_mode
    {
        gradient
      , thresholds
    };

  public:
    colour_map();
    colour_map(io::configuration const& config);

    auto missing() const -> colour                { return missing_; }
    auto underflow() const -> colour              { return underflow_; }
    auto overflow() const -> colour               { return overflow_; }
    auto colours() const -> array1<pair<float, colour>> const& { return table_; }
    auto mode() const -> interpolation_mode       { return mode_; }

    auto min_value() const { return table_[0].first; }
    auto max_value() const { return table_[table_.size()-1].first; }

    /// Determine the colour for a value using a default 0..1 scale
    auto evaluate(float value) const -> colour;

  private:
    colour          missing_;
    colour          underflow_;
    colour          overflow_;
    array1<pair<float, colour>> table_;
    interpolation_mode          mode_;
  };

  BOM_DECLARE_ENUM_TRAITS(colour_map::interpolation_mode, gradient, thresholds);
}

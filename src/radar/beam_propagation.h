/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2017 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <bom/angle.h>
#include <bom/raii.h>

namespace bom::radar
{
  /// Model of radar beam propagation path
  class beam_propagation
  {
  public:
    beam_propagation(
          double altitude = 0.0
        , angle elevation = 0.0_deg
        , double earth_radius = 6378137.000 // wgs84 major axis
        , double effective_multiplier = 4.0 / 3.0 // text-book default
        );

    auto elevation() const -> angle                       { return elevation_; }
    auto set_elevation(angle val) -> void;

    auto altitude() const -> double                       { return altitude_; }
    auto set_altitude(double val) -> void;

    auto earth_radius() const -> double                   { return earth_radius_; }
    auto set_earth_radius(double val) -> void;

    auto effective_multiplier() const -> double           { return multiplier_; }
    auto set_effective_multiplier(double val) -> void;

    /// Determine ground range and altitude from slant range
    auto ground_range_altitude(double slant_range) const -> pair<double, double>;

    /// Determine ground range (only) from slant range
    auto ground_range(double slant_range) const -> double;

    /// Determine altitude (only) from slant range
    auto altitude(double slant_range) const -> double;

    /// Determine slant range from ground range
    auto slant_range(double ground_range) const -> double;

    /// Determine slant range (only) from altitude
    auto slant_range_at_altitude(double altitude) const -> double;

    // the functions below allow override of the elevation angle on a per-call basis at a speed penalty

    /// Determine ground range and altitude from slant range using a custom elevation
    auto ground_range_altitude(angle elevation, double slant_range) const -> pair<double, double>;

    /// Determine ground range (only) from slant range using a custom elevation
    auto ground_range(angle elevation, double slant_range) const -> double;

    /// Determine altitude (only) from slant range using a custom elevation
    auto altitude(angle elevation, double slant_range) const -> double;

    /// Determine slant range from ground range using a custom elevation
    auto slant_range(angle elevation, double ground_range) const -> double;

    /// Determine slant range (only) from altitude
    auto slant_range_at_altitude(angle elevation, double altitude) const -> double;

    /// Determine required elevation angle to hit given ground range and altitude
    auto required_elevation_angle(double ground_range, double altitude) const -> angle;

  private:
    angle   elevation_;
    double  altitude_;
    double  earth_radius_;
    double  multiplier_;

    // cached for speed of repeated calls
    double  effective_radius_;
    double  cos_elev_;
    double  sin_elev_;
    double  eer_alt_;
  };
}

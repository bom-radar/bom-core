/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2017 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "beam_propagation.h"

using namespace bom;
using namespace bom::radar;

beam_propagation::beam_propagation(
      double altitude
    , angle elevation
    , double earth_radius
    , double effective_multiplier)
  : elevation_{elevation}
  , altitude_{altitude}
  , earth_radius_{earth_radius}
  , multiplier_(effective_multiplier)
  , effective_radius_{multiplier_ * earth_radius_}
  , cos_elev_{cos(elevation_)}
  , sin_elev_{sin(elevation_)}
  , eer_alt_{effective_radius_ + altitude_}
{ }

auto beam_propagation::set_elevation(angle val) -> void
{
  elevation_ = val;
  cos_elev_ = cos(elevation_);
  sin_elev_ = sin(elevation_);
}

auto beam_propagation::set_altitude(double val) -> void
{
  altitude_ = val;
  eer_alt_ = effective_radius_ + altitude_;
}

auto beam_propagation::set_earth_radius(double val) -> void
{
  earth_radius_ = val;
  effective_radius_ = multiplier_ * earth_radius_;
  eer_alt_ = effective_radius_ + altitude_;
}

auto beam_propagation::set_effective_multiplier(double val) -> void
{
  multiplier_ = val;
  effective_radius_ = multiplier_ * earth_radius_;
  eer_alt_ = effective_radius_ + altitude_;
}

auto beam_propagation::ground_range_altitude(double slant_range) const -> pair<double, double>
{
  // distance along plane tangental to earth at site location
  auto h = slant_range * cos_elev_;
  // distance above plane tangental to earth at site location
  auto v = (slant_range * sin_elev_) + eer_alt_;
  return
  {
      // x = ground range = curved distance along earth surface
      std::atan(h/v) * effective_radius_
      // y = altitude = distance above spherical earth surface
    , std::hypot(h, v) - effective_radius_
  };

#if 0
  /* Alternative version based on theory from text book:
   *    Dopper Radar and Weather Observations, Doviak & Zrnic
   * The two versions here were derived separately, however result in negligible
   * difference (< 6.5mm height, < 0.1mm range @ 100km with elev angle of 30 deg) */
  auto hgt = sqrt(in.x * in.x + eff_sqr_ + 2.0 * in.x * eff_rad_ * sin_elev_) - eff_rad_ + ref_point_sph_.hgt;
  auto rng = eff_rad_ * asin((in.x * cos_elev_)/(eff_rad_ + hgt));
#endif
}

// same as ground_range_altitude with final altitude calculation skipped
auto beam_propagation::ground_range(double slant_range) const -> double
{
  // distance along plane tangental to earth at site location
  auto h = slant_range * cos_elev_;
  // distance above plane tangental to earth at site location
  auto v = (slant_range * sin_elev_) + eer_alt_;
  // ground range = curved distance along earth surface
  return std::atan(h/v) * effective_radius_;
}

// same as ground_range_altitude with final ground range calculation skipped
auto beam_propagation::altitude(double slant_range) const -> double
{
  // distance along plane tangental to earth at site location
  auto h = slant_range * cos_elev_;
  // distance above plane tangental to earth at site location
  auto v = (slant_range * sin_elev_) + eer_alt_;
  // altitude = distance above spherical earth surface
  return std::hypot(h, v) - effective_radius_;
}

auto beam_propagation::slant_range(double ground_range) const -> double
{
  // TODO - it would be nice to calculate the geometric height here too...

  // inverse of above formula
  if (ground_range <= 0.0)
    return 0.0;
  else
    return eer_alt_ / ((cos_elev_ / std::tan(ground_range / effective_radius_)) - sin_elev_);
}

auto beam_propagation::slant_range_at_altitude(double altitude) const -> double
{
  auto era = altitude + effective_radius_;
  auto cst = eer_alt_ * sin_elev_;
  return std::sqrt(era * era - eer_alt_ * eer_alt_ + cst * cst) - cst;
}

// identical to above, except that it uses a once off different elevation angle
auto beam_propagation::ground_range_altitude(angle elevation, double slant_range) const -> pair<double, double>
{
  // distance along plane tangental to earth at site location
  auto h = slant_range * cos(elevation);
  // distance above plane tangental to earth at site location
  auto v = (slant_range * sin(elevation)) + eer_alt_;
  return
  {
      // x = ground range = curved distance along earth surface
      std::atan(h/v) * effective_radius_
      // y = altitude = distance above spherical earth surface
    , std::hypot(h, v) - effective_radius_
  };
}

// identical to above, except that it uses a once off different elevation angle
auto beam_propagation::ground_range(angle elevation, double slant_range) const -> double
{
  // distance along plane tangental to earth at site location
  auto h = slant_range * cos(elevation);
  // distance above plane tangental to earth at site location
  auto v = (slant_range * sin(elevation)) + eer_alt_;
  // ground range = curved distance along earth surface
  return std::atan(h/v) * effective_radius_;
}

// identical to above, except that it uses a once off different elevation angle
auto beam_propagation::altitude(angle elevation, double slant_range) const -> double
{
  // distance along plane tangental to earth at site location
  auto h = slant_range * cos(elevation);
  // distance above plane tangental to earth at site location
  auto v = (slant_range * sin(elevation)) + eer_alt_;
  // altitude = distance above spherical earth surface
  return std::hypot(h, v) - effective_radius_;
}

// identical to above, except that it uses a once off different elevation angle
auto beam_propagation::slant_range(angle elevation, double ground_range) const -> double
{
  // TODO - it would be nice to calculate the geometric height here too...

  // inverse of above formula
  if (ground_range <= 0.0)
    return 0.0;
  else
    return eer_alt_ / ((cos(elevation) / std::tan(ground_range / effective_radius_)) - sin(elevation));
}

auto beam_propagation::slant_range_at_altitude(angle elevation, double altitude) const -> double
{
  auto era = altitude + effective_radius_;
  auto cst = eer_alt_ * sin(elevation);
  return std::sqrt(era * era - eer_alt_ * eer_alt_ + cst * cst) - cst;
}

auto beam_propagation::required_elevation_angle(double ground_range, double altitude) const -> angle
{
  /* Warning to modifiers:
   * It is crucial that the calculations performed by this function use double precision
   * math.  This is due mostly to the very small angle of theta and very large values
   * of tgt_alt and eer_alt.  DO NOT CHANGE IT TO SINGLE PRECISION!  */

  // angle between site location and target on great circle
  angle theta = (ground_range / effective_radius_) * 1.0_rad;

  // slant range (law of cosines)
  auto tgt_alt = effective_radius_ + altitude;
  auto eer_alt = eer_alt_;
  auto slant_range
    = (tgt_alt * tgt_alt)
    + (eer_alt * eer_alt)
    - 2.0 * tgt_alt * eer_alt * cos(theta);
  slant_range = std::sqrt(slant_range);

  // elevation angle (law of sines)
  // find the angle opposite the shorter side (since asin always returns < 90 degrees)
  if (tgt_alt < eer_alt)
    return asin((tgt_alt * sin(theta)) / slant_range) - 90.0_deg;
  else
    return 90.0_deg - theta - asin((eer_alt * sin(theta)) / slant_range);
}


/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "rapic.h"
#include <bom/math_utils.h>
#include <bom/trace.h>
#include <bom/unit_test.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <cmath>

using namespace bom;
using namespace bom::io;
using namespace bom::io::rapic;

BOM_DEFINE_ENUM_TRAITS(bom::io::rapic::message_type)
BOM_DEFINE_ENUM_TRAITS(bom::io::rapic::scan_type)
BOM_DEFINE_ENUM_TRAITS(bom::io::rapic::connection_state)

static constexpr message_type no_message = static_cast<message_type>(-1);

static const auto msg_keepalive = "RDRSTAT:\n"s;

static const auto msg_comment_head = "/"s;
static const auto msg_mssg_head = "MSSG:"s;
static const auto msg_mssg30_head = "MSSG: 30"s;
static const auto msg_mssg30_term = "END STATUS"s;
static const auto msg_status_head = "RDRSTAT:"s;
static const auto msg_permcon_head = "RPQUERY: SEMIPERMANENT CONNECTION"s;
static const auto msg_query_head = "RPQUERY:"s;
static const auto msg_filter_head = "RPFILTER:"s;
static const auto msg_scan_term = "END RADAR IMAGE"s;

// this table translates the ASCII encoding absolute, RLE digits and delta lookups
namespace
{
  enum enc_type : int
  {
      value
    , digit
    , delta
    , error
    , terminate
  };
  struct lookup_value
  {
    enc_type type;
    int val;
    int val2;
  };
}
constexpr lookup_value lend() { return { enc_type::terminate, 0, 0 }; }
constexpr lookup_value lnul() { return { enc_type::error, 0, 0 }; }
constexpr lookup_value lval(int x) { return { enc_type::value, x, 0 }; }
constexpr lookup_value lrel(int x) { return { enc_type::digit, x, 0 }; }
constexpr lookup_value ldel(int x, int y) { return { enc_type::delta, x, y }; }
constexpr lookup_value lookup[] =
{
  lend(),     lnul(),      lnul(),      lnul(),      lnul(),      lnul(),      lnul(),      lnul(),      // 00-07
  lnul(),     lnul(),      lend(),      lnul(),      lnul(),      lend(),      lnul(),      lnul(),      // 08-0f
  lnul(),     lnul(),      lnul(),      lnul(),      lnul(),      lnul(),      lnul(),      lnul(),      // 10-17
  lnul(),     lnul(),      lnul(),      lnul(),      lnul(),      lnul(),      lnul(),      lnul(),      // 18-1f
  lnul(),     ldel(-3,-3), lval(16),    lnul(),      ldel(-3,3),  lnul(),      ldel(3, 3),  lval(17),    // 20-27
  ldel(-3,2), ldel(3,2),   lval(18),    ldel(1,0),   lval(19),    ldel(-1,0),  ldel(0,0),   ldel(-3,-2), // 28-2f
  lrel(0),    lrel(1),     lrel(2),     lrel(3),     lrel(4),     lrel(5),     lrel(6),     lrel(7),     // 30-37
  lrel(8),    lrel(9),     lval(20),    lval(21),    ldel(0,-1),  lval(22),    ldel(0,1),   lval(23),    // 38-3f
  ldel(3,-3), lval(0),     lval(1),     lval(2),     lval(3),     lval(4),     lval(5),     lval(6),     // 40-47
  lval(7),    lval(8),     lval(9),     lval(10),    lval(11),    lval(12),    lval(13),    lval(14),    // 48-4f
  lval(15),   lval(24),    lval(25),    ldel(-1,2),  ldel(0,2),   ldel(1,2),   ldel(2,2),   ldel(-1,3),  // 50-57
  ldel(0,3),  ldel(1,3),   lval(26),    ldel(-2,-3), ldel(3,-2),  ldel(2,-3),  lval(27),    lval(28),    // 58-5f
  lnul(),     ldel(-1,-3), ldel(0,-3),  ldel(1,-3),  ldel(-2,-2), ldel(-1,-2), ldel(0,-2),  ldel(1,-2),  // 60-67
  ldel(2,-2), ldel(-3,-1), ldel(-2,-1), ldel(-1,-1), ldel(1,-1),  ldel(2,-1),  ldel(3,-1),  ldel(-3,0),  // 68-6f
  ldel(-2,0), ldel(2,0),   ldel(3,0),   ldel(-3,1),  ldel(-2,1),  ldel(-1,1),  ldel(1,1),   ldel(2,1),   // 70-77
  ldel(3,1),  ldel(-2,2),  lval(29),    ldel(-2,3),  lval(30),    ldel(2,3),   lval(31),    lnul(),      // 78-7f
  lval(32),   lval(33),    lval(34),    lval(35),    lval(36),    lval(37),    lval(38),    lval(39),    // 80-87
  lval(40),   lval(41),    lval(42),    lval(43),    lval(44),    lval(45),    lval(46),    lval(47),    // 88-8f
  lval(48),   lval(49),    lval(50),    lval(51),    lval(52),    lval(53),    lval(54),    lval(55),    // 90-97
  lval(56),   lval(57),    lval(58),    lval(59),    lval(60),    lval(61),    lval(62),    lval(63),    // 98-9f
  lval(64),   lval(65),    lval(66),    lval(67),    lval(68),    lval(69),    lval(70),    lval(71),    // a0-a7
  lval(72),   lval(73),    lval(74),    lval(75),    lval(76),    lval(77),    lval(78),    lval(79),    // a8-af
  lval(80),   lval(81),    lval(82),    lval(83),    lval(84),    lval(85),    lval(86),    lval(87),    // b0-b7
  lval(88),   lval(89),    lval(90),    lval(91),    lval(92),    lval(93),    lval(94),    lval(95),    // b8-bf
  lval(96),   lval(97),    lval(98),    lval(99),    lval(100),   lval(101),   lval(102),   lval(103),   // c0-c7
  lval(104),  lval(105),   lval(106),   lval(107),   lval(108),   lval(109),   lval(110),   lval(111),   // c8-cf
  lval(112),  lval(113),   lval(114),   lval(115),   lval(116),   lval(117),   lval(118),   lval(119),   // d0-d7
  lval(120),  lval(121),   lval(122),   lval(123),   lval(124),   lval(125),   lval(126),   lval(127),   // d8-df
  lval(128),  lval(129),   lval(130),   lval(131),   lval(132),   lval(133),   lval(134),   lval(135),   // e0-e7
  lval(136),  lval(137),   lval(138),   lval(139),   lval(140),   lval(141),   lval(142),   lval(143),   // e8-ef
  lval(144),  lval(145),   lval(146),   lval(147),   lval(148),   lval(149),   lval(150),   lval(151),   // f0-f7
  lval(152),  lval(153),   lval(154),   lval(155),   lval(156),   lval(157),   lval(158),   lval(159)    // f8-ff
};

constexpr char deltas[7][7] =
{
  //  -3   -2   -1    0   +1   +2   +3
    { '!', '[', 'a', 'b', 'c', ']', '@' }   // -3
  , { '/', 'd', 'e', 'f', 'g', 'h', '\\' } // -2
  , { 'i', 'j', 'k', '<', 'l', 'm', 'n' }  // -1
  , { 'o', 'p', '-', '.', '+', 'q', 'r' }  // 0
  , { 's', 't', 'u', '>', 'v', 'w', 'x' }  // +1
  , { '(', 'y', 'S', 'T', 'U', 'V', ')' }  // +2
  , { '$', '{', 'W', 'X', 'Y', '}', '&' }  // +3
};

constexpr uint8_t absolutes[160] =
{
    0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50
  , 0x22, 0x27, 0x2a, 0x2c, 0x3a, 0x3b, 0x3d, 0x3f, 0x51, 0x52, 0x5a, 0x5e, 0x5f, 0x7a, 0x7c, 0x7e
  , 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f
  , 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f
  , 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf
  , 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf
  , 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf
  , 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf
  , 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef
  , 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff
};

static auto find_non_whitespace(uint8_t const* begin, uint8_t const* end) -> uint8_t const*
{
  while (begin != end && *begin <= 0x20)
    ++begin;
  return begin;
}

static auto find_non_whitespace_or_eol(uint8_t const* begin, uint8_t const* end) -> uint8_t const*
{
  while (begin != end && *begin <= 0x20 && *begin != '\n' && *begin != '\r' && *begin != '\0')
    ++begin;
  return begin;
}

static auto starts_with(uint8_t const* begin, uint8_t const* end, string const& str) -> bool
{
  auto b2 = str.c_str(), e2 = str.c_str() + str.size();
  while (begin != end && b2 != e2 && *begin == *b2)
  {
    ++begin;
    ++b2;
  }
  return b2 == e2;
}

static auto find_eol(uint8_t const* begin, uint8_t const* end) -> uint8_t const*
{
  while (begin != end && *begin != '\n' && *begin != '\r' && *begin != '\0')
    ++begin;
  return begin;
}

auto bom::io::rapic::parse_station_id(char const* in) -> int
{
  if (strcasecmp(in, "ANY") == 0)
    return 0;

  int ret = atoi(in);
  if (ret == 0 && in[0] != '0')
    throw std::runtime_error{"invalid station id"};
  return ret;
}

auto bom::io::rapic::parse_scan_type(char const* in) -> pair<scan_type, int>
{
  // is it a numeric equivalent (must match ROWLF definitions - see scantype.cc)
  if (in[0] == '-' || std::isdigit(in[0]))
  {
    int val = std::atoi(in);
    if (val < -1 || val > 7 || (val == 0 && in[0] != '0'))
      throw std::runtime_error{"invalid scan type"};
    return { static_cast<scan_type>(val), -1 };
  }

  // is it a plain type identifier string?
  if (strcasecmp(in, "ANY") == 0)
    return { scan_type::any, -1 };
  if (strcasecmp(in, "PPI") == 0)
    return { scan_type::ppi, -1 };
  if (strcasecmp(in, "RHI") == 0)
    return { scan_type::rhi, -1 };
  if (strcasecmp(in, "CompPPI") == 0)
    return { scan_type::comp_ppi, -1 };
  if (strcasecmp(in, "IMAGE") == 0)
    return { scan_type::image, -1 };
  if (strcasecmp(in, "VOL") == 0)
    return { scan_type::volume, -1 };
  if (strcasecmp(in, "VOLUME") == 0)
    return { scan_type::volume, -1 };
  if (strcasecmp(in, "RHI_SET") == 0)
    return { scan_type::rhi_set, -1 };
  if (strcasecmp(in, "MERGE") == 0)
    return { scan_type::merge, -1 };
  if (strcasecmp(in, "SCAN_ERROR") == 0)
    return { scan_type::scan_error, -1 };

  // is it a VOLUMEXX identifier?
  int volid;
  if (sscanf(in, "VOLUME%d", &volid) == 1)
    return { scan_type::volume, volid };
  if (sscanf(in, "COMPPPI%d", &volid) == 1)
    return { scan_type::comp_ppi, volid };

  throw std::runtime_error{"invalid scan type"};
}

static auto scan_type_string(scan_type type, int volume_id, char buf[16]) -> char const*
{
  switch (type)
  {
  case rapic::scan_type::any:
    return "ANY";
  case rapic::scan_type::ppi:
    return "PPI";
  case rapic::scan_type::rhi:
    return "RHI";
  case rapic::scan_type::comp_ppi:
    if (volume_id == -1)
      return "CompPPI";
    sprintf(buf, "COMPPPI%d", volume_id);
    return buf;
  case rapic::scan_type::image:
    return "IMAGE";
  case rapic::scan_type::volume:
    if (volume_id == -1)
      return "VOLUME";
    sprintf(buf, "VOLUME%d", volume_id);
    return buf;
  case rapic::scan_type::rhi_set:
    return "RHI_SET";
  case rapic::scan_type::merge:
    return "MERGE";
  case rapic::scan_type::scan_error:
    return "SCAN_ERROR";
  }

  __builtin_unreachable(); // LCOV_EXCL_LINE
}

auto bom::io::rapic::parse_query_type(char const* in) -> query_type
{
  if (strcasecmp(in, "LATEST") == 0)
    return query_type::latest;
  if (strcasecmp(in, "TOTIME") == 0)
    return query_type::to_time;
  if (strcasecmp(in, "FROMTIME") == 0)
    return query_type::from_time;
  if (strcasecmp(in, "CENTRETIME") == 0)
    return query_type::center_time;

  throw std::runtime_error{"invalid query type"};
}

static auto query_type_string(query_type type) -> char const*
{
  switch (type)
  {
  case query_type::latest:
    return "LATEST";
  case query_type::to_time:
    return "TOTIME";
  case query_type::from_time:
    return "FROMTIME";
  case query_type::center_time:
    return "CENTRETIME";
  }

  __builtin_unreachable(); // LCOV_EXCL_LINE
}

auto bom::io::rapic::parse_data_types(char const* in) -> vector<string>
{
  if (strcasecmp(in, "ANY") == 0)
    return {};
  return tokenize<string>(in, ",");
}

auto bom::io::rapic::parse_volumetric_header(string const& product) -> timestamp
{
  // use out-of-bounts mday to convert day of year into correct day
  struct tm t;
  if (sscanf(product.c_str(), "VOLUMETRIC [%02d%02d%03d%02d]", &t.tm_hour, &t.tm_min, &t.tm_mday, &t.tm_year) != 4)
    throw std::runtime_error{"invalid PRODUCT header"};
  t.tm_sec = 0;
  t.tm_mon = 0; // january
  if (t.tm_year < 70) // cope with two digit year, convert to years since 1900
    t.tm_year += 100;
  t.tm_isdst = -1;
  return timestamp{timegm(&t)};
}

auto bom::io::rapic::parse_timestamp_header(char const* str) -> timestamp
{
  struct tm t;
  if (sscanf(str, "%04d%02d%02d%02d%02d%02d", &t.tm_year, &t.tm_mon, &t.tm_mday, &t.tm_hour, &t.tm_min, &t.tm_sec) != 6)
    throw std::runtime_error{"invalid rapic timestamp"};
  t.tm_year -= 1900;
  t.tm_mon -= 1;
  return timestamp{timegm(&t)};
}

decode_error::decode_error(message_type type, uint8_t const* in, size_t size)
  : std::runtime_error{"TODO"}
{ }

buffer::buffer(size_t size, size_t max_size)
  : data_{size}
  , wpos_{0}
  , rpos_{0}
  , max_size_{max_size}
{ }

buffer::buffer(buffer const& rhs)
  : data_{rhs.data_.size()}
  , wpos_{rhs.wpos_ - rhs.rpos_}
  , rpos_{0}
  , max_size_{rhs.max_size_}
{
  for (size_t i = 0; i < wpos_; ++i)
    data_[i] = rhs.data_[rhs.rpos_ + i];
}

auto buffer::operator=(buffer const& rhs) -> buffer&
{
  if (data_.size() != rhs.data_.size())
    data_.resize(rhs.data_.size());

  // no exceptions beyond here
  wpos_ = rhs.wpos_ - rhs.rpos_;
  rpos_ = 0;
  max_size_ = rhs.max_size_;
  for (size_t i = 0; i < wpos_; ++i)
    data_[i] = rhs.data_[rhs.rpos_ + i];

  return *this;
}

auto buffer::resize(size_t size) -> void
{
  if (size < wpos_ - rpos_)
    throw std::logic_error{"rapic buffer resize would corrupt data stream"};

  if (size == data_.size())
    return;

  auto tmp = array1<uint8_t>{size};
  for (size_t i = 0; i < wpos_ - rpos_; ++i)
    tmp[i] = data_[rpos_ + i];

  data_ = std::move(tmp);
  wpos_ -= rpos_;
  rpos_ = 0;
}

auto buffer::optimize() -> void
{
  if (rpos_ != 0)
  {
    for (size_t i = 0; i < wpos_ - rpos_; ++i)
      data_[i] = data_[rpos_ + i];
    wpos_ -= rpos_;
    rpos_ = 0;
  }
}

auto buffer::size() const -> size_t
{
  return data_.size();
}

auto buffer::clear() -> void
{
  wpos_ = 0;
  rpos_ = 0;
}

auto buffer::write_acquire(size_t min_space) -> pair<uint8_t*, size_t>
{
  // expand or shuffle to ensure min_space requirement is met
  auto space = data_.size() - wpos_;
  if (space < min_space)
  {
    auto min_size = wpos_ - rpos_ + min_space;
    if (min_size > max_size_)
      throw std::runtime_error{"rapic: allocating requested write space would exceed maximum buffer size"};
    if (space + rpos_ < min_space)
      resize(std::max(data_.size() * 2, min_size));
    else
      optimize();
    space = data_.size() - wpos_;
  }
  /* if min_space is 0 and wpos_ hits the end then force a shuffle.  without this fixed size buffers (i.e. those
   * for which the user always specifies min_space == 0) which hit the fill point part way through a message will
   * never have a chance to clear themselves because they will never perform a read_advance(). */
  else if (space == 0)
    optimize();
  return {&data_[wpos_], space};
}

auto buffer::write_advance(size_t len) -> void
{
  if (wpos_ + len > data_.size())
    throw std::out_of_range{"rapic buffer overflow detected on write operation"};
  wpos_ += len;
}

auto buffer::read_acquire() const -> pair<uint8_t const*, size_t>
{
  return {&data_[rpos_], wpos_ - rpos_};
}

auto buffer::read_advance(size_t len) -> void
{
  if (len > wpos_ - rpos_)
    throw std::out_of_range{"rapic buffer overflow detected on read operation"};
  rpos_ += len;
  if (rpos_ == wpos_)
    rpos_ = wpos_ = 0;
}

auto buffer::read_detect(message_type& type, size_t& len) const -> bool
{
  uint8_t const* pos = &data_[rpos_];
  uint8_t const* end = &data_[wpos_];
  uint8_t const* nxt;
  message_type msg = no_message;

  // ignore leading whitespace (and return if no data at all)
  if ((pos = find_non_whitespace(pos, end)) == end)
    return false;

  // is it a comment (i.e. IMAGE header)?
  if (starts_with(pos, end, msg_comment_head))
  {
    if ((nxt = find_eol(pos, end)) != end)
      msg = message_type::comment;
  }
  // is it an MSSG 30 style message? (must check mssg30 before mssg as mssg header is a subset of mssg30 header)
  else if (starts_with(pos, end, msg_mssg30_head))
  {
    // status 30 is multi-line terminated by "END STATUS"
    pos += msg_mssg30_head.size();
    while ((nxt = find_eol(pos, end)) != end)
    {
      if (   size_t(nxt - pos) == msg_mssg30_term.size()
          && msg_mssg30_term.compare(0, msg_mssg30_term.size(), reinterpret_cast<char const*>(pos), nxt - pos) == 0)
      {
        msg = message_type::mssg;
        break;
      }
      pos = nxt + 1;
    }
  }
  // is it an MSSG style message?
  else if (starts_with(pos, end, msg_mssg_head))
  {
    if ((nxt = find_eol(pos, end)) != end)
      msg = message_type::mssg;
  }
  // is it an RDRSTAT message?
  else if (starts_with(pos, end, msg_status_head))
  {
    if ((nxt = find_eol(pos, end)) != end)
      msg = message_type::status;
  }
  // is it a SEMIPERMANENT CONNECTION message? (must check this before RPQUERY due to header similarity)
  else if (starts_with(pos, end, msg_permcon_head))
  {
    if ((nxt = find_eol(pos, end)) != end)
      msg = message_type::permcon;
  }
  // is it a RPQUERY style message?
  else if (starts_with(pos, end, msg_query_head))
  {
    if ((nxt = find_eol(pos, end)) != end)
      msg = message_type::query;
  }
  // is it an RPFILTER styles message?
  else if (starts_with(pos, end, msg_filter_head))
  {
    if ((nxt = find_eol(pos, end)) != end)
      msg = message_type::filter;
  }
  // otherwise assume it is a scan message and look for "END RADAR IMAGE"
  else
  {
    while ((nxt = find_eol(pos, end)) != end)
    {
      // the "END RADAR IMAGE" token is _sometimes_ prefixed with a ^Z (0x28), so skip leading whitespace
      if (   (pos = find_non_whitespace(pos, nxt)) != nxt
          && size_t(nxt - pos) == msg_scan_term.size()
          && msg_scan_term.compare(0, msg_scan_term.size(), reinterpret_cast<char const*>(pos), nxt - pos) == 0)
      {
        msg = message_type::scan;
        break;
      }
      pos = nxt + 1;
    }
  }

  // did we manage to locate a message?
  if (msg != no_message)
  {
    type = msg;
    len = nxt + 1 - &data_[rpos_];
    return true;
  }

  return false;
}

message::~message()
{ }

comment::comment()
{
  reset();
}

comment::comment(buffer const& in)
{
  reset();
  decode(in);
}

auto comment::type() const -> message_type
{
  return message_type::comment;
}

auto comment::reset() -> void
{
  text_.clear();
}

auto comment::encode(buffer& out) const -> void
{
  auto wa = out.write_acquire(text_.size() + 8);
  auto ret = snprintf(reinterpret_cast<char*>(wa.first), wa.second, "/%s\n", text_.c_str());
  if (ret < 0 || size_t(ret) >= wa.second)
    throw std::runtime_error{"rapic: failed to encode message"};
  out.write_advance(ret);
}

auto comment::decode(buffer const& in) -> void
{
  auto ra = in.read_acquire();
  auto pos = ra.first, end = ra.first + ra.second;

  // skip leading whitespace
  if ((pos = find_non_whitespace(pos, end)) == end)
    throw std::runtime_error{"failed to parse message header"};

  // read the message identifier and number
  ssize_t len = 0;
  if (sscanf(reinterpret_cast<char const*>(pos), "/%zn", &len) != 0 || len == 0)
    throw std::runtime_error{"failed to parse message header"};
  pos += len;
  if (pos >= end)
    throw std::runtime_error{"read buffer overflow"};

  // remainder of line is the message text
  decltype(pos) eol;
  if ((eol = find_eol(pos, end)) == end)
    throw std::runtime_error{"read buffer overflow"};
  text_.assign(reinterpret_cast<char const*>(pos), eol - pos);
  pos = eol + 1;
}

mssg::mssg()
{
  reset();
}

mssg::mssg(buffer const& in)
{
  reset();
  decode(in);
}

auto mssg::type() const -> message_type
{
  return message_type::mssg;
}

auto mssg::reset() -> void
{
  number_ = -1;
  text_.clear();
}

auto mssg::encode(buffer& out) const -> void
{
  auto wa = out.write_acquire(text_.size() + 32);
  auto ret = snprintf(
        reinterpret_cast<char*>(wa.first)
      , wa.second
      , number_ == 30 ? "MSSG: %d %s\nEND STATUS\n" : "MSSG: %d %s\n"
      , number_
      , text_.c_str());
  if (ret < 0 || size_t(ret) >= wa.second)
    throw std::runtime_error{"rapic: failed to encode message"};
  out.write_advance(ret);
}

auto mssg::decode(buffer const& in) -> void
{
  auto ra = in.read_acquire();
  auto pos = ra.first, end = ra.first + ra.second;

  // skip leading whitespace
  if ((pos = find_non_whitespace(pos, end)) == end)
    throw std::runtime_error{"failed to parse message header"};

  // read the message identifier and number
  ssize_t len = 0;
  if (sscanf(reinterpret_cast<char const*>(pos), "MSSG: %d%zn", &number_, &len) != 1 || len == 0)
    throw std::runtime_error{"failed to parse message header"};
  pos += len;
  if (pos >= end)
    throw std::runtime_error{"read buffer overflow"};

  // skip whitepsace between the number and text
  if ((pos = find_non_whitespace_or_eol(pos, end)) == end)
    throw std::runtime_error{"read buffer overflow"};

  // remainder of line is the message text
  decltype(pos) eol;
  if ((eol = find_eol(pos, end)) == end)
    throw std::runtime_error{"read buffer overflow"};
  text_.assign(reinterpret_cast<char const*>(pos), eol - pos);
  pos = eol + 1;

  // handle multi-line messages (only #30)
  if (number_ == 30)
  {
    while ((eol = find_eol(pos, end)) != end)
    {
      if (   size_t(eol - pos) == msg_mssg30_term.size()
          && msg_mssg30_term.compare(0, msg_mssg30_term.size(), reinterpret_cast<char const*>(pos), eol - pos) == 0)
        break;
      text_.push_back('\n');
      text_.append(reinterpret_cast<char const*>(pos), eol - pos);
      pos = eol + 1;
    }
    if (eol == end)
      throw std::runtime_error{"read buffer overflow"};
  }
}

status::status()
{
  reset();
}

status::status(buffer const& in)
{
  reset();
  decode(in);
}

auto status::type() const -> message_type
{
  return message_type::status;
}

auto status::reset() -> void
{
  text_.clear();
}

auto status::encode(buffer& out) const -> void
{
  auto wa = out.write_acquire(text_.size() + 16);
  auto ret = snprintf(reinterpret_cast<char*>(wa.first), wa.second, "RDRSTAT: %s\n", text_.c_str());
  if (ret < 0 || size_t(ret) >= wa.second)
    throw std::runtime_error{"rapic: failed to encode message"};
  out.write_advance(ret);
}

auto status::decode(buffer const& in) -> void
{
  auto ra = in.read_acquire();
  auto pos = ra.first, end = ra.first + ra.second;

  // skip leading whitespace
  if ((pos = find_non_whitespace(pos, end)) == end)
    throw std::runtime_error{"failed to parse message header"};

  // read the message identifier and number
  ssize_t len = 0;
  if (sscanf(reinterpret_cast<char const*>(pos), "RDRSTAT:%zn", &len) != 0 || len == 0)
    throw std::runtime_error{"failed to parse message header"};
  pos += len;
  if (pos >= end)
    throw std::runtime_error{"read buffer overflow"};

  // skip whitepsace between the number and text
  if ((pos = find_non_whitespace_or_eol(pos, end)) == end)
    throw std::runtime_error{"read buffer overflow"};

  // remainder of line is the message text
  decltype(pos) eol;
  if ((eol = find_eol(pos, end)) == end)
    throw std::runtime_error{"read buffer overflow"};
  text_.assign(reinterpret_cast<char const*>(pos), eol - pos);
  pos = eol + 1;
}

permcon::permcon()
{
  reset();
}

permcon::permcon(buffer const& in)
{
  reset();
  decode(in);
}

auto permcon::type() const -> message_type
{
  return message_type::permcon;
}

auto permcon::reset() -> void
{
  tx_complete_scans_ = false;
}

auto permcon::encode(buffer& out) const -> void
{
  auto wa = out.write_acquire(96);
  auto ret = snprintf(
        reinterpret_cast<char*>(wa.first)
      , wa.second
      , "RPQUERY: SEMIPERMANENT CONNECTION - SEND ALL DATA TXCOMPLETESCANS=%d\n"
      , tx_complete_scans_);
  if (ret < 0 || size_t(ret) >= wa.second)
    throw std::runtime_error{"rapic: failed to encode message"};
  out.write_advance(ret);
}

auto permcon::decode(buffer const& in) -> void
{
  auto ra = in.read_acquire();
  auto pos = ra.first, end = ra.first + ra.second;

  // skip leading whitespace
  if ((pos = find_non_whitespace(pos, end)) == end)
    throw std::runtime_error{"failed to parse message header"};

  // read the message identifier and number
  int ival;
  ssize_t len = 0;
  auto ret = sscanf(
        reinterpret_cast<char const*>(pos)
      , "RPQUERY: SEMIPERMANENT CONNECTION - SEND ALL DATA TXCOMPLETESCANS=%d%zn"
      , &ival
      , &len);
  if (ret != 1 || len == 0)
    throw std::runtime_error{"failed to parse message header"};
  tx_complete_scans_ = ival != 0;
  pos += len;
  if (pos >= end)
    throw std::runtime_error{"read buffer overflow"};

  // find the end of the line (discard any extra text)
  if ((pos = find_eol(pos, end)) == end)
    throw std::runtime_error{"read buffer overflow"};
  ++pos;
}

query::query()
{
  reset();
}

query::query(buffer const& in)
{
  reset();
  decode(in);
}

auto query::type() const -> message_type
{
  return message_type::query;
}

auto query::reset() -> void
{
  station_id_ = 0;
  scan_type_ = rapic::scan_type::any;
  volume_id_ = -1;
  angle_ = -1.0f;
  repeat_count_ = -1;
  query_type_ = rapic::query_type::latest;
  time_ = 0;
  data_types_.clear();
  video_res_ = -1;
}

auto query::encode(buffer& out) const -> void
{
  // determine maximum needed buffer space
  size_t limit = 9 + 21 + 21 + 6 + 11 + 21 + 6;
  for (auto& type : data_types_)
    limit += type.size() + 1;

  // build the datatypes string list
  char str_dtype[256];
  {
    auto ptr = &str_dtype[0];
    for (auto& type : data_types_)
    {
      strcpy(ptr, type.c_str());
      ptr += type.size();
      *ptr++ = ',';
    }
    *(ptr - 1) = '\0';
  }

  // write the message
  char stypebuf[16];
  auto wa = out.write_acquire(limit);
  auto ret = snprintf(
        reinterpret_cast<char*>(wa.first)
      , wa.second
      , "RPQUERY: %d %s %g %d %s %ld %s %d\n"
      , station_id_
      , scan_type_string(scan_type_, volume_id_, stypebuf)
      , angle_
      , repeat_count_
      , query_type_string(query_type_)
      , time_
      , str_dtype
      , video_res_);
  if (ret < 0 || size_t(ret) >= wa.second)
    throw std::runtime_error{"rapic: failed to encode message"};
  out.write_advance(ret);
}

auto query::decode(buffer const& in) -> void
{
  auto ra = in.read_acquire();
  auto pos = ra.first, end = ra.first + ra.second;

  // skip leading whitespace
  if ((pos = find_non_whitespace(pos, end)) == end)
    throw std::runtime_error{"failed to parse message header"};

  // read the header
  long time;
  char str_stn[21], str_stype[21], str_qtype[21], str_dtype[128];
  ssize_t len = 0;
  auto ret = sscanf(
        reinterpret_cast<char const*>(pos)
      , "RPQUERY: %20s %20s %f %d %20s %ld %127s %d%zn"
      , str_stn
      , str_stype
      , &angle_
      , &repeat_count_
      , str_qtype
      , &time
      , str_dtype
      , &video_res_
      , &len);
  if (ret != 8 || len == 0)
    throw std::runtime_error{"failed to parse message header"};
  pos += len;
  if (pos >= end)
    throw std::runtime_error{"read buffer overflow"};

  // check/parse the individual tokens
  station_id_ = parse_station_id(str_stn);
  std::tie(scan_type_, volume_id_) = parse_scan_type(str_stype);
  query_type_ = parse_query_type(str_qtype);
  time_ = time;
  data_types_ = parse_data_types(str_dtype);

  // find the end of the line (discard any extra text)
  if ((pos = find_eol(pos, end)) == end)
    throw std::runtime_error{"read buffer overflow"};
  ++pos;
}

filter::filter()
{
  reset();
}

filter::filter(buffer const& in)
{
  reset();
  decode(in);
}

auto filter::type() const -> message_type
{
  return message_type::filter;
}

auto filter::reset() -> void
{
  station_id_ = 0;
  scan_type_ = rapic::scan_type::any;
  volume_id_ = -1;
  video_res_ = -1;
  source_.clear();
  data_types_.clear();
}

auto filter::encode(buffer& out) const -> void
{
  // determine maximum needed buffer space
  size_t limit = 13 + 3 + 8 + 3 + 8;
  for (auto& type : data_types_)
    limit += type.size() + 1;

  // build the datatypes string list
  char str_dtype[256];
  if (data_types_.empty())
    strcpy(str_dtype, "ANY");
  else
  {
    auto ptr = &str_dtype[0];
    for (auto& type : data_types_)
    {
      strcpy(ptr, type.c_str());
      ptr += type.size();
      *ptr++ = ',';
    }
    *(ptr - 1) = '\0';
  }

  // write the message
  char stypebuf[16];
  auto wa = out.write_acquire(limit);
  auto ret = snprintf(
        reinterpret_cast<char*>(wa.first)
      , wa.second
      , "RPFILTER:%d:%s:%d:%s:%s\n"
      , station_id_
      , scan_type_string(scan_type_, volume_id_, stypebuf)
      , video_res_
      , source_.c_str()
      , str_dtype);
  if (ret < 0 || size_t(ret) >= wa.second)
    throw std::runtime_error{"rapic: failed to encode message"};
  out.write_advance(ret);
}

auto filter::decode(buffer const& in) -> void
{
  auto ra = in.read_acquire();
  auto pos = ra.first, end = ra.first + ra.second;

  // skip leading whitespace
  if ((pos = find_non_whitespace(pos, end)) == end)
    throw std::runtime_error{"failed to parse message header"};

  // read the header
  char str_stn[21], str_stype[21], str_src[21], str_dtype[256];
  str_src[0] = '\0';
  ssize_t len = 0;
  auto ret = sscanf(
        reinterpret_cast<char const*>(pos)
      , "RPFILTER:%20[^:]:%20[^:]:%d:%20[^:]:%255s%zn"
      , str_stn
      , str_stype
      , &video_res_
      , str_src
      , str_dtype
      , &len);
  if (ret != 5 || len == 0)
    throw std::runtime_error{"failed to parse message header"};
  pos += len;
  if (pos >= end)
    throw std::runtime_error{"read buffer overflow"};

  // check/parse the individual tokens
  station_id_ = parse_station_id(str_stn);
  std::tie(scan_type_, volume_id_) = parse_scan_type(str_stype);
  source_.assign(str_src);
  data_types_ = parse_data_types(str_dtype);

  // find the end of the line (discard any extra text)
  if ((pos = find_eol(pos, end)) == end)
    throw std::runtime_error{"read buffer overflow"};
  ++pos;
}

auto scan::header::get_boolean() const -> bool
{
  if (   strcasecmp(value_.c_str(), "true") == 0
      || strcasecmp(value_.c_str(), "on") == 0
      || strcasecmp(value_.c_str(), "yes") == 0
      || strcasecmp(value_.c_str(), "1") == 0)
    return true;

  if (   strcasecmp(value_.c_str(), "false") == 0
      || strcasecmp(value_.c_str(), "off") == 0
      || strcasecmp(value_.c_str(), "no") == 0
      || strcasecmp(value_.c_str(), "0") == 0)
    return false;

  throw std::runtime_error{"bad boolean value"};
}

auto scan::header::get_integer() const -> long
{
  return from_string<long>(value_, 10);
}

auto scan::header::get_real() const -> double
{
  return from_string<double>(value_);
}

auto scan::header::get_integer_array() const -> vector<long>
{
  return tokenize<long>(value_, " ", 10);
}

auto scan::header::get_real_array() const -> vector<double>
{
  return tokenize<double>(value_, " ");
}

scan::scan()
{
  reset();
}

scan::scan(buffer const& in)
{
  reset();
  decode(in);
}

scan::scan(vector<header> headers, vector<ray_header> ray_headers, array2<uint8_t> level_data)
  : headers_{std::move(headers)}
  , ray_headers_{std::move(ray_headers)}
  , level_data_{std::move(level_data)}
{
  initialize_rays();
}

auto scan::type() const -> message_type
{
  return message_type::scan;
}

auto scan::reset() -> void
{
  headers_.clear();
  ray_headers_.clear();
  level_data_.resize(vec2z{0, 0});

  station_id_ = -1;
  volume_id_ = -1;
  product_.clear();
  pass_ = -1;
  pass_count_ = -1;
  is_rhi_ = false;
  angle_min_ = nan<float>();
  angle_max_ = nan<float>();
  angle_resolution_ = nan<float>();
}

static inline auto check_ele_angle(float angle) -> float
{
  // don't invert this logic - it is this way so that it works for nans
  if (!(angle >= 0.0f && angle <= 90.0f))
    throw std::runtime_error{fmt::format("invalid elevation angle {}", angle)};
  return angle;
}

// round angle to nearest 1 degree and normalize to [0, 359]
static inline auto check_azi_angle_ascii(float angle) -> float
{
  /* rapic spec requires azimuth normalized to range [0,359] in ASCII mode so we must round to the desired
   * precision and then mod 360.  we cannot rely on printf rounding because it will round 259.9 up to 360. */
  if (is_nan(angle))
    throw std::runtime_error{"invalid azimuth angle"};
  while (angle < 0.0f)
    angle += 360.0f;
  return std::fmod(round_to_precision<std::ratio<1>>(angle), 360.0f);
}

// round angle to nearest 0.1 degree and normalize to [0, 359.9]
static inline auto check_azi_angle_binary(float angle) -> float
{
  /* rapic spec _technically_ doesn't require azimuth to be in range [0,359] when in binary mode but we
   * do it anyway since downstream applications have been written and tested against ASCII mode only */
  if (is_nan(angle))
    throw std::runtime_error{"invalid azimuth angle"};
  while (angle < 0.0f)
    angle += 360.0f;
  return std::fmod(round_to_precision<std::deci>(angle), 360.0f);
}

auto scan::encode(buffer& out) const -> void
{
  // sanity check
  if (ray_headers_.size() > rays())
    throw std::runtime_error{fmt::format("ray header count {} exceeds ray count {}", ray_headers_.size(), rays())};

  // determine the maximum amount of memory that could be required to encode the scan
  // assumes rays are encoded as 256 level alternating level 0 and 1 which is worst case
  size_t limit = 0;
  for (auto& header : headers_)
    limit += header.name().size() + header.value().size() + 2;  // 2 for ':' + '\n'
  limit += rays() * (bins() * 2 + 18);                          // 2 for worst case rle, 18 for ray header
  limit += msg_scan_term.size() + 3;                            // 3 for leading 0x1a 0x20 and final '\n'

  // acquire the worst case memory block from our buffer so we don't have to check buffer capacity after every write
  auto wa = out.write_acquire(limit);
  auto pos = wa.first;

  // write the headers
  for (auto& header : headers_)
  {
    strcpy(reinterpret_cast<char*>(pos), header.name().c_str());
    pos += header.name().size();
    *pos++ = ':';
    *pos++ = ' ';
    strcpy(reinterpret_cast<char*>(pos), header.value().c_str());
    pos += header.value().size();
    *pos++ = '\n';
  }

  // determine the video resolution
  auto vidres = 0;
  for (auto& header : headers_)
    if (header.name() == "VIDRES")
      vidres = header.get_integer();

  // write the rays
  if (vidres == 16 || vidres == 32 || vidres == 64 || vidres == 160)
  {
    for (size_t ray = 0; ray < ray_headers_.size(); ++ray)
    {
      auto ray_data = level_data_[ray];

      // ascii ray header
      if (is_rhi_)
        pos += sprintf(reinterpret_cast<char*>(pos), "%%%02.1f", check_ele_angle(ray_headers_[ray].elevation()));
      else
        pos += sprintf(reinterpret_cast<char*>(pos), "%%%03.0f", check_azi_angle_ascii(ray_headers_[ray].azimuth()));

      // always encode first bin as an absolute value
      if (bins() > 0)
      {
        if (ray_data[0] >= vidres)
          throw std::runtime_error{"invalid level detected while encoding"};
        *pos++ = absolutes[ray_data[0]];
      }

      // encode the bins
      size_t bin = 1;
      while (bin + 1 < bins())
      {
        auto count = 0;
        while (bin + count < bins() && ray_data[bin + count] == ray_data[bin - 1])
          ++count;
        if (count > 2)
        {
          pos += sprintf(reinterpret_cast<char*>(pos), "%d", count);
          bin += count;
          continue;
        }

        // try delta encoding (50% compression)
        auto delta0 = int(ray_data[bin]) - int(ray_data[bin - 1]);
        auto delta1 = int(ray_data[bin + 1]) - int(ray_data[bin]);
        if (std::abs(delta0) <= 3 && std::abs(delta1) <= 3)
        {
          // '+3' to adjust deltas into row/col index
          *pos++ = deltas[delta1 + 3][delta0 + 3];
          bin += 2;
          continue;
        }

        // sanity check the actual level values
        if (ray_data[bin] >= vidres)
          throw std::runtime_error{"invalid level detected while encoding"};

        // can't compress - use absolute encoding (no compression)
        *pos++ = absolutes[ray_data[bin]];
        ++bin;
      }

      // above loop may stop short by one bin this is to prevent delta encoding from overflowing the array due
      // to the second half of a delta pair being past the end of the ray.  if we stopped short just encode
      // the final bin using an absolute value
      if (bin + 1 == bins())
      {
        if (ray_data[bin] >= vidres)
          throw std::runtime_error{"invalid level detected while encoding"};
        *pos++ = absolutes[ray_data[bin]];
      }

      // terminating new line
      *pos++ = '\n';
    }
  }
  else if (vidres == 256)
  {
    for (size_t ray = 0; ray < ray_headers_.size(); ++ray)
    {
      // binary ray header
      pos += sprintf(
            reinterpret_cast<char*>(pos)
          , "@%05.1f,%05.1f,%03d="
          , check_azi_angle_binary(ray_headers_[ray].azimuth())
          , check_ele_angle(ray_headers_[ray].elevation())
          , ray_headers_[ray].time_offset());

      // leave space for the count
      auto len_pos = pos;
      pos += 2;

      // encode the bins
      size_t bin = 0;
      auto ray_data = level_data_[ray];
      while (bin < bins())
      {
        auto val = ray_data[bin];
        if (val == 0 || val == 1)
        {
          *pos++ = val;
          auto limit = std::min(size_t{255}, bins() - bin);
          auto count = size_t{1};
          while (count < limit && ray_data[bin + count] == val)
            ++count;
          *pos++ = count;
          bin += count;
        }
        else
        {
          *pos++ = val;
          ++bin;
        }
      }
      *pos++ = 0;
      *pos++ = 0;

      // fill the count now that we know what it is
      auto ray_len = pos - len_pos - 2;
      len_pos[0] = (ray_len >> 8) & 0xff;
      len_pos[1] = (ray_len >> 0) & 0xff;
    }
  }
  else
    throw std::runtime_error{"unsupported or unknown video resolution"};

  // write the terminator
  *pos++ = 0x1a; // ^Z as dictated by rapic spec
  *pos++ = 0x20; // space as dictated by rapic spec
  strcpy(reinterpret_cast<char*>(pos), msg_scan_term.c_str());
  pos += msg_scan_term.size();
  *pos++ = '\n';

  // commit the encoded message to the buffer (will also sanity check for overflow)
  out.write_advance(pos - wa.first);
}

auto scan::decode(buffer const& in) -> void
try
{
  auto ra = in.read_acquire();
  auto dat = ra.first;
  auto size = ra.second;

  reset();

  for (size_t pos = 0; pos < size; ++pos)
  {
    auto next = dat[pos];

    // ascii encoded ray
    if (next == '%')
    {
      ++pos;

      // if this is our first ray, setup the data structures
      if (ray_headers_.empty())
        initialize_rays();

      // sanity check that we don't have too many rays
      if (ray_headers_.size() == rays())
        throw std::runtime_error{"scan data overflow (too many rays)"};

      // sanity check that we have enough space for at least the header
      if (pos + 4 >= size)
        throw std::runtime_error{"corrupt scan detected (1)"};

      // determine the ray angle
      float angle;
      if (sscanf(reinterpret_cast<char const*>(&dat[pos]), is_rhi_ ? "%4f" : "%3f", &angle) != 1)
        throw std::runtime_error{"invalid ascii ray header"};
      pos += is_rhi_ ? 4 : 3;

      // create the ray entry
      if (is_rhi_)
        ray_headers_.emplace_back(nan<float>(), angle, -1);
      else
        ray_headers_.emplace_back(angle, nan<float>(), -1);

      // decode the data into levels
      auto out = level_data_[ray_headers_.size() - 1];
      int prev = 0;
      size_t bin = 0;
      while (pos < size)
      {
        auto& cur = lookup[dat[pos++]];

        //  absolute pixel value
        if (cur.type == enc_type::value)
        {
          if (bin < bins())
            out[bin++] = prev = cur.val;
          else
            throw std::runtime_error{"scan data overflow (ascii abs)"};
        }
        // run length encoding of the previous value
        else if (cur.type == enc_type::digit)
        {
          auto count = cur.val;
          while (pos < size && lookup[dat[pos]].type == enc_type::digit)
          {
            count *= 10;
            count += lookup[dat[pos++]].val;
          }
          if (bin + count > bins())
            throw std::runtime_error{"scan data overflow (ascii rle)"};
          for (int i = 0; i < count; ++i)
            out[bin++] = prev;
        }
        // delta encoding
        // silently ignore potential overflow caused by second half of a delta encoding at end of ray
        // we assume it is just an artefact of the encoding process
        else if (cur.type == enc_type::delta)
        {
          if (bin < bins())
            out[bin++] = prev += cur.val;
          else
            throw std::runtime_error{"scan data overflow (ascii delta)"};

          if (bin < bins())
            out[bin++] = prev += cur.val2;
          else if (pos < size && lookup[dat[pos]].type != enc_type::terminate)
            throw std::runtime_error{"scan data overflow (ascii delta)"};
        }
        // null or end of line character - end of radial
        else if (cur.type == enc_type::terminate)
        {
          /* hack to work around extra newline characters that corrupt the data stream of some radars
           * (looking at you Dampier).  if we ever have headers appear in the file after rays then this
           * will break. */
          {
            auto i = pos;
            while (i < size && dat[i] <= ' ')
              ++i;
            if (   i < size
                && dat[i] != '%'
                && size - i >= msg_scan_term.size()
                && strncmp(reinterpret_cast<char const*>(&dat[i]), msg_scan_term.c_str(), msg_scan_term.size()) != 0)
              continue;
          }

          --pos;
          break;
        }
        else
          throw std::runtime_error{"invalid character encountered in ray encoding"};
      }
    }
    // binary encoding
    else if (next == '@')
    {
      ++pos;

      // if this is our first ray, setup the data structures
      if (ray_headers_.empty())
        initialize_rays();

      // sanity check that we don't have too many rays
      if (ray_headers_.size() == rays())
        throw std::runtime_error{"scan data overflow (too many rays)"};

      // sanity check that we have enough space for at least the header
      if (pos + 18 >= size)
        throw std::runtime_error{"corrupt scan detected (2)"};

      // read the ray header
      float azi, el;
      int sec;
      if (sscanf(reinterpret_cast<char const*>(&dat[pos]), "%f,%f,%d=", &azi, &el, &sec) != 3)
        throw std::runtime_error("invalid binary ray header");
      // note: we ignore the length for now
      //auto len = (((unsigned int) dat[16]) << 8) + ((unsigned int) dat[17]);
      pos += 18;

      // create the ray entry
      ray_headers_.emplace_back(azi, el, sec);

      // decode the data into levels
      auto out = level_data_[ray_headers_.size() - 1];
      size_t bin = 0;
      while (true)
      {
        int val = dat[pos++];
        if (val == 0 || val == 1)
        {
          int count = dat[pos++];
          if (count == 0)
          {
            --pos;
            break;
          }
          if (bin + count > bins())
            throw std::runtime_error{"scan data overflow (binary rle)"};
          for (int i = 0; i < count; ++i)
            out[bin++] = val;
        }
        else if (bin < bins())
          out[bin++] = val;
        else
          throw std::runtime_error{"scan data overflow (binary abs)"};
      }
    }
    // header field
    else if (next > ' ')
    {
      size_t pos2, pos3, pos4;

      // find the end of the header name
      for (pos2 = pos + 1; pos2 < size; ++pos2)
        if (dat[pos2] < ' ' || dat[pos2] == ':')
          break;

      // check for end of scan or corruption
      if (pos2 >= size || dat[pos2] != ':')
      {
        // valid end of scan?
        if (   pos2 - pos == msg_scan_term.size()
            && strncmp(reinterpret_cast<char const*>(&dat[pos]), msg_scan_term.c_str(), msg_scan_term.size()) == 0)
          return;
        throw std::runtime_error{"corrupt scan detected (3)"};
      }

      // find the start of the header value
      for (pos3 = pos2 + 1; pos3 < size; ++pos3)
        if (dat[pos3] > ' ')
          break;

      // check for corruption
      if (pos3 == size)
        throw std::runtime_error{"corrupt scan detected (4)"};

      // find the end of the header value
      for (pos4 = pos3 + 1; pos4 < size; ++pos4)
        if (dat[pos4] < ' ') // note: spaces are valid characters in the header value
          break;

      // store the header
      headers_.emplace_back(
            string(reinterpret_cast<char const*>(&dat[pos]), pos2 - pos)
          , string(reinterpret_cast<char const*>(&dat[pos3]), pos4 - pos3));

      // advance past the header line
      pos = pos4;
    }
    // else whitespace - skip
  }

  throw std::runtime_error{"corrupt scan detected (5)"};
}
catch (std::exception& err)
{
  auto desc = "failed to decode scan"s;
  if (auto p = find_header("STNID"))
    desc.append(" stnid: ").append(p->value());
  if (auto p = find_header("NAME"))
    desc.append(" name: ").append(p->value());
  if (auto p = find_header("PRODUCT"))
    desc.append(" product: ").append(p->value());
  if (auto p = find_header("TILT"))
    desc.append(" tilt: ").append(p->value());
  if (auto p = find_header("PASS"))
    desc.append(" pass: ").append(p->value());
  if (auto p = find_header("VIDEO"))
    desc.append(" video: ").append(p->value());
  std::throw_with_nested(std::runtime_error{desc});
}

auto scan::find_header(char const* name) const -> header const*
{
  for (auto& h : headers_)
    if (h.name() == name)
      return &h;
  return nullptr;
}

auto scan::find_or_insert_header(char const* name) -> header&
{
  for (auto& h : headers_)
    if (h.name() == name)
      return h;
  headers_.emplace_back(name);
  return headers_.back();
}

auto scan::get_header_string(char const* name) const -> string const&
{
  if (auto p = find_header(name))
    return p->value();
  throw std::runtime_error{"missing mandatory header "s + name};
}

auto scan::get_header_integer(char const* name) const -> long
{
  if (auto p = find_header(name))
    return p->get_integer();
  throw std::runtime_error{"missing mandatory header "s + name};
}

auto scan::get_header_real(char const* name) const -> double
{
  if (auto p = find_header(name))
    return p->get_real();
  throw std::runtime_error{"missing mandatory header "s + name};
}

auto scan::initialize_rays() -> void
{
  // if this is our first ray, setup the data array

  // store the header fields which we cache
  station_id_ = get_header_integer("STNID");
  if (auto p = find_header("VOLUMEID"))
    volume_id_ = p->get_integer();
  product_ = get_header_string("PRODUCT");
  if (auto p = find_header("PASS"))
  {
    if (sscanf(p->value().c_str(), "%d of %d", &pass_, &pass_count_) != 2)
      throw std::runtime_error{"invalid PASS header"};
  }
  is_rhi_ = get_header_string("IMGFMT") == "RHI";

  // get the mandatory characteristics needed to determine scan structure
  angle_resolution_ = get_header_real("ANGRES");
  double rngres = get_header_real("RNGRES");
  double startrng = get_header_real("STARTRNG");
  double endrng = get_header_real("ENDRNG");

  // if start/end angles are provided, use them to limit our ray count
  int inc = 1;
  if (sscanf(product_.c_str(), "%*s %*s SECTOR ANGLE1=%f ANGLE2=%f ANGLEINCREASING=%d", &angle_min_, &angle_max_, &inc) == 3)
  {
    if (inc == 0)
      std::swap(angle_min_, angle_max_);
    while (angle_max_ <= angle_min_)
      angle_max_ += 360.0;
  }
  else
  {
    angle_min_ = 0.0f;
    angle_max_ = 360.0f;
  }

  auto rays = std::lround((angle_max_ - angle_min_) / angle_resolution_);
  if (std::fabs(remainder(angle_max_ - angle_min_, angle_resolution_)) > 0.001)
    throw std::runtime_error{"ANGRES is not a factor of sweep length"};

  auto bins = long(std::ceil((endrng - startrng) / rngres));
  if (bins < 0)
    throw std::runtime_error("calculated range bin count is invalid");

  // if there is no data yet, initialize our arrays
  if (ray_headers_.empty() && level_data_.empty())
  {
    ray_headers_.reserve(rays);
    level_data_.resize(vec2z(bins, rays));
    level_data_.fill(0);
  }
  // otherwise, perform a consistency check
  else
  {
    if (ray_headers_.size() > size_t(rays))
      throw std::runtime_error{fmt::format("ray header count {} exceeds ray count {}", ray_headers_.size(), size_t(rays))};

    if (level_data_.extents().y != size_t(rays) || level_data_.extents().x != size_t(bins))
      throw std::runtime_error{fmt::format("level data size {} mismatch with {} rays, {} bins", level_data_.extents(), rays, bins)};
  }
}

connection::connection(
      string address
    , string service
    , duration keepalive_period
    , duration inactivity_timeout
    , size_t max_buffer_size)
  : address_(std::move(address))
  , service_(std::move(service))
  , keepalive_period_{keepalive_period}
  , inactivity_timeout_{inactivity_timeout}
  , state_{connection_state::disconnected}
  , wbuffer_{256, max_buffer_size}
  , rbuffer_{256, max_buffer_size}
{ }

auto connection::connect() -> void
{
  // update the connection attempt timer
  /* we do this here instead of right at the point of the connect() system call to prevent users
   * from getting caught in dumb tight loops.  for example, if the user gives a bad host name
   * we will fail out before getting to connect().  if the user is also relying on the last_connect()
   * value to timeout a retry then we don't want them to immediately retry and fail to resolve the
   * same hostname! */
  last_connect_ = timestamp::now();

  if (state_ != rapic::connection_state::disconnected)
    throw std::runtime_error{"rapic: connect called while already connected"};

  // lookup the host
  addrinfo hints, *addr;
  memset(&hints, 0, sizeof(hints));
  hints.ai_flags = 0;
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  int ret = getaddrinfo(address_.c_str(), service_.c_str(), &hints, &addr);
  if (ret != 0 || addr == nullptr)
    throw std::runtime_error{"rapic: unable to resolve server address"};

  // TODO - loop through all addresses?
  if (addr->ai_next)
  {

  }

  // create the socket
  socket_.reset(socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol));
  if (!socket_)
  {
    freeaddrinfo(addr);
    throw std::system_error{errno, std::system_category(), "rapic: socket creation failed"};
  }

  // set non-blocking I/O
  int flags = fcntl(socket_, F_GETFL);
  if (flags == -1)
  {
    disconnect();
    freeaddrinfo(addr);
    throw std::system_error{errno, std::system_category(), "rapic: failed to read socket flags"};
  }
  if (fcntl(socket_, F_SETFL, flags | O_NONBLOCK) == -1)
  {
    disconnect();
    freeaddrinfo(addr);
    throw std::system_error{errno, std::system_category(), "rapic: failed to set socket flags"};
  }

  // connect to the remote host
  ret = ::connect(socket_, addr->ai_addr, addr->ai_addrlen);
  if (ret < 0)
  {
    if (errno != EINPROGRESS)
    {
      disconnect();
      freeaddrinfo(addr);
      throw std::system_error{errno, std::system_category(), "rapic: failed to establish connection"};
    }
    state_ = rapic::connection_state::in_progress;
  }
  else
    state_ = rapic::connection_state::established;

  // clean up the address list allocated by getaddrinfo
  freeaddrinfo(addr);

  // set the last activity to now so we don't immediately timeout
  last_activity_ = timestamp::now();
}

auto connection::disconnect() -> void
{
  socket_.reset();
  state_ = connection_state::disconnected;
  last_keepalive_ = timestamp{};
  wbuffer_.clear();
}

auto connection::poll_read() const -> bool
{
  return state_ == connection_state::established;
}

auto connection::poll_write() const -> bool
{
  return state_ == connection_state::in_progress || wbuffer_.read_acquire().second > 0;
}

auto connection::poll(duration timeout) const -> void
{
  if (state_ == connection_state::disconnected)
    throw std::runtime_error{"rapic: attempt to poll while disconnected"};

  struct pollfd fds;
  fds.fd = socket_;
  fds.events = POLLRDHUP | (poll_read() ? POLLIN : 0) | (poll_write() ? POLLOUT : 0);
  ::poll(&fds, 1, std::chrono::floor<std::chrono::seconds>(timeout).count());
}

static auto is_socket_writeable(int socket) -> bool
{
  fd_set writefds;
  FD_ZERO(&writefds);
  FD_SET(socket, &writefds);

  timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = 0;

  int ret;
  while ((ret = select(socket + 1, nullptr, &writefds, nullptr, &tv)) == -1)
  {
    if (errno != EINTR)
      throw std::system_error{errno, std::system_category(), "rapic: select failure"};
    tv.tv_sec = 0;
    tv.tv_usec = 0;
  }

  return ret == 1;
}

auto connection::process_traffic() -> bool
try
{
  // sanity check
  if (state_ == connection_state::disconnected)
    return false;

  // need to check our connection attempt progress
  if (state_ == rapic::connection_state::in_progress)
  {
    /* SO_ERROR is only set once the socket is writeable, so manually check via select that it is.
     * without this check clients can call process_traffic() before the socket is writeable and cause
     * the connection to look established before it really is. */
    if (!is_socket_writeable(socket_))
      return false;

    // get the socket error status
    int res = 0; socklen_t len = sizeof(res);
    if (getsockopt(socket_, SOL_SOCKET, SO_ERROR, &res, &len) < 0)
      throw std::system_error{errno, std::system_category(), "rapic: getsockopt failure"};

    // not connected yet?
    if (res == EINPROGRESS)
      return false;

    // okay, connection attempt is complete.  did it succeed?
    if (res < 0)
      throw std::system_error{res, std::system_category(), "rapic: failed to establish connection (async)"};

    state_ = rapic::connection_state::established;
  }

  // get current time
  auto now = timestamp::now();

  // do we need to send a keepalive? (ie: RDRSTAT)
  if (now - last_keepalive_ > keepalive_period_)
  {
    auto wa = wbuffer_.write_acquire(msg_keepalive.size());
    std::memcpy(wa.first, msg_keepalive.data(), msg_keepalive.size());
    wbuffer_.write_advance(msg_keepalive.size());
    last_keepalive_ = now;
  }

  // write everything we can
  if (auto ra = wbuffer_.read_acquire(); ra.second > 0)
  {
    // write as much as we can
    ssize_t bytes;
    while ((bytes = write(socket_, ra.first, ra.second)) == -1)
    {
      if (errno == EAGAIN || errno == EWOULDBLOCK)
      {
        bytes = 0;
        break;
      }
      else if (errno != EINTR)
        throw std::system_error{errno, std::system_category(), "rapic: write failure"};
    }

    // remove the written data from the buffer
    wbuffer_.read_advance(bytes);
  }

  // read everything we can
  while (true)
  {
    // see how much _contiguous_ space is available in our buffer and ensure we get at least 256 bytes
    // if our buffer is full we will fail here with an exception
    auto wa = rbuffer_.write_acquire(256);

    // read some data off the wire
    auto bytes = recv(socket_, wa.first, wa.second, 0);
    if (bytes > 0)
    {
      // reset our inactivity timeout
      last_activity_ = now;

      // advance our write position
      rbuffer_.write_advance(bytes);

      // if we read as much as we asked for there may be more still waiting so return true
      return static_cast<size_t>(bytes) == wa.second;
    }
    else if (bytes < 0)
    {
      // if we've run out of data to read stop trying
      if (errno == EAGAIN || errno == EWOULDBLOCK)
      {
        // check our inactivity timeout
        if (now - last_activity_ > inactivity_timeout_)
          throw std::runtime_error{"rapic: inactivity timeout"};

        return false;
      }

      // if we were interrupted by a signal handler just try again
      if (errno == EINTR)
        continue;

      // a real receive error - kill the connection
      throw std::system_error{errno, std::system_category(), "rapic: recv failure"};
    }
    else /* if (bytes == 0) */
    {
      // connection has been closed normally by the remote side
      disconnect();
      return false;
    }
  }
}
catch (...)
{
  disconnect();
  throw;
}

auto connection::enqueue(message const& msg) -> void
{
  msg.encode(wbuffer_);
}

auto connection::dequeue(bool skip_broken) -> unique_ptr<message>
{
  message_type type;
  size_t len;
  unique_ptr<message> msg;
  if (rbuffer_.read_detect(type, len))
  {
    try
    {
      switch (type)
      {
      case message_type::comment:
        msg = make_unique<comment>(rbuffer_);
        break;
      case message_type::mssg:
        msg = make_unique<mssg>(rbuffer_);
        break;
      case message_type::status:
        msg = make_unique<status>(rbuffer_);
        break;
      case message_type::permcon:
        msg = make_unique<permcon>(rbuffer_);
        break;
      case message_type::query:
        msg = make_unique<query>(rbuffer_);
        break;
      case message_type::filter:
        msg = make_unique<filter>(rbuffer_);
        break;
      case message_type::scan:
        msg = make_unique<scan>(rbuffer_);
        break;
      }
      rbuffer_.read_advance(len);
    }
    catch (std::exception& err)
    {
      // always move past the message even if it was corrupt and we want to throw
      rbuffer_.read_advance(len);

      if (skip_broken)
        trace::error("rapic: failed to decode message type {}: {}", type, format_exception(err));
      else
        std::throw_with_nested(std::runtime_error{fmt::format("rapic: failed to decode message type {}", type)});
    }
  }
  return msg;
}

auto connection::enqueue_direct(buffer const& buf) -> void
{
  auto ra = buf.read_acquire();
  auto wa = wbuffer_.write_acquire(ra.second);
  std::memcpy(wa.first, ra.first, ra.second);
  wbuffer_.write_advance(ra.second);
}

auto connection::enqueue_direct(string const& buf) -> void
{
  auto wa = wbuffer_.write_acquire(buf.size());
  std::memcpy(wa.first, buf.data(), buf.size());
  wbuffer_.write_advance(buf.size());
}

auto connection::release_connection() -> socket_desc_hnd
{
  return std::move(socket_);
}

auto connection::resume_connection(
      socket_desc_hnd socket
    , connection_state state
    , timestamp last_connect
    , timestamp last_keepalive
    , timestamp last_activity
    ) -> void
{
  if (socket_)
    throw std::runtime_error{"rapic: attempt to resume connection while socket is active"};

  socket_ = std::move(socket);
  state_ = state;
  last_connect_ = last_connect;
  last_keepalive_ = last_keepalive;
  last_activity_ = last_activity;

  // set non-blocking I/O
  /* this should not be necessary if the socket was stored in the systemd FDSTORE but better safe than sorry.  it is
   * after all possible that the user did some weird things to the socket while it was out of our control.  also on
   * RHEL 7 it seems that even the FDSTORE does not preserve socket options like it is supposed to. */
  int flags = fcntl(socket_, F_GETFL);
  if (flags == -1)
  {
    disconnect();
    throw std::system_error{errno, std::system_category(), "rapic: failed to read socket flags"};
  }
  if (fcntl(socket_, F_SETFL, flags | O_NONBLOCK) == -1)
  {
    disconnect();
    throw std::system_error{errno, std::system_category(), "rapic: failed to set socket flags"};
  }
}

// LCOV_EXCL_START
TEST_CASE("io::rapic::parse_station_id")
{
  CHECK(parse_station_id("ANY") == 0);
  CHECK(parse_station_id("2") == 2);
  CHECK(parse_station_id("100") == 100);
  CHECK_THROWS(parse_station_id("bad"));
}

TEST_CASE("io::rapic::parse_scan_type")
{
  CHECK(parse_scan_type("-1") == std::make_pair(scan_type::any, -1));
  CHECK(parse_scan_type("0") == std::make_pair(scan_type::ppi, -1));

  CHECK(parse_scan_type("aNY") == std::make_pair(scan_type::any, -1));
  CHECK(parse_scan_type("pPI") == std::make_pair(scan_type::ppi, -1));
  CHECK(parse_scan_type("rHI") == std::make_pair(scan_type::rhi, -1));
  CHECK(parse_scan_type("compPPI") == std::make_pair(scan_type::comp_ppi, -1));
  CHECK(parse_scan_type("IMAGe") == std::make_pair(scan_type::image, -1));
  CHECK(parse_scan_type("VOl") == std::make_pair(scan_type::volume, -1));
  CHECK(parse_scan_type("VOLUMe") == std::make_pair(scan_type::volume, -1));
  CHECK(parse_scan_type("RHI_SEt") == std::make_pair(scan_type::rhi_set, -1));
  CHECK(parse_scan_type("MERGe") == std::make_pair(scan_type::merge, -1));
  CHECK(parse_scan_type("SCAn_ERROR") == std::make_pair(scan_type::scan_error, -1));

  CHECK(parse_scan_type("VOLUME2") == std::make_pair(scan_type::volume, 2));
  CHECK(parse_scan_type("COMPPPI4") == std::make_pair(scan_type::comp_ppi, 4));

  CHECK_THROWS(parse_scan_type("-2"));
  CHECK_THROWS(parse_scan_type("8"));
  CHECK_THROWS(parse_scan_type("bad"));
}

TEST_CASE("io::rapic::scan_type_string")
{
  char buf[16];
  CHECK(scan_type_string(scan_type::any, -1, buf) == "ANY"s);
  CHECK(scan_type_string(scan_type::ppi, -1, buf) == "PPI"s);
  CHECK(scan_type_string(scan_type::rhi, -1, buf) == "RHI"s);
  CHECK(scan_type_string(scan_type::comp_ppi, -1, buf) == "CompPPI"s);
  CHECK(scan_type_string(scan_type::comp_ppi, 7, buf) == "COMPPPI7"s);
  CHECK(scan_type_string(scan_type::image, -1, buf) == "IMAGE"s);
  CHECK(scan_type_string(scan_type::volume, -1, buf) == "VOLUME"s);
  CHECK(scan_type_string(scan_type::volume, 7, buf) == "VOLUME7"s);
  CHECK(scan_type_string(scan_type::rhi_set, -1, buf) == "RHI_SET"s);
  CHECK(scan_type_string(scan_type::merge, -1, buf) == "MERGE"s);
  CHECK(scan_type_string(scan_type::scan_error, -1, buf) == "SCAN_ERROR"s);
}

TEST_CASE("io::rapic::query_type_string")
{
  CHECK(query_type_string(query_type::latest) == "LATEST"s);
  CHECK(query_type_string(query_type::to_time) == "TOTIME"s);
  CHECK(query_type_string(query_type::from_time) == "FROMTIME"s);
  CHECK(query_type_string(query_type::center_time) == "CENTRETIME"s);
}

TEST_CASE("io::rapic::parse_query_type")
{
  CHECK(parse_query_type("lAtEsT") == query_type::latest);
  CHECK(parse_query_type("TotImE") == query_type::to_time);
  CHECK(parse_query_type("fRoMtImE") == query_type::from_time);
  CHECK(parse_query_type("CeNtReTiMe") == query_type::center_time);

  CHECK_THROWS(parse_query_type("bad"));
}

TEST_CASE("io::rapic::parse_data_types")
{
  auto val = parse_data_types("");
  CHECK(val.empty());

  val = parse_data_types("aNy");
  CHECK(val.empty());

  val = parse_data_types("Refl");
  CHECK(val.size() == 1);
  CHECK(val[0] == "Refl");

  val = parse_data_types("Refl,Vel,SpWdth");
  CHECK(val.size() == 3);
  CHECK(val[0] == "Refl");
  CHECK(val[1] == "Vel");
  CHECK(val[2] == "SpWdth");
}

TEST_CASE("io::rapic::buffer")
{
  message_type type;
  size_t len;
  buffer buf{35};

  CHECK(buf.size() == 35);
  CHECK(buf.read_acquire().second == 0);

  auto wa = buf.write_acquire(36);
  REQUIRE(wa.first != nullptr);
  REQUIRE(wa.second >= 36);
  for (int i = 0; i < 36; ++i)
    wa.first[i] = i;
  CHECK_THROWS(buf.write_advance(wa.second + 1));
  CHECK_NOTHROW(buf.write_advance(36));

  CHECK(buf.read_detect(type, len) == false);

  auto buf2 = buf;
  auto ra2 = buf2.read_acquire();
  REQUIRE(ra2.first != nullptr);
  REQUIRE(ra2.second == 36);
  for (int i = 0; i < 36; ++i)
    CHECK(ra2.first[i] == i);

  auto buf3 = buffer{1};
  buf3 = buf2;
  auto ra3 = buf3.read_acquire();
  REQUIRE(ra3.first != nullptr);
  REQUIRE(ra3.second == 36);
  for (int i = 0; i < 36; ++i)
    CHECK(ra3.first[i] == i);

  auto ra = buf.read_acquire();
  REQUIRE(ra.first != nullptr);
  REQUIRE(ra.second == 36);
  for (int i = 0; i < 36; ++i)
    CHECK(ra.first[i] == i);
  CHECK_NOTHROW(buf.read_advance(10));

  CHECK_NOTHROW(buf.optimize());
  ra = buf.read_acquire();
  REQUIRE(ra.first != nullptr);
  REQUIRE(ra.second == 26);
  for (int i = 0; i < 26; ++i)
    CHECK(ra.first[i] == i + 10);
  CHECK_THROWS(buf.read_advance(27));
  CHECK_NOTHROW(buf.read_advance(10));

  CHECK_THROWS(buf.resize(10));
  CHECK_NOTHROW(buf.resize(16));
  CHECK_NOTHROW(buf.resize(16)); // deliberate duplicate - test null change

  CHECK_NOTHROW(buf.optimize());
  ra = buf.read_acquire();
  REQUIRE(ra.first != nullptr);
  REQUIRE(ra.second == 16);
  for (int i = 0; i < 16; ++i)
    CHECK(ra.first[i] == i + 20);
  CHECK_THROWS(buf.read_advance(17));
  CHECK_NOTHROW(buf.read_advance(16));

  CHECK_NOTHROW(buf.clear());
  CHECK(buf.read_acquire().second == 0);

  CHECK(buf.read_detect(type, len) == false);
}

TEST_CASE("io::rapic::message")
{
  string enc;
  void (*val)(message_type, buffer&, buffer&);
  SUBCASE("comment")
  {
    enc = "/IMAGE: 0002 1708250354\n";
    val = [](message_type type, buffer& b1, buffer& b2)
    {
      auto nul = comment{};
      auto msg = comment{b1};
      CHECK(msg.type() == type);
      CHECK(msg.text() == "IMAGE: 0002 1708250354");
      CHECK_NOTHROW(msg.encode(b2));
    };
  }
  SUBCASE("mssg")
  {
    enc = "MSSG: 123 hello world\n";
    val = [](message_type type, buffer& b1, buffer& b2)
    {
      auto nul = mssg{};
      auto msg = mssg{b1};
      CHECK(msg.type() == type);
      CHECK(msg.number() == 123);
      CHECK(msg.text() == "hello world");
      CHECK_NOTHROW(msg.encode(b2));
    };
  }
  SUBCASE("mssg(30)")
  {
    enc = "MSSG: 30 hello world\nfoo bar\nEND STATUS\n";
    val = [](message_type type, buffer& b1, buffer& b2)
    {
      auto nul = mssg{};
      auto msg = mssg{b1};
      CHECK(msg.type() == type);
      CHECK(msg.number() == 30);
      CHECK(msg.text() == "hello world\nfoo bar");
      CHECK_NOTHROW(msg.encode(b2));
    };
  }
  SUBCASE("status")
  {
    enc = "RDRSTAT: hello world\n";
    val = [](message_type type, buffer& b1, buffer& b2)
    {
      auto nul = status{};
      auto msg = status{b1};
      CHECK(msg.type() == type);
      CHECK(msg.text() == "hello world");
      CHECK_NOTHROW(msg.encode(b2));
    };
  }
  SUBCASE("permcon")
  {
    enc = "RPQUERY: SEMIPERMANENT CONNECTION - SEND ALL DATA TXCOMPLETESCANS=1\n";
    val = [](message_type type, buffer& b1, buffer& b2)
    {
      auto nul = permcon{};
      auto msg = permcon{b1};
      CHECK(msg.type() == type);
      CHECK(msg.tx_complete_scans() == true);
      CHECK_NOTHROW(msg.encode(b2));
    };
  }
  SUBCASE("query")
  {
    enc = "RPQUERY: 4 VOLUME7 1.2 44 LATEST 0 Refl,Vel,SpWdth 256\n";
    val = [](message_type type, buffer& b1, buffer& b2)
    {
      auto nul = query{};
      auto msg = query{b1};
      CHECK(msg.type() == type);
      CHECK(msg.station_id() == 4);
      CHECK(msg.scan_type() == scan_type::volume);
      CHECK(msg.volume_id() == 7);
      CHECK(msg.angle() == approx(1.2));
      CHECK(msg.repeat_count() == 44);
      CHECK(msg.query_type() == query_type::latest);
      CHECK(msg.time() == 0);
      CHECK(msg.data_types().size() == 3);
      CHECK(msg.data_types()[0] == "Refl");
      CHECK(msg.data_types()[1] == "Vel");
      CHECK(msg.data_types()[2] == "SpWdth");
      CHECK(msg.video_resolution() == 256);
      CHECK_NOTHROW(msg.encode(b2));
    };
  }
  SUBCASE("filter")
  {
    enc = "RPFILTER:64:VOLUME2:256:ANY:Refl,Vel,SpWdth\n";
    val = [](message_type type, buffer& b1, buffer& b2)
    {
      auto nul = filter{};
      auto msg = filter{b1};
      CHECK(msg.type() == type);
      CHECK(msg.station_id() == 64);
      CHECK(msg.scan_type() == scan_type::volume);
      CHECK(msg.volume_id() == 2);
      CHECK(msg.video_resolution() == 256);
      CHECK(msg.source() == "ANY");
      CHECK(msg.data_types().size() == 3);
      CHECK(msg.data_types()[0] == "Refl");
      CHECK(msg.data_types()[1] == "Vel");
      CHECK(msg.data_types()[2] == "SpWdth");
      CHECK_NOTHROW(msg.encode(b2));
    };
  }

  // load a buffer with out sample message
  auto b1 = buffer{128};
  auto wa = b1.write_acquire(enc.size());
  memcpy(wa.first, enc.data(), enc.size());
  b1.write_advance(enc.size());

  // find the message in the buffer and verify type
  message_type type;
  size_t len;
  CHECK(b1.read_detect(type, len));

  // decode the message, verify contents and encode into a new buffer
  auto b2 = buffer{128};
  val(type, b1, b2);

  // encode into a new buffer and verify encoding matches original
  auto ra = b2.read_acquire();
  CHECK(ra.second >= enc.size());
  for (size_t i = 0; i < enc.size(); ++i)
    CHECK(ra.first[i] == enc[i]);
}

TEST_CASE("io::rapic::scan")
{
  // create a scan
  int levels;
  SUBCASE("ascii")  { levels = 160; }
  SUBCASE("binary") { levels = 256; }
  vector<scan::header> hdrs =
  {
      { "COUNTRY", "036" }
    , { "NAME", "Melb" }
    , { "STNID", "02" }
    , { "STN_NUM", "087031" }
    , { "LATITUDE", "37.8520" }
    , { "LONGITUDE", "144.7520" }
    , { "HEIGHT", "14" }
    , { "DATE", "23717" }
    , { "TIME", "03.55" }
    , { "TIMESTAMP", "20170825035451" }
    , { "RNGRES", "250" }
    , { "ANGRES", "1.0" }
    , { "VIDRES", to_string(levels) }
    , { "STARTRNG", "0" }
    , { "ENDRNG", "224000" }
    , { "PRODUCT", "VOLUMETRIC [035423717]" }
    , { "PASS", "01 of 28" }
    , { "TILT", "03 of 14" }
    , { "VOLUMEID", "4" }
    , { "VIDEO", "Refl" }
    , { "IMGFMT", "PPI" }
    , { "ELEV", "000.5" }
    , { "DBZLVL", " -30.0 -9.0 -7.5 -6.0 " }
    , { "INTS", " 1 2 3 " }
    , { "T1", "tRuE" }
    , { "T2", "oN" }
    , { "T3", "YeS" }
    , { "T4", "1" }
    , { "F1", "fAlSe" }
    , { "F2", "oFF" }
    , { "F3", "No" }
    , { "F4", "0" }
  };
  vector<scan::ray_header> rhdrs;
  for (size_t i = 0; i < 360; ++i)
    rhdrs.emplace_back((i + 5) % 360, 0.5f, i);
  array2<uint8_t> lvls{{896, 360}};
  for (size_t y = 0; y < lvls.extents().y; ++y)
    for (size_t x = 0; x < lvls.extents().x; ++x)
      lvls[y][x] = ((x + y) * (y % 5)) % levels;
  scan s1{hdrs, rhdrs, lvls};

  // encode it (with some leading whitespace for good measure)
  buffer b1{256};
  auto wa = b1.write_acquire(5);
  REQUIRE(wa.first != nullptr);
  REQUIRE(wa.second >= 5);
  wa.first[0] = '\0';
  wa.first[1] = '\t';
  wa.first[2] = '\r';
  wa.first[3] = '\n';
  wa.first[4] = ' ';
  CHECK_NOTHROW(b1.write_advance(5));
  CHECK_NOTHROW(s1.encode(b1));

  // test the read detect
  message_type type;
  size_t len;
  CHECK(b1.read_detect(type, len) == true);
  CHECK(type == message_type::scan);
  CHECK(len > 0);

  // decode it back into another scan message
  scan s2{b1};

  // check contents matches
  CHECK(s2.type() == message_type::scan);
  CHECK(s2.station_id() == 2);
  CHECK(s2.volume_id() == 4);
  CHECK(s2.product() == "VOLUMETRIC [035423717]");
  CHECK(s2.pass() == 1);
  CHECK(s2.pass_count() == 28);
  CHECK(s2.angle_min() == approx(0.0f));
  CHECK(s2.angle_max() == approx(360.0f));
  CHECK(s2.angle_resolution() == approx(1.0f));
  CHECK(s2.headers().size() == hdrs.size());
  CHECK(s2.find_header("HEIGHT"s) != nullptr);
  CHECK(s2.find_header("bad") == nullptr);
  REQUIRE(s2.find_header("DBZLVL") != nullptr);
  auto dbzlvl = s2.find_header("DBZLVL")->get_real_array();
  CHECK(dbzlvl.size() == 4);
  CHECK(dbzlvl[0] == approx(-30.0));
  CHECK(dbzlvl[1] == approx(-9.0));
  CHECK(dbzlvl[2] == approx(-7.5));
  CHECK(dbzlvl[3] == approx(-6.0));
  REQUIRE(s2.find_header("INTS") != nullptr);
  auto ints = s2.find_header("INTS")->get_integer_array();
  CHECK(ints.size() == 3);
  CHECK(ints[0] == 1);
  CHECK(ints[1] == 2);
  CHECK(ints[2] == 3);
  CHECK(s2.find_header("T1")->get_boolean() == true);
  CHECK(s2.find_header("T2")->get_boolean() == true);
  CHECK(s2.find_header("T3")->get_boolean() == true);
  CHECK(s2.find_header("T4")->get_boolean() == true);
  CHECK(s2.find_header("F1")->get_boolean() == false);
  CHECK(s2.find_header("F2")->get_boolean() == false);
  CHECK(s2.find_header("F3")->get_boolean() == false);
  CHECK(s2.find_header("F4")->get_boolean() == false);
  CHECK_THROWS(s2.find_header("INTS")->get_boolean());
  CHECK(s2.find_or_insert_header("new").name() == "new");
  CHECK(s2.find_or_insert_header("new2").value() == "");
  CHECK(s2.ray_headers().size() == rhdrs.size());
  CHECK(s2.ray_headers()[10].azimuth() == approx(rhdrs[10].azimuth()));
  CHECK(s2.rays() == 360);
  CHECK(s2.bins() == 896);
  CHECK(s2.level_data().size() == lvls.size());
  for (size_t i = 0; i < s2.level_data().size(); ++i)
    CHECK(s2.level_data().data()[i] == lvls.data()[i]);
}
// LCOV_EXCL_STOP

/*---------------------------------------------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2023 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "rb5_to_odim.h"
#include "odim.h"

using namespace bom;

/* this builds an index that tells us where to find each sweep/moment combination within the scan set.  the index uses
 * the sweep number as the y dimension and the data type number (index into data_types) as the x dimension.  the value
 * of the index is the index into the scan_set store, or -1 if there is no data available for that sweep/moment. */
static auto build_scan_set_index(span<io::rb5::file const*> scan_set, span<pair<string, string> const> data_types) -> array2i
{
  // determine the sweep count
  auto sweep_count = int(scan_set[0]->metadata.root()["scan"]["slice"]["scantotalno"]);

  // initialize the index
  auto out = array2i{vec2z{data_types.size(), size_t(sweep_count)}};
  out.fill(-1);

  // populate the index
  for (auto ifile = 0; ifile < scan_set.size(); ++ifile)
  {
    auto& scan = scan_set[ifile]->metadata.root()["scan"];
    for (auto& slice : scan.children())
    {
      // ignore everything except slice elements
      if (slice.name() != "slice")
        continue;

      // determine the sweep number (scanno is 1 based in rainbow)
      auto isweep = int(slice["scanno"]) - 1;
      if (isweep < 0 || isweep >= sweep_count)
        throw std::runtime_error{fmt::format("Invalid scanno {} detected", isweep)};

      // loop through moments
      for (auto& data : slice["slicedata"].children())
      {
        if (data.name() != "rawdata")
          continue;

        auto& type = data("type").get();
        for (size_t idata = 0; idata < data_types.size(); ++idata)
          if (data_types[idata].first == type)
            out[isweep][idata] = ifile;
      }
    }
  }

  return out;
}

/* this returns a vector of pointers to xml nodes which represent all the slices in the file, in read order, up to and
 * and including the target slice.  this is necessary because metadata elements are only present in a slice if their
 * value has changed since the previous slice.  to use slice metadata you must loop through this vector in REVERSE
 * order to find the current value of the desired element. */
static auto build_slice_metadata(io::xml::node const& metascan, int isweep) -> vector<io::xml::node const*>
{
  vector<io::xml::node const*> slices;
  for (auto& entry : metascan.children())
  {
    if (entry.name() != "slice")
      continue;
    slices.push_back(&entry);
    if (int(entry["scanno"]) == isweep + 1)
      return slices;
  }

  // should be impossible given our earlier indexing
  throw std::runtime_error{"Unable to locate slice"};
}

static auto find_slice_metadata(vector<io::xml::node const*> const& metaslice, char const* name) -> io::xml::node const*
{
  for (auto islice = metaslice.rbegin(); islice != metaslice.rend(); ++islice)
    if (auto imeta = (*islice)->children().find(name); imeta != (*islice)->children().end())
      return &(*imeta);
  return nullptr;
}

static auto get_slice_metadata(vector<io::xml::node const*> const& metaslice, char const* name) -> io::xml::node const&
{
  if (auto node = find_slice_metadata(metaslice, name))
    return *node;
  throw std::runtime_error{fmt::format("Required slice metadata '{}' does not exist", name)};
}

static auto find_slice(io::xml::node const& metascan, int isweep) -> io::xml::node const&
{
  for (auto& entry : metascan.children())
    if (entry.name() == "slice" && int(entry["scanno"]) == isweep + 1)
      return entry;

  // should be impossible given our earlier indexing
  throw std::runtime_error{"Unable to locate slice"};
}

static auto find_rawdata(io::xml::node const& metaslicedata, string type) -> io::xml::node const&
{
  for (auto& entry : metaslicedata.children())
    if (entry.name() == "rawdata" && entry("type").get() == type)
      return entry;

  // should be impossible given our earlier indexing
  throw std::runtime_error{"Unable to locate rawdata"};
}

static auto decide_data_type(int depth) -> io::odim::data::data_type
{
  if (depth == 8)
    return io::odim::data::data_type::u8;
  if (depth == 16)
    return io::odim::data::data_type::u16;
  throw std::runtime_error{"Unexpected raw data bit depth"};
}

/* note that we use the start[i] to start[i+1] rather than start[i] to stop[i] to find north.  this is because
 * there is normally a small gap between the stop of ray[i] and the start of ray[i+1].  i expect this gap is
 * typically the angular distance swept between last pulse of the batch for ray[i] and first pulse contributing
 * to the batch of ray[i+1].  this gap causes us to be unable to find north in a surprising number of cases.
 *
 * we still need the stopangle data though for the last ray, since we have no start[i+1] in that case.
 *
 * also note that angle are always positive and normalized to 0..360 before being passed into this function. */
static auto find_north_ray_index(io::rb5::rayinfo const& raydata)
{
  if (raydata.startangle.empty())
    throw std::runtime_error{"Missing required rayinfo startangle"};
  if (raydata.stopangle.size() != raydata.startangle.size())
    throw std::runtime_error{"Missing required rayinfo stopangle"};
  for (auto iray = 0; iray < raydata.startangle.size() - 1; ++iray)
  {
    if (   (raydata.startangle[iray] > 350.0_deg && raydata.startangle[iray+1] < 10.0_deg)
        || (raydata.startangle[iray] < 10.0_deg && raydata.startangle[iray+1] > 350.0_deg))
      return iray;
  }
  auto iray = int(raydata.startangle.size()) - 1;
  if (   (raydata.startangle[iray] > 350.0_deg && raydata.stopangle[iray] < 10.0_deg)
      || (raydata.startangle[iray] < 10.0_deg && raydata.stopangle[iray] > 350.0_deg))
    return iray;
  throw std::runtime_error{"Unable to locate north ray"};
}

static auto rotate_rayinfo_data(io::rb5::rayinfo& raydata, int north_ray)
{
  auto do_rotate = [](auto& array, int count)
  {
    if (!array.empty())
      std::rotate(array.begin(), array.begin() + count, array.end());
  };

  do_rotate(raydata.startangle, north_ray);
  do_rotate(raydata.stopangle, north_ray);
  do_rotate(raydata.startfixangle, north_ray);
  do_rotate(raydata.stopfixangle, north_ray);
  do_rotate(raydata.dataflag, north_ray);
  do_rotate(raydata.numpulses, north_ray);
  do_rotate(raydata.timestamp, north_ray);
  do_rotate(raydata.txpower, north_ray);
  do_rotate(raydata.noisepowerh, north_ray);
  do_rotate(raydata.noisepowerv, north_ray);
}

static auto get_threshold_flags(string const& thresholds, string const& data_type) -> uint64_t
{
  auto values = tokenize<string>(thresholds, ",:");
  if (values.size() % 2 != 0)
    throw std::runtime_error{"Invalid threshold flags"};
  for (auto i = 0; i < values.size(); i += 2)
    if (values[i] == data_type)
      return from_string<uint64_t>(values[i+1]);

  // data types like SNR don't have an entry in the thresholds flags since it doesn't make
  // sense for them to be thresholded
  return 0;
}

static auto determine_odim_object_type(io::xml::node const& metadata) -> io::odim::file::object_type
{
  auto& file_type = metadata("type").get();
  if (file_type == "azi" || file_type == "vol")
    return metadata["scan"]["pargroup"]["sectorscan"].get() == "On"
      ? io::odim::file::object_type::azimuthal_object
      : io::odim::file::object_type::polar_volume;
  if (file_type == "ele")
    return io::odim::file::object_type::elevational_object;
  throw std::runtime_error{"Failed to determine basic volume type"};
}

static auto time_to_odim_strings(timestamp val) -> std::pair<char[9], char[7]>
{
  pair<char[9], char[7]> date_time;
  auto t = val.as_time_t();
  struct tm tms;
  gmtime_r(&t, &tms);
  if (snprintf(date_time.first, 9, "%04d%02d%02d", tms.tm_year + 1900, tms.tm_mon + 1, tms.tm_mday) >= 9)
    throw std::logic_error{"Bad snprintf"};
  if (snprintf(date_time.second, 7, "%02d%02d%02d", tms.tm_hour, tms.tm_min, tms.tm_sec) >= 7)
    throw std::logic_error{"Bad snprintf"};
  return date_time;
}

auto bom::io::rb5_to_odim(
      std::filesystem::path const& path
    , span<io::rb5::file const*> scan_set
    , span<pair<string, string> const> data_types
    , io::configuration const& metadata_static
    , int odim_compression
    , int only_sweep
    , int only_type
    ) -> void
{
  if (scan_set.empty())
    throw std::runtime_error{"Attempt to convert empty scan set to odim"};

  auto& metadata = scan_set[0]->metadata.root();
  auto& metasensor = metadata["sensorinfo"];
  auto& metascan = metadata["scan"];
  auto& metapargroup = metascan["pargroup"];

  /* some top level metadata from an ODIM point of view is stored in the first slice from the Rainbow point of view
   * this object lets us use metadata from the first slice at the top level.  we don't create it using the
   * build_slice_meatadata(metascan, 0) because the first file in our scan_set might not actually contain slice 1.
   * this could happen if files are received out of order, or if we only received part of a volume.  instead just
   * grab the first "slice" element from the file regardless of which sweep it is. the subscript operator on an
   * io::xml::node will return the first matching child element so it's okay that there are multiple slice elements
   * under a each scan. */
  auto metaslice0 = vector<io::xml::node const*>{&metascan["slice"]};

  // determine what fundamental product type we are outputting
  auto obj_type = determine_odim_object_type(metadata);

  auto h5vol = io::odim::polar_volume{path.native(), io_mode::create};
  h5vol.set_object(obj_type);
  h5vol.set_date_time(from_string<timestamp>(metascan("date").get() + "T" + metascan("time").get() + "Z"));
  h5vol.attributes()["lat"].set(double(metasensor["lat"]));
  h5vol.attributes()["lon"].set(double(metasensor["lon"]));
  h5vol.attributes()["height"].set(double(metasensor["alt"]));

  // extended (how) attributes
  h5vol.attributes()["beamwidth"].set(double(metasensor["beamwidth"]));
  if (auto node = find_slice_metadata(metaslice0, "spbhorbeam"))
    h5vol.attributes()["beamwH"].set(double(*node));
  if (auto node = find_slice_metadata(metaslice0, "spbverbeam"))
    h5vol.attributes()["beamwV"].set(double(*node));
  h5vol.attributes()["wavelength"].set(double(metasensor["wavelen"]) * 100.0);
  h5vol.attributes()["task"].set(metascan("name").get());
  h5vol.attributes()["software"].set("RAINBOW");
  h5vol.attributes()["sw_version"].set(metadata("version").get());
  h5vol.attributes()["simulated"].set(false);
  h5vol.attributes()["comment"].set(metapargroup["comment"].get());
  if (auto node = find_slice_metadata(metaslice0, "gdrx_dp_proc_mode"))
  {
    if (node->get() == "GdrxDpModeHV_HV")
    {
      h5vol.attributes()["poltype"].set("simultaneous-dual");
      h5vol.attributes()["polmode"].set("simultaneous-dual");
    }
    else if (node->get() == "GdrxDpModeHV_H")
    {
      h5vol.attributes()["poltype"].set("simultaneous-dual");
      h5vol.attributes()["polmode"].set("single-H");
    }
    else if (node->get() == "GdrxDpModeHV_V")
    {
      h5vol.attributes()["poltype"].set("simultaneous-dual");
      h5vol.attributes()["polmode"].set("single-V");
    }
    else if (node->get() == "GdrxDpModeH_HV")
    {
      h5vol.attributes()["poltype"].set("simultaneous-dual");
      h5vol.attributes()["polmode"].set("LDR-H");
    }
    else if (node->get() == "GdrxDpModeV_HV")
    {
      h5vol.attributes()["poltype"].set("simultaneous-dual");
      h5vol.attributes()["polmode"].set("LDR-V");
    }
  }
  if (auto node = find_slice_metadata(metaslice0, "spbrxloss"))
    h5vol.attributes()["RXlossH"].set(double(*node));
  if (auto node = find_slice_metadata(metaslice0, "spbdpvrxloss"))
    h5vol.attributes()["RXlossV"].set(double(*node));
  if (auto node = find_slice_metadata(metaslice0, "spbtxloss"))
    h5vol.attributes()["TXlossH"].set(double(*node));
  if (auto node = find_slice_metadata(metaslice0, "spbdpvtxloss"))
    h5vol.attributes()["TXlossV"].set(double(*node));
  if (auto node = find_slice_metadata(metaslice0, "spbantgain"))
    h5vol.attributes()["antgainH"].set(double(*node));
  if (auto node = find_slice_metadata(metaslice0, "spbdpvantgain"))
    h5vol.attributes()["antgainV"].set(double(*node));
  if (auto node = find_slice_metadata(metaslice0, "spbradomloss"))
  {
    h5vol.attributes()["radomelossH"].set(double(*node));
    h5vol.attributes()["radomelossV"].set(double(*node));
  }
  #if 0 // TODO - can we get these attributes from the Rainbow file?
  h5vol.attributes()["nomTXpower"].set(...);
  h5vol.attributes()["gasattn"].set(...);
  #endif

  // add in static metadata
  // TODO - alllow user defined static metadata for int and double types
  for (auto& meta : metadata_static.object())
    h5vol.attributes()[meta.first].set(meta.second.string());

  // output the sweeps as datasetN
  auto scan_set_index = build_scan_set_index(scan_set, data_types);
  for (auto is = 0; is < scan_set_index.extents().y; ++is)
  {
    // ignore sweeps we don't want to output
    if (only_sweep != -1 && is != only_sweep)
      continue;

    // find the index of the first present moment for this sweep
    auto ifirst = 0;
    while (ifirst < data_types.size())
    {
      if (scan_set_index[is][ifirst] >= 0)
        break;
      ++ifirst;
    }

    // ignore sweep if there are no moments received for it
    if (ifirst == data_types.size())
      continue;

    auto& rb5datafirst = *scan_set[scan_set_index[is][ifirst]];
    auto metaslice = build_slice_metadata(rb5datafirst.metadata.root()["scan"], is);
    auto& metaslicedata = get_slice_metadata(metaslice, "slicedata");
    auto& metarawdatafirst = find_rawdata(metaslicedata, data_types[ifirst].first);
    auto rays = int(metarawdatafirst("rays"));
    auto bins = int(metarawdatafirst("bins"));

    // extract the per ray data
    auto raydata = io::rb5::rayinfo{metaslicedata, rb5datafirst.blobs};
    auto sweepstarttime = io::rb5::parse_date_time_high_accuracy(metaslicedata("datetimehighaccuracy"));
    auto sweepstoptime = timestamp{};
    if (auto val = metaslicedata("stopdatetimehighaccuracy", string{}); !val.empty())
      sweepstoptime = io::rb5::parse_date_time_high_accuracy(val);
    else if (!raydata.timestamp.empty())
      sweepstoptime = sweepstarttime + raydata.timestamp[raydata.timestamp.size() - 1];
    else
      throw std::runtime_error{"Unable to determine sweep stop time"};

    // locate ray spanning 0 deg and rotate per ray data so that this ray is at index 0
    // ray rotation is only mandated for full 360 degree sweeps (i.e. what/product == SCAN)
    auto north_ray = obj_type == io::odim::file::object_type::polar_volume || obj_type == io::odim::file::object_type::polar_scan ? find_north_ray_index(raydata) : 0;
    if (north_ray != 0)
      rotate_rayinfo_data(raydata, north_ray);

    // create the odim scan
    auto h5sweep = h5vol.dataset_append();
    switch (obj_type)
    {
    case io::odim::file::object_type::polar_volume:
    case io::odim::file::object_type::polar_scan:
      {
        // we only cope with clockwise rotation at the moment (due to ODIM ray ordering constraints)
        // if we allow CCW rotation also need to add sign to antspeed below
        if (int(get_slice_metadata(metaslice, "antdirection")) != 0)
          throw std::runtime_error{"Unexpected antenna sweep direction"};
        h5sweep.attributes()["product"].set("SCAN");
        h5sweep.attributes()["elangle"].set(double(get_slice_metadata(metaslice, "posangle")));
        h5sweep.attributes()["a1gate"].set(long((rays - north_ray) % rays));
        h5sweep.attributes()["astart"].set((raydata.startangle[0].normalize().degrees() - 360.0));
      }
      break;
    case io::odim::file::object_type::azimuthal_object:
      {
        if (raydata.startangle.empty() || raydata.stopangle.empty())
          throw std::runtime_error{"Per ray angles required for sector scan conversion"};
        h5sweep.attributes()["product"].set("AZIM");
        h5sweep.attributes()["elangle"].set(double(get_slice_metadata(metaslice, "posangle")));
        /* the rainbow format does provide dedicated 'startangle' and 'stopangle' metadata in the file.  however these
         * are the commanded angles.  my experience is that the commanded angles are not necessarily to be trusted, sometimes
         * different from the read back by the width of an entire ray.  for this reason I ignore this metadata and use the
         * readback start[ray 0] and stop[ray N] angles instead.
         *
         * it seems that in the radars we have, the sector 'stop' azimuth is exclusive.  so if you ask for a sector from
         * 20 to 30 (with 1 degree rays), the radar will actually scan 20-29.  the frustrating part of this is that for
         * a sector volume, the return sweep is then exclusive in the other way scanning 30-21.  which means you never
         * actually scan the same sector, but alternate between 20-29 and 21-30 depending on sweep direction. */
        h5sweep.attributes()["startaz"].set(raydata.startangle[0].degrees());
        h5sweep.attributes()["stopaz"].set(raydata.stopangle[raydata.stopangle.size() - 1].degrees());
        h5sweep.attributes()["a1gate"].set(0l);
      }
      break;
    case io::odim::file::object_type::elevational_object:
      if (raydata.startangle.empty() || raydata.stopangle.empty())
        throw std::runtime_error{"Per ray angles required for RHI scan conversion"};
      h5sweep.attributes()["product"].set("RHI");
      h5sweep.attributes()["az_angle"].set(double(get_slice_metadata(metaslice, "posangle")));
      /* using actual readback values.  we don't seem to have the 'exclusive end' problem that sectors do, but
       * i'd rather be safe than sorry.  readback is more accurate anyway. */
      h5sweep.attributes()["startel"].set(raydata.startangle[0].degrees());
      h5sweep.attributes()["stopel"].set(raydata.stopangle[raydata.stopangle.size()-1].degrees());
      h5sweep.attributes()["a1gate"].set(0l);
      break;
    default:
      throw std::logic_error{"Unhandled ODIM type"};
    }
    h5sweep.attributes()["nbins"].set(long(bins));
    h5sweep.attributes()["rstart"].set(double(get_slice_metadata(metaslice, "start_range")));
    h5sweep.attributes()["rscale"].set(double(get_slice_metadata(metaslice, "rangestep")) * 1000.0);
    h5sweep.attributes()["nrays"].set(long(rays));
    {
      auto dts = time_to_odim_strings(sweepstarttime);
      h5sweep.attributes()["startdate"].set(dts.first);
      h5sweep.attributes()["starttime"].set(dts.second);
    }
    {
      auto dts = time_to_odim_strings(sweepstoptime);
      h5sweep.attributes()["enddate"].set(dts.first);
      h5sweep.attributes()["endtime"].set(dts.second);
    }

    // use the start and stop azimuths of the first ray to detect sweep direction (if available)
    bool positive_sweep = true;
    if (!raydata.startangle.empty() && !raydata.stopangle.empty())
    {
      auto start = raydata.startangle[0].normalize().degrees();
      auto stop = raydata.stopangle[0].normalize().degrees();
      auto delta = std::abs(stop - start);
      if ((start > stop && delta < 180.0) || (stop > start && delta > 180.0))
        positive_sweep = false;
    }

    // extended (how) attributes
    h5sweep.attributes()["NI"].set(double(get_slice_metadata(metaslice, "dynv")("max")));
    h5sweep.attributes()["scan_index"].set(long(get_slice_metadata(metaslice, "scanno")));
    h5sweep.attributes()["scan_count"].set(long(get_slice_metadata(metaslice, "scantotalno")));
    h5sweep.attributes()["antspeed"].set((positive_sweep ? 1 : -1) * double(get_slice_metadata(metaslice, "antspeed")));
    // pw_usec is only present if using a fixed pulse width, otherwise assume dynamice pulse width mode
    if (auto node = find_slice_metadata(metaslice, "pw_index"))
    {
      auto pw_index = long(*node);
      h5sweep.attributes()["pulsewidth"].set(double(get_slice_metadata(metaslice, "pw_usec")) * 0.000001);
      if (auto node = find_slice_metadata(metaslice, "rspdphradconst"))
        h5sweep.attributes()["radconstH"].set(tokenize<double>(node->get(), " ").at(pw_index));
      else if (auto node = find_slice_metadata(metaslice, "rspradconst"))
        h5sweep.attributes()["radconstH"].set(tokenize<double>(node->get(), " ").at(pw_index));
      if (auto node = find_slice_metadata(metaslice, "rspdpvradconst"))
        h5sweep.attributes()["radconstH"].set(tokenize<double>(node->get(), " ").at(pw_index));
    }
    else if (auto node = find_slice_metadata(metaslice, "dynpw"))
    {
      h5sweep.attributes()["pulsewidth"].set(double(*node) * 0.000001);
      if (auto node = find_slice_metadata(metaslice, "rspdphradconst"))
        h5sweep.attributes()["radconstH"].set(double(*node));
      else if (auto node = find_slice_metadata(metaslice, "rspradconst"))
        h5sweep.attributes()["radconstH"].set(double(*node));
      if (auto node = find_slice_metadata(metaslice, "rspdpvradconst"))
        h5sweep.attributes()["radconstV"].set(double(*node));
    }
    h5sweep.attributes()["NEZH"].set(double(get_slice_metadata(metaslice, "noise_power_dbz")));
    h5sweep.attributes()["NEZV"].set(double(get_slice_metadata(metaslice, "noise_power_dbz_dpv")));
    h5sweep.attributes()["startepochs"].set(double(sweepstarttime.as_time_t()));
    h5sweep.attributes()["endepochs"].set(double(sweepstoptime.as_time_t()));
    h5sweep.attributes()["Vsamples"].set(long(get_slice_metadata(metaslice, "timesamp")));
    #if 0 // TODO - would be nice to find a way to get these into the rainbow files
    h5sweep.attributes()["powerdiff"].set(...);
    h5sweep.attributes()["phasediff"].set(...);
    h5sweep.attributes()["peakpwr"].set(...);
    h5sweep.attributes()["avgpwr"].set(...);
    #endif
    auto malfunc = false;
    auto radar_msg = string{};
    if (auto node = find_slice_metadata(metaslice, "mpdbcufaultstat"))
    {
      radar_msg.append(fmt::format("MPDBCU: {}\n", node->get()));
      malfunc = malfunc || (node->get() != "OK");
    }
    if (auto node = find_slice_metadata(metaslice, "gdrx5faultstat"))
    {
      radar_msg.append(fmt::format("GDRX: {}\n", node->get()));
      malfunc = malfunc || (node->get() != "OK");
    }
    if (auto node = find_slice_metadata(metaslice, "txfaultstat"))
    {
      radar_msg.append(fmt::format("TX: {}\n", node->get()));
      malfunc = malfunc || (node->get() != "OK");
    }
    if (auto node = find_slice_metadata(metaslice, "acufaultstat"))
    {
      radar_msg.append(fmt::format("ACU: {}\n", node->get()));
      malfunc = malfunc || (node->get() != "OK");
    }
    if (auto node = find_slice_metadata(metaslice, "rxfaultstat"))
    {
      radar_msg.append(fmt::format("ARX: {}\n", node->get()));
      malfunc = malfunc || (node->get() != "OK");
    }
    if (!radar_msg.empty())
    {
      h5sweep.attributes()["malfunc"].set(malfunc);
      h5sweep.attributes()["radar_msg"].set(radar_msg);
    }
    if (auto node = find_slice_metadata(metaslice, "spbsysgainoffset"))
      h5sweep.attributes()["zcalH"].set(double(*node));
    if (auto node = find_slice_metadata(metaslice, "spbdpvsysgainoffset"))
      h5sweep.attributes()["zcalV"].set(double(*node));
    h5sweep.attributes()["VPRCorr"].set(false);
    h5sweep.attributes()["anglesync"].set(obj_type == io::odim::file::object_type::elevational_object ? "elevation" : "azimuth");
    h5sweep.attributes()["anglesyncRes"].set(double(get_slice_metadata(metaslice, "anglestep")));
    {
      auto oss = std::ostringstream{};
      if (auto node = find_slice_metadata(metaslice, "cmd"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_doppler"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "fftfilter"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "filtermode"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_spatial"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_dectree"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_gip"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "filterwidth"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_gip_width"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_gip_mode_const"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "gdrx_cluttermap"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_dectree-vlimit"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_dectree-thresone"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_dectree-threstwo"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_dectree-lagfactor"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_dectree-mapthr"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_dectree-mapmax"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_dectree_snr_thr"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cf_dectree_sqi_thr"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "filterdepth"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "fft_weight"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "statisticalfilter"))
        node->write(oss);
      if (auto node = find_slice_metadata(metaslice, "cmd"))
        node->write(oss);
      auto clutter_type = oss.str();
      if (!clutter_type.empty())
        h5sweep.attributes()["clutterType"].set(clutter_type);
    }
    auto prf_h = double(get_slice_metadata(metaslice, "highprf"));
    if (prf_h > 0.0)
      h5sweep.attributes()["highprf"].set(prf_h);
    auto prf_m = double(get_slice_metadata(metaslice, "midprf"));
    if (prf_m > 0.0)
      h5sweep.attributes()["midprf"].set(prf_m);
    auto prf_l = double(get_slice_metadata(metaslice, "lowprf"));
    if (prf_l > 0.0)
      h5sweep.attributes()["lowprf"].set(prf_l);
    {
      // non-standard
      auto phase_coding = int(get_slice_metadata(metaslice, "phase_coding"));
      if (phase_coding == 2)
        h5sweep.attributes()["phase_code"].set("none"s);
      else if (phase_coding == 4)
      {
        auto phase_pattern = int(get_slice_metadata(metaslice, "phase_pattern"));
        if (phase_pattern == 0)
          h5sweep.attributes()["phase_code"].set("random"s);
        else if (phase_pattern == 1)
          h5sweep.attributes()["phase_code"].set("SZ(8/64)"s);
      }
    }
    if (!raydata.startangle.empty())
    {
      auto values = vector<double>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = raydata.startangle[iray].degrees();
      h5sweep.attributes()[obj_type == io::odim::file::object_type::elevational_object ? "startelA" : "startazA"].set(values);
    }
    if (!raydata.stopangle.empty())
    {
      auto values = vector<double>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = raydata.stopangle[iray].degrees();
      h5sweep.attributes()[obj_type == io::odim::file::object_type::elevational_object ? "stopelA" : "stopazA"].set(values);
    }
    if (!raydata.startfixangle.empty())
    {
      auto values = vector<double>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = raydata.startfixangle[iray].degrees();
      h5sweep.attributes()[obj_type == io::odim::file::object_type::elevational_object ? "startazA" : "startelA"].set(values);
    }
    if (!raydata.stopfixangle.empty())
    {
      auto values = vector<double>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = raydata.stopfixangle[iray].degrees();
      h5sweep.attributes()[obj_type == io::odim::file::object_type::elevational_object ? "stopazA" : "stopelA"].set(values);
    }
    if (!raydata.dataflag.empty())
    {
      auto values = vector<long>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = raydata.dataflag[iray];
      h5sweep.attributes()["dataflag"].set(values); // Non-Standard
    }
    /* we are only able to output per-ray PRTs if we have the data flags and are running dual-prf in "fixed" mode.
     * for now we ignore triple prf mode as well since the rainbow spec doesn't say what bitmask to use for the midprf */
    if (   !raydata.dataflag.empty()
        && prf_h > 0.0 && prf_m <= 0.0 && prf_l > 0.0
        && get_slice_metadata(metaslice, "dualprfmode").get() == "SdfDPrfModeFix")
    {
      auto prt_h = 1.0 / prf_h;
      auto prt_l = 1.0 / prf_l;
      auto values = vector<double>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = (raydata.dataflag[iray] & 0x0100) ? prt_h : prt_l;
      h5sweep.attributes()["prt"].set(values);
    }
    if (!raydata.numpulses.empty())
    {
      auto values = vector<long>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = raydata.numpulses[iray];
      h5sweep.attributes()["numpulses"].set(values); // Non-Standard
    }
    if (!raydata.timestamp.empty())
    {
      auto base = io::rb5::parse_date_time_high_accuracy(metaslicedata("datetimehighaccuracy")).as_time_t();
      auto values = vector<double>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = base + double(raydata.timestamp[iray].count()) * 0.001;
      h5sweep.attributes()["startT"].set(values);
    }
    if (!raydata.txpower.empty())
    {
      auto values = vector<double>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = raydata.txpower[iray] / 1000.0;
      h5sweep.attributes()["TXpower"].set(values);
    }
    if (!raydata.noisepowerh.empty())
    {
      auto values = vector<double>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = raydata.noisepowerh[iray];
      h5sweep.attributes()["noisepowerh"].set(values); // Non-Standard
    }
    if (!raydata.noisepowerv.empty())
    {
      auto values = vector<double>(rays);
      for (int iray = 0; iray < rays; ++iray)
        values[iray] = raydata.noisepowerv[iray];
      h5sweep.attributes()["noisepowerv"].set(values); // Non-Standard
    }

    // output the moments as dataM
    for (auto it = 0; it < data_types.size(); ++it)
    {
      // ignore data types we don't want to output
      if (only_type != -1 && it != only_type)
        continue;

      // ignore data type if it is unavailable
      if (scan_set_index[is][it] < 0)
        continue;

      auto& rb5data = *scan_set[scan_set_index[is][it]];
      auto& metarawdata = find_rawdata(find_slice(rb5data.metadata.root()["scan"], is)["slicedata"], data_types[it].first);
      auto depth = int(metarawdata("depth"));
      auto minval = float(metarawdata("min"));
      auto maxval = float(metarawdata("max"));
      auto gain = (maxval - minval) / (depth == 8 ? 254 : 65534);
      auto offset = minval - gain;

      // create the odim data
      size_t dims[2] = { rays, bins };
      auto h5data = h5sweep.data_append(decide_data_type(depth), 2, dims, odim_compression);
      h5data.set_quantity(data_types[it].second);
      h5data.set_gain(gain);
      h5data.set_offset(offset);
      h5data.set_nodata(0.0);
      h5data.set_undetect(0.0);

      // data specific metadata
      auto thflags = get_threshold_flags(get_slice_metadata(metaslice, "threshold").get(), data_types[it].first);
      if (thflags & 1 || thflags & 2)
        h5data.attributes()["LOG"].set(double(get_slice_metadata(metaslice, "log")));
      if ((thflags & 4 || thflags & 64) && (thflags & 16 || thflags & 128))
        h5data.attributes()["SQI"].set(std::max(double(get_slice_metadata(metaslice, "sqi")), double(get_slice_metadata(metaslice, "zsqi"))));
      else if (thflags & 4 || thflags & 64)
        h5data.attributes()["SQI"].set(double(get_slice_metadata(metaslice, "sqi")));
      else if (thflags & 16 || thflags & 128)
        h5data.attributes()["SQI"].set(double(get_slice_metadata(metaslice, "zsqi")));
      if (thflags & 8)
        h5data.attributes()["CSR"].set(double(get_slice_metadata(metaslice, "csr")));

      // find our blob
      auto blob = rb5data.blobs.find(metarawdata("blobid"));
      if (blob == rb5data.blobs.end())
        throw std::runtime_error{fmt::format("Missing blob id {}", metarawdata("blobid").get())};

      // rotate our blob so that the north most ray is first (if needed) and deal with endianness of the 16bit types
      if (depth == 8)
      {
        auto buf = array2<uint8_t>{vec2z{bins, rays}};
        for (auto iray = 0; iray < rays; ++iray)
        {
          auto irayin = (north_ray + iray) % rays;
          for (auto ibin = 0; ibin < bins; ++ibin)
            buf[iray][ibin] = blob->second[irayin * bins + ibin];
        }
        h5data.write(buf.data());
      }
      else if (depth == 16)
      {
        auto buf = array2<uint16_t>{vec2z{bins, rays}};
        for (auto iray = 0; iray < rays; ++iray)
        {
          auto irayin = (north_ray + iray) % rays;
          for (auto ibin = 0; ibin < bins; ++ibin)
            buf[iray][ibin]
              = (blob->second[irayin * bins * 2 + ibin * 2] << 8)
              + (blob->second[irayin * bins * 2 + ibin * 2 + 1] << 0);
        }
        h5data.write(buf.data());
      }
      else
        throw std::runtime_error{"Unexpected depth"};
    }
  }
}

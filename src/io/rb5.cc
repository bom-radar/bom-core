/*---------------------------------------------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2023 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "config.h"
#include "rb5.h"

#include <zlib.h>
#include <bom/io_error.h>
#include <bom/trace.h>
#include <bom/io/configuration.h>
#include <sstream>

using namespace bom;
using namespace bom::io::rb5;

auto bom::io::rb5::extract_xml(std::istream& file) -> string
{
  string xml, line;
  while (file)
  {
    std::getline(file, line);
    if (line == "<!-- END XML -->")
      break;
    xml.append(1, '\n').append(line);
  }
  return xml;
}

auto bom::io::rb5::extract_blobs(std::istream& file) -> blob_store
{
  blob_store blobs;

  string line;
  while (std::getline(file, line))
  {
    trim(line);

    // ignore extra whitespace lines
    if (line.empty())
      continue;

    // parse the blob header
    size_t id, size;
    char comp[16];
    if (sscanf(line.c_str(), "<BLOB blobid = \"%zu\" size = \"%zu\" compression = \"%15[^\"]\" >", &id, &size, comp) != 3)
      throw io_error{"failed to parse blob header"};

    // read the compressed data
    array1<uint8_t> data{size};
    file.read(reinterpret_cast<char*>(data.data()), size);
    file.ignore();
    if (!file)
      throw io_error{"failed to read blob data"};

    // uncompress the data
    if (strcmp(comp, "qt") == 0)
    {
      // sanity check
      if (data.size() < 4)
        throw io_error{"missing uncompressed size in qt compressed blob data"};

      // extract the decompressed length and initialize some storage
      // encoded as a 32bit big-endian value
      auto full_size = (size_t(data[0]) << 24) + (size_t(data[1]) << 16) + (size_t(data[2]) << 8) + size_t(data[3]);
      array1<uint8_t> inflated{full_size};

      // inflate the blob using zlib
      z_stream stream;
      stream.next_in = data.data() + 4;
      stream.avail_in = data.size() - 4;
      stream.next_out = inflated.data();
      stream.avail_out = inflated.size();
      stream.zalloc = nullptr;
      stream.zfree = nullptr;
      int err;
      if ((err = inflateInit(&stream)) != Z_OK)
        throw io_error{fmt::format("failed to inflate compressed blob: {}", zError(err))};
      if ((err = inflate(&stream, Z_FINISH)) != Z_STREAM_END)
      {
        inflateEnd(&stream);
        throw io_error{fmt::format("failed to inflate compressed blob: {}", zError(err))};
      }
      inflateEnd(&stream);

      // move the inflated data into our main store
      data = std::move(inflated);
    }
    else if (strcmp(comp, "none") != 0)
      throw io_error{"unsupported blob compression type"};

    // skip the blob footer
    std::getline(file, line);
    trim(line);
    if (line != "</BLOB>")
      throw io_error{"failed to parse blob footer"};

    // store the blob for later return
    blobs.emplace(id, std::move(data));
  }

  return blobs;
}

auto bom::io::rb5::extract_blobs_raw(std::istream& file) -> blob_store
{
  blob_store blobs;

  string line;
  while (std::getline(file, line))
  {
    trim(line);

    // ignore extra whitespace lines
    if (line.empty())
      continue;

    // parse the blob header
    size_t id, size;
    char comp[16];
    if (sscanf(line.c_str(), "<BLOB blobid = \"%zu\" size = \"%zu\" compression = \"%15[^\"]\" >", &id, &size, comp) != 3)
      throw io_error{"failed to parse blob header"};

    // allocate our array (including space for the psuedo-XML header and footer
    array1<uint8_t> data{line.size() + 1 + size + 1 + 7};

    // copy in the header
    memcpy(data.data(), line.c_str(), line.size());
    data[line.size()] = '\n';

    // read the compressed data
    file.read(reinterpret_cast<char*>(data.data() + line.size() + 1), size);
    file.ignore();
    if (!file)
      throw io_error{"failed to read blob data"};

    // copy in the footer
    memcpy(data.data() + line.size() + 1 + size, "\n</BLOB>", 1 + 7);

    // skip the blob footer
    std::getline(file, line);
    trim(line);
    if (line != "</BLOB>")
      throw io_error{"failed to parse blob footer"};

    // store the blob for later return
    blobs.emplace(id, std::move(data));
  }

  return blobs;
}

auto bom::io::rb5::convert_rayinfo_uint16(size_t size, array1<uint8_t> const& data) -> array1<uint16_t>
{
  if (size * 2 != data.size())
    throw std::runtime_error{"rayinfo / blob size mismatch"};

  array1<uint16_t> out{size};
  for (size_t i = 0; i < size; ++i)
    out.data()[i] = (data[i * 2] << 8) + data[i * 2 + 1];

  return out;
}

auto bom::io::rb5::convert_rayinfo_uint32(size_t size, array1<uint8_t> const& data) -> array1<uint32_t>
{
  if (size * 4 != data.size())
    throw std::runtime_error{"rayinfo / blob size mismatch"};

  array1<uint32_t> out{size};
  for (size_t i = 0; i < size; ++i)
    out.data()[i]
      = (data[i * 4 + 0] << 24)
      + (data[i * 4 + 1] << 16)
      + (data[i * 4 + 2] << 8)
      + (data[i * 2 + 1] << 0);

  return out;
}

auto bom::io::rb5::convert_rayinfo_floats(size_t size, array1<uint8_t> const& data) -> array1f
{
  if (size * 4 != data.size())
    throw std::runtime_error{"rayinfo / blob size mismatch"};

  array1f out{size};
  for (size_t i = 0; i < size; ++i)
  {
    static_assert(
          sizeof(float) == 4 && std::numeric_limits<float>::is_iec559
        , "IEEE754 float32 type required for reinterpret cast");
    out.data()[i] = *reinterpret_cast<float const*>(&data[i * 4]);
  }

  return out;
}

auto bom::io::rb5::convert_rayinfo_angles(size_t size, array1<uint8_t> const& data) -> array1<angle>
{
  if (size * 2 != data.size())
    throw std::runtime_error{"rayinfo / blob size mismatch"};

  auto step = 360.0_deg / 65536;

  // TODO - double check this given the -1 error in the rawdata blob that we had
  array1<angle> out{size};
  for (size_t i = 0; i < size; ++i)
    out.data()[i] = step * ((data[i * 2] << 8) + data[i * 2 + 1]);

  return out;
}

auto bom::io::rb5::convert_rayinfo_times(size_t size, array1<uint8_t> const& data) -> array1<std::chrono::milliseconds>
{
  if (size * 4 != data.size())
    throw std::runtime_error{"rayinfo / blob size mismatch"};

  array1<std::chrono::milliseconds> out{size};
  for (size_t i = 0; i < size; ++i)
  {
    auto d = &data[i * 4];
    out.data()[i] = std::chrono::milliseconds{(d[0] << 24u) + (d[1] << 16u) + (d[2] << 8u) + d[3]};
  }

  return out;
}

auto bom::io::rb5::convert_rawdata_blob(vec2z size, float nodata, float min, float max, int depth, array1<uint8_t> const& data) -> array2f
{
  if (size.y * size.x * (depth / 8) != data.size())
    throw std::runtime_error{"rawdata / blob size mismatch"};

  array2f out{size};

  if (depth == 8)
  {
    auto step = (max - min) / 254;
    for (size_t i = 0; i < size.x * size.y; ++i)
      out.data()[i] = data[i] == 0 ? nodata : min + (data[i] - 1) * step;
  }
  else if (depth == 16)
  {
    // given the much finer resolution in 16 bit, do the math as double then convert down
    auto dmin = double(min), dmax = double(max);
    auto step = (dmax - dmin) / 65534;
    for (size_t i = 0; i < size.x * size.y; ++i)
    {
      auto val = (data[i * 2] << 8) + data[i * 2 + 1];
      out.data()[i] = val == 0 ? nodata : float(dmin + (val - 1) * step);
    }
  }
  else
    throw std::runtime_error{"unsupported rawdata depth"};

  return out;
}

auto bom::io::rb5::parse_date_time_high_accuracy(string const& str) -> timestamp
{
  return from_string<timestamp>(str.substr(0, 19)) + std::chrono::milliseconds{from_string<int>(str.substr(20, 3), 10)};
}

rayinfo::rayinfo(io::xml::node const& slicedata, blob_store const& blobs)
{
  for (auto& rayinfo : slicedata.children())
  {
    if (rayinfo.name() != "rayinfo")
      continue;

    auto iblob = blobs.find(rayinfo("blobid"));
    if (iblob == blobs.end())
      throw std::runtime_error{fmt::format("Missing blob id {}", rayinfo("blobid").get())};

    auto rays = int(rayinfo("rays"));

    auto& refid = rayinfo("refid").get();
    if (refid == "startangle")
      startangle = convert_rayinfo_angles(rays, iblob->second);
    else if (refid == "stopangle")
      stopangle = convert_rayinfo_angles(rays, iblob->second);
    else if (refid == "startfixangle")
      startfixangle = convert_rayinfo_angles(rays, iblob->second);
    else if (refid == "stopfixangle")
      stopfixangle = convert_rayinfo_angles(rays, iblob->second);
    else if (refid == "dataflag")
      dataflag = convert_rayinfo_uint16(rays, iblob->second);
    else if (refid == "numpulses")
      numpulses = convert_rayinfo_uint16(rays, iblob->second);
    else if (refid == "timestamp")
      timestamp = convert_rayinfo_times(rays, iblob->second);
    else if (refid == "txpower")
      txpower = convert_rayinfo_uint32(rays, iblob->second);
    else if (refid == "noisepowerh")
      noisepowerh = convert_rayinfo_floats(rays, iblob->second);
    else if (refid == "noisepowerv")
      noisepowerv = convert_rayinfo_floats(rays, iblob->second);
    else
      trace::debug("Ignoring unknown rayinfo type {}", refid);
  }
}

/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <bom/array1.h>
#include <bom/file_utils.h>
#include <bom/io_error.h>
#include <bom/raii.h>
#include <bom/span.h>
#include <bom/string_utils.h>
#include <bom/timestamp.h>
#include <array>

/// NetCDF I/O classes
namespace bom::io::nc
{
  namespace bnc = ::bom::io::nc;

  /// Data types supported for NetCDF variable storage
  enum class data_type
  {
      i8
    , u8
    , i16
    , u16
    , i32
    , u32
    , i64
    , u64
    , f32
    , f64
    , string
  };

  /// Maximum number of dimensions per variable supported by this API
  /** This number is significantly lower than the theoretical NetCDF maximum of NC_MAX_VAR_DIMS (1024).
   *  It is safe to increase this value as needed.  Having said that, if you need more than 16 dimensions in a single
   *  variable you should have a very hard think about your data design. */
  constexpr size_t max_var_dims = 16;

  /// Special 'start' index used in variable read/write calls
  /** Specifying this value as the 'start' index for a dimension indicates that all values for
   *  the dimension will be read or written. */
  constexpr size_t all = -1;

  /// Special 'unlimited' size used to create record dimensions
  constexpr size_t unlimited = 0;

  class dimension;
  class variable;
  class group;
  class object;

  /// Exception class for NetCDF related errors
  class error : public std::exception
  {
  public:
    error(dimension const* at, string const& desc, char const* api = nullptr, int status = 0);
    error(object const* at, string const& desc, char const* api = nullptr, int status = 0);

    virtual auto what() const noexcept -> char const*;

  private:
    string description_;
  };

  /// Dimension
  class dimension
  {
  public:
    dimension(dimension const&) = delete;
    dimension(dimension&&) = default;
    auto operator=(dimension const&) -> dimension& = delete;
    auto operator=(dimension&&) ->dimension& = default;

    /// Get the group that this dimension is defined in
    auto parent()               { return parent_; }

    /// Get the group that this dimension is defined in
    auto const parent() const   { return parent_; }

    /// Get the dimension name
    auto& name() const          { return name_; }

    /// Get whether this is an unlimited dimension
    auto unlimited() const      { return unlimited_; }

    /// Get the dimension size
    auto size() const -> size_t { return size_; }

    /// Rename the dimension
    auto rename(string const& name) -> void;

  private:
    dimension(group* parent, string name, size_t size);
    dimension(group* parent, int dimid);

    auto dimid() const          { return dimid_; }

    friend class group;
    friend class variable;

  private:
    group*  parent_;
    int     dimid_;
    string  name_;
    bool    unlimited_;
    size_t  size_;
  };

  /// Base class for groups and variables which share a common attribute API
  /** The group and variable classes inherit from this class privately and then expose the public functions to the
   *  user via the 'attributes()' function.  Other common functionality which is protected here is exposed to the
   *  user directly from the group/variable classes by a using directive. */
  class object
  {
  public:
    object(object const&) = delete;
    object(object&&) = default;
    auto operator=(object const&) -> object& = delete;
    auto operator=(object&&) -> object& = default;

    /// Get the group that this variable is defined in
    auto parent()             { return parent_; }
    /// Get the group that this variable is defined in
    auto const parent() const { return parent_; }

    /// Get the variable name
    auto& name() const        { return name_; }

    /// Get the number of attributes
    auto att_size() const -> size_t;

    /// Get the name of the n'th attribute
    auto att_name(size_t i) const -> string;

    /// Determine whether an attribute of the given name exists
    auto att_exists(char const* name) const -> bool;
    auto att_exists(string const& name) const { return att_exists(name.c_str()); }

    /// Get the stored type of an attribute
    auto att_type(char const* name) const -> data_type;
    auto att_type(string const& name) const { return att_type(name.c_str()); }

    /// Get the number of elements stored (array size) of an attribute
    auto att_size(char const* name) const -> size_t;
    auto att_size(string const& name) const { return att_size(name.c_str()); }

    /// Rename an attributes
    auto att_rename(char const* name, char const* new_name) -> void;
    auto att_rename(string const& name, string const& new_name) { att_rename(name.c_str(), new_name.c_str()); }

    /// Erase an attribute
    auto att_erase(char const* name) -> void;
    auto att_erase(string const& name) { att_erase(name.c_str()); }

    /// Copy a single attribute from another object
    auto att_copy(object const& from, char const* name) -> void;
    auto att_copy(object const& from, string const& name) { att_copy(from, name.c_str()); }

    /// Copy all attributes from another object
    auto att_copy(object const& from, bool overwrite) -> void;

    /// Write an attribute
    auto att_set(char const* name, signed char val) -> void;
    auto att_set(char const* name, unsigned char val) -> void;
    auto att_set(char const* name, short val) -> void;
    auto att_set(char const* name, unsigned short val) -> void;
    auto att_set(char const* name, int val) -> void;
    auto att_set(char const* name, unsigned int val) -> void;
    auto att_set(char const* name, long val) -> void;
    auto att_set(char const* name, unsigned long val) -> void;
    auto att_set(char const* name, long long val) -> void;
    auto att_set(char const* name, unsigned long long val) -> void;
    auto att_set(char const* name, float val) -> void;
    auto att_set(char const* name, double val) -> void;
    auto att_set(char const* name, char const* val) -> void;
    auto att_set(char const* name, string const& val) -> void;

    auto att_set(char const* name, span<signed char const> val) -> void;
    auto att_set(char const* name, span<unsigned char const> val) -> void;
    auto att_set(char const* name, span<short const> val) -> void;
    auto att_set(char const* name, span<unsigned short const> val) -> void;
    auto att_set(char const* name, span<int const> val) -> void;
    auto att_set(char const* name, span<unsigned int const> val) -> void;
    auto att_set(char const* name, span<long const> val) -> void;
    auto att_set(char const* name, span<unsigned long const> val) -> void;
    auto att_set(char const* name, span<long long const> val) -> void;
    auto att_set(char const* name, span<unsigned long long const> val) -> void;
    auto att_set(char const* name, span<float const> val) -> void;
    auto att_set(char const* name, span<double const> val) -> void;

    auto att_set(string const& name, signed char val)                     { att_set(name.c_str(), val); }
    auto att_set(string const& name, unsigned char val)                   { att_set(name.c_str(), val); }
    auto att_set(string const& name, short val)                           { att_set(name.c_str(), val); }
    auto att_set(string const& name, unsigned short val)                  { att_set(name.c_str(), val); }
    auto att_set(string const& name, int val)                             { att_set(name.c_str(), val); }
    auto att_set(string const& name, unsigned int val)                    { att_set(name.c_str(), val); }
    auto att_set(string const& name, long val)                            { att_set(name.c_str(), val); }
    auto att_set(string const& name, unsigned long val)                   { att_set(name.c_str(), val); }
    auto att_set(string const& name, long long val)                       { att_set(name.c_str(), val); }
    auto att_set(string const& name, unsigned long long val)              { att_set(name.c_str(), val); }
    auto att_set(string const& name, float val)                           { att_set(name.c_str(), val); }
    auto att_set(string const& name, double val)                          { att_set(name.c_str(), val); }
    auto att_set(string const& name, char const* val)                     { att_set(name.c_str(), val); }
    auto att_set(string const& name, string const& val)                   { att_set(name.c_str(), val); }

    auto att_set(string const& name, span<signed char const> val)         { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<unsigned char const> val)       { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<short const> val)               { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<unsigned short const> val)      { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<int const> val)                 { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<unsigned int const> val)        { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<long const> val)                { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<unsigned long const> val)       { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<long long const> val)           { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<unsigned long long const> val)  { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<float const> val)               { att_set(name.c_str(), val); }
    auto att_set(string const& name, span<double const> val)              { att_set(name.c_str(), val); }

    template <typename T>
    auto att_set(char const* name, span<T> val) { att_set(name, span<T const>{val}); }

    template <typename T>
    auto att_set(string const& name, span<T> val) { att_set(name.c_str(), span<T const>{val}); }

    template <typename T>
    auto att_set(char const* name, optional<T> const& val)
    {
      if (val)
        att_set(name, *val);
      else if (att_exists(name))
        att_erase(name);
    }

    template <typename T>
    auto att_set(string const& name, optional<T> const& val) { att_set(name.c_str(), val); }

    /// Read an attribute
    auto att_get(char const* name, signed char& val) const -> void;
    auto att_get(char const* name, unsigned char& val) const -> void;
    auto att_get(char const* name, short& val) const -> void;
    auto att_get(char const* name, unsigned short& val) const -> void;
    auto att_get(char const* name, int& val) const -> void;
    auto att_get(char const* name, unsigned int& val) const -> void;
    auto att_get(char const* name, long& val) const -> void;
    auto att_get(char const* name, unsigned long& val) const -> void;
    auto att_get(char const* name, long long& val) const -> void;
    auto att_get(char const* name, unsigned long long& val) const -> void;
    auto att_get(char const* name, float& val) const -> void;
    auto att_get(char const* name, double& val) const -> void;
    auto att_get(char const* name, string& val) const -> void;

    auto att_get(char const* name, span<signed char> val) const -> void;
    auto att_get(char const* name, span<unsigned char> val) const -> void;
    auto att_get(char const* name, span<short> val) const -> void;
    auto att_get(char const* name, span<unsigned short> val) const -> void;
    auto att_get(char const* name, span<int> val) const -> void;
    auto att_get(char const* name, span<unsigned int> val) const -> void;
    auto att_get(char const* name, span<long> val) const -> void;
    auto att_get(char const* name, span<unsigned long> val) const -> void;
    auto att_get(char const* name, span<long long> val) const -> void;
    auto att_get(char const* name, span<unsigned long long> val) const -> void;
    auto att_get(char const* name, span<float> val) const -> void;
    auto att_get(char const* name, span<double> val) const -> void;

    /* we can't use a template for the string name versions of these functions because this makes the calls
     * ambiguous when passing a container directly instead of a span.  for example:
     *
     * vector<int> data;
     * var.att_get("name", data);
     *
     * it's ambiguous whether to call:
     * - att_get(char const*, span<int>) by converting data to span<int>
     * - att_get(string const&, T&&) by converting "name" to a string */

    auto att_get(string const& name, signed char& val) const              { att_get(name.c_str(), val); }
    auto att_get(string const& name, unsigned char& val) const            { att_get(name.c_str(), val); }
    auto att_get(string const& name, short& val) const                    { att_get(name.c_str(), val); }
    auto att_get(string const& name, unsigned short& val) const           { att_get(name.c_str(), val); }
    auto att_get(string const& name, int& val) const                      { att_get(name.c_str(), val); }
    auto att_get(string const& name, unsigned int& val) const             { att_get(name.c_str(), val); }
    auto att_get(string const& name, long& val) const                     { att_get(name.c_str(), val); }
    auto att_get(string const& name, unsigned long& val) const            { att_get(name.c_str(), val); }
    auto att_get(string const& name, long long& val) const                { att_get(name.c_str(), val); }
    auto att_get(string const& name, unsigned long long& val) const       { att_get(name.c_str(), val); }
    auto att_get(string const& name, float& val) const                    { att_get(name.c_str(), val); }
    auto att_get(string const& name, double& val) const                   { att_get(name.c_str(), val); }
    auto att_get(string const& name, string& val) const                   { att_get(name.c_str(), val); }

    auto att_get(string const& name, span<signed char> val) const         { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<unsigned char> val) const       { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<short> val) const               { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<unsigned short> val) const      { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<int> val) const                 { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<unsigned int> val) const        { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<long> val) const                { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<unsigned long> val) const       { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<long long> val) const           { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<unsigned long long> val) const  { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<float> val) const               { att_get(name.c_str(), val); }
    auto att_get(string const& name, span<double> val) const              { att_get(name.c_str(), val); }

    /// Read an attribute
    template <typename T>
    auto att_get_as(char const* name) const -> typename std::enable_if<!is_instance_of<T, optional>::value, T>::type
    {
      // TODO - how should we initialize vector/array to correct size here?
      T val;
      att_get(name, val);
      return val;
    }

    /// Specialization for optional<T> types which returns a nullopt if attribute is not found
    template <typename T>
    auto att_get_as(char const* name) const -> typename std::enable_if<is_instance_of<T, optional>::value, T>::type
    {
      // TODO - how should we initialize vector/array to correct size here?
      T val;
      if (att_exists(name))
      {
        val.emplace();
        att_get(name, *val);
      }
      return val;
    }

    template <typename T>
    auto att_get_as(string const& name) const { return att_get_as<T>(name.c_str()); }

    /// Read an attribute providing a default value if it does not exist
    template <typename T>
    auto att_get_as(char const* name, T&& def) const -> T
    {
      return att_exists(name) ? att_get_as<T>(name) : def;
    }

    template <typename T>
    auto att_get_as(string const& name, T&& def) const { return att_get_as(name.c_str(), std::forward<T>(def)); }

  protected:
    object(group* parent, string name, int ncid, int varid);

    auto ncid() const   { return ncid_; }
    auto varid() const  { return varid_; }

    friend class error;
    friend class dimension;
    friend class variable;

  protected:
    group*  parent_;
    string  name_;
    int     ncid_;  // file or group id
    int     varid_; // variable id (or NC_GLOBAL if not a variable)
  };

  /// Variable
  /**
   * The read and write functions for a variable allow the user to fully specify the NetCDF hyperslab by providing
   * start indexes and count run lengths for each dimension.  For convenience the start and count values may be
   * omitted in which case the behaviour is as follows:
   * - The count for any omitted dimension defaults to 1
   * - If start is omitted or set to 'all' for any dimension it is set to 0 and count is set to the dimension size
   *
   * It is an error to specify more count values than start values.  It is also an error to set the start value for
   * a dimension to 'all' and then specify a count other than the size of the dimension.  In this case the special
   * count value of '0' may be supplied which will be ignored (and default to the correct dimension size).  This
   * allows the user to avoid having to manually lookup the dimension size.
   *
   * This behaviour simplifies API usage for the common case of reading and writing 2D images from a higher rank
   * variable such as a time series (3D) or ensemble (4D).  Examples corresponding to acessing a 4D variable are
   * shown below.  In this example the dimensions are assumed to correspond to member, time, y and x in order.
   *
   * var.read(data);                    // read ensemble
   * var.read(data, {1});               // read member (entire time series)
   * var.read(data, {7, 3});            // read forecast (single time step)
   * var.read(data, {6, 1, 4});         // read column of forecast
   * var.read(data, {8, 3, 2, 8});      // read cell of forecast
   * var.read(data, {8, 3, all, 8});    // read row of forecast
   *
   * var.read(data, {all, 3});          // read forecast 3 of every member
   * var.read(data, {6, 3}, {1, 5});    // read forecast 3-7 of member 6
   * var.read(data, {all, 3}, {0, 5});  // read forecast 3-7 of every member
   */
  class variable : public object
  {
  public:
    /// Get the stored type of a variable
    auto type() const         { return type_; }

    /// Get the chunk size
    auto chunking(span<size_t> chunk_size) const -> bool;

    /// Get the compression level
    auto compression() const -> int;

    /// Get the dimensions
    auto& dimensions()        { return dimensions_; }
    auto& dimensions() const  { return dimensions_; }

    /// Rename the variable
    auto rename(string const& name) -> void;

    /// Read variable data
    auto read(span<signed char> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<unsigned char> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<short> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<unsigned short> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<int> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<unsigned int> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<long> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<unsigned long> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<long long> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<unsigned long long> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<float> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<double> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<char> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;
    auto read(span<string> data, span<size_t const> start = {}, span<size_t const> count = {}) const -> void;

    // TODO - get rid of these if span can be constructed directly from initializer_list
    void read(span<signed char> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<unsigned char> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<short> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<unsigned short> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<int> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<unsigned int> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<long> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<unsigned long> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<long long> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<unsigned long long> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<float> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<double> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<char> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;
    void read(span<string> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) const;

    /// Write variable data
    auto write(span<signed char const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<unsigned char const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<short const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<unsigned short const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<int const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<unsigned int const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<long const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<unsigned long const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<long long const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<unsigned long long const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<float const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<double const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;
    auto write(span<string const> data, span<size_t const> start = {}, span<size_t const> count = {}) -> void;

    // TODO - get rid of these if span can be constructed directly from initializer_list
    void write(span<signed char const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<unsigned char const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<short const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<unsigned short const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<int const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<unsigned int const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<long const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<unsigned long const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<long long const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<unsigned long long const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<float const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<double const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});
    void write(span<string const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {});

  private:
    using hs_indexes = std::array<size_t, max_var_dims>;

  private:
    variable(
          group* parent
        , string name
        , data_type type
        , span<dimension* const> dims
        , span<size_t const> chunk_size
        , int compression_level);
    variable(group* parent, variable const& rhs);
    variable(group* parent, int varid);

    auto setup_hyperslab(
          size_t elements
        , span<size_t const> start
        , span<size_t const> count
        , hs_indexes& hs_start
        , hs_indexes& hs_count
        ) const -> void;

    friend class group;

  private:
    data_type           type_;
    array1<dimension*>  dimensions_;
  };

  inline void variable::read(span<signed char> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<unsigned char> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<short> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<unsigned short> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<int> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<unsigned int> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<long> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<unsigned long> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<long long> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<unsigned long long> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<float> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<double> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<char> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::read(span<string> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count) const
  {
    read(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<signed char const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<unsigned char const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<short const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<unsigned short const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<int const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<unsigned int const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<long const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<unsigned long const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<long long const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<unsigned long long const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<float const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<double const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }
  inline void variable::write(span<string const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count)
  {
    write(data, {start.begin(), start.end()}, {count.begin(), count.end()});
  }

  template <typename T, typename F>
  class restricted_list : private list<T>
  {
  public:
    // TODO - expose the member types

    using list<T>::empty;
    using list<T>::size;
    using list<T>::max_size;

    using list<T>::front;
    using list<T>::back;

    using list<T>::begin;
    using list<T>::end;
    using list<T>::rbegin;
    using list<T>::rend;
    using list<T>::cbegin;
    using list<T>::cend;
    using list<T>::crbegin;
    using list<T>::crend;

    friend F;
  };

  // we must use a list for dimensions (at least) so that adding dimensions does not invalidate
  // the dimension pointers held by the variables
  using dimension_store = restricted_list<dimension, group>;
  using variable_store = restricted_list<variable, group>;
  using group_store = restricted_list<group, group>;

  /// Group
  class group : public object
  {
  public:
    /// Rename the group
    auto rename(string const& name) -> void;

    /// Get the sub-groups
    auto& groups()            { return groups_; }
    auto& groups() const      { return groups_; }

    /// Add a subgroup
    auto create_group(string name) -> group&;

    /// Get the dimensions
    auto& dimensions()        { return dimensions_; }
    auto& dimensions() const  { return dimensions_; }

    /// Add a dimension
    auto create_dimension(string name, size_t size) -> dimension&;

    /// Find a dimension by name
    /** May return a pointer to a dimension owned by a parent group */
    auto find_dimension(string_view name) const -> dimension const*;
    auto find_dimension(string_view name) -> dimension*
    {
      return const_cast<dimension*>(static_cast<group const*>(this)->find_dimension(name));
    }

    /// Lookup a dimension using a path relative to this group
    /** Path must be descending only.  Ascending to parents using the traditional '..' token is not supported. */
    auto lookup_dimension(string_view path) const -> dimension const&;
    auto lookup_dimension(string_view path) -> dimension&
    {
      return const_cast<dimension&>(static_cast<group const*>(this)->lookup_dimension(path));
    }
    auto lookup_dimension_ptr(string_view path) const -> dimension const*;
    auto lookup_dimension_ptr(string_view path) -> dimension*
    {
      return const_cast<dimension*>(static_cast<group const*>(this)->lookup_dimension_ptr(path));
    }

    /// Get the variables
    auto& variables()         { return variables_; }
    auto& variables() const   { return variables_; }

    /// Add a variable
    auto create_variable(
          string name
        , data_type type
        , span<dimension* const> dims = {}
        , span<size_t const> chunk_size = {}
        , int compression_level = 5
        ) -> variable&;
    // TODO - get rid of this - allow initialization of spans from initializer lists instead
    auto create_variable(
          string name
        , data_type type
        , std::initializer_list<dimension*> dims
        , std::initializer_list<size_t> chunk_size = {}
        , int compression_level = 5
        ) -> variable&;

    /// Copy a variable
    auto copy_variable(variable const& rhs) -> variable&;

    /// Find a variable by name
    /** May return a pointer to a variable owned by a parent group */
    auto find_variable(string_view name) const -> variable const*;
    auto find_variable(string_view name) -> variable*
    {
      return const_cast<variable*>(static_cast<group const*>(this)->find_variable(name));
    }

    /// Lookup a variable using a path relative to this group
    /** Path must be descending only.  Ascending to parents using the traditional '..' token is not supported. */
    auto lookup_variable(string_view path) const -> variable const&;
    auto lookup_variable(string_view path) -> variable&
    {
      return const_cast<variable&>(static_cast<group const*>(this)->lookup_variable(path));
    }
    auto lookup_variable_ptr(string_view path) const -> variable const*;
    auto lookup_variable_ptr(string_view path) -> variable*
    {
      return const_cast<variable*>(static_cast<group const*>(this)->lookup_variable_ptr(path));
    }

    /// Lookup a group using a path relative to this group
    /** Path must be descending only.  Ascending to parents using the traditional '..' token is not supported. */
    auto lookup_group(string_view path) const -> group const&;
    auto lookup_group(string_view path) -> group&
    {
      return const_cast<group&>(static_cast<group const*>(this)->lookup_group(path));
    }
    auto lookup_group_ptr(string_view path) const -> group const*;
    auto lookup_group_ptr(string_view path) -> group*
    {
      return const_cast<group*>(static_cast<group const*>(this)->lookup_group_ptr(path));
    }

  protected:
    group(group* parent, string name);
    group(group* parent);
    auto init(int ncid) -> void;

    friend class file;

  private:
    group_store     groups_;
    dimension_store dimensions_;
    variable_store  variables_;
  };

  inline auto group::create_variable(
        string name
      , data_type type
      , std::initializer_list<dimension*> dims
      , std::initializer_list<size_t> chunk_size
      , int compression_level
      ) -> variable&
  {
    return create_variable(std::move(name), type, {dims.begin(), dims.end()}, {chunk_size.begin(), chunk_size.end()}, compression_level);
  }

  /// File
  class file : public group
  {
  public:
    file(string path, io_mode mode);

    /// Get path used to open the underlying file
    auto& path() const { return path_; }

    /// Flush any writes to the underlying file
    auto flush() -> void;

  private:
    using nc_file_handle = unique_hnd<int, int(*)(int), 0>;

  private:
    string          path_;
    nc_file_handle  handle_;
  };
}

namespace bom
{
  BOM_DECLARE_ENUM_TRAITS(io::nc::data_type, i8, u8, i16, u16, i32, u32, i64, u64, f32, f64, string);
}

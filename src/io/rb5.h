/*---------------------------------------------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2023 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include <bom/array1.h>
#include <bom/array2.h>
#include <bom/io/xml.h>
#include <bom/string_utils.h>
#include <bom/timestamp.h>
#include <bom/vec2.h>
#include <istream>
#include <chrono>

namespace bom::io::rb5
{
  /// Map from blob id's to raw uncompressed blob data
  using blob_store = map<int, array1<uint8_t>>;

  /// Read the XML (text data) from a hybrid-XML Rainbow 5 file
  auto extract_xml(std::istream& file) -> string;

  /// Read the blobs (binary data) from a hybrid-XML Rainbow 5 file
  auto extract_blobs(std::istream& file) -> blob_store;

  /// Read the blobs (binary data) from a hybrid-XML Rainbow 5 file and return them without decompresession
  auto extract_blobs_raw(std::istream& file) -> blob_store;

  /// Convert rayinfo blob data to 1D array of uint16
  auto convert_rayinfo_uint16(size_t size, array1<uint8_t> const& data) -> array1<uint16_t>;

  /// Convert rayinfo blob data to 1D array of uint32
  auto convert_rayinfo_uint32(size_t size, array1<uint8_t> const& data) -> array1<uint32_t>;

  /// Convert rayinfo blob data to 1D array of floats
  auto convert_rayinfo_floats(size_t size, array1<uint8_t> const& data) -> array1f;

  /// Convert rayinfo blob data to 1D array of angles
  auto convert_rayinfo_angles(size_t size, array1<uint8_t> const& data) -> array1<angle>;

  /// Convert rayinfo blob data to 1D array of millisecond offsets
  auto convert_rayinfo_times(size_t size, array1<uint8_t> const& data) -> array1<std::chrono::milliseconds>;

  /// Convert rawdata blob to 2D array of floats
  auto convert_rawdata_blob(
        vec2z size
      , float nodata
      , float min
      , float max
      , int depth
      , array1<uint8_t> const& data
      ) -> array2f;

  /// Parse a 'high accuracy' timestamp (YYYY-mm-ddTHH:MM:SS.SSS)
  auto parse_date_time_high_accuracy(string const& str) -> timestamp;

  /// Collection of all known rayinfo arrays
  struct rayinfo
  {
    array1<angle> startangle;
    array1<angle> stopangle;
    array1<angle> startfixangle;
    array1<angle> stopfixangle;
    array1<uint16_t> dataflag;
    array1<uint16_t> numpulses;
    array1<std::chrono::milliseconds> timestamp;
    array1<uint32_t> txpower;
    array1<float> noisepowerh;
    array1<float> noisepowerv;

    rayinfo(io::xml::node const& slicedata, blob_store const& blobs);
  };

  /// Simple wrapper for the basic elements comprising a rainbow file
  struct file
  {
    string            header;     // metadata XML in raw string form
    blob_store        blobs;      // binary blobs
    io::xml::document metadata;   // metadata XML in object form

    file() = default;

    file(std::istream& fin)
      : header(extract_xml(fin))
      , blobs{extract_blobs(fin)}
      , metadata{header}
    { }
  };
}

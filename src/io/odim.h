/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <bom/file_utils.h>
#include <bom/raii.h>
#include <bom/string_utils.h>
#include <bom/timestamp.h>
#include <bom/trace.h>

#include <cmath>                // round()
#include <limits>

/// OPERA Data Information Model (ODIM), HDF5 Representation support
namespace bom::io::odim
{
  /// Get the default ODIM_H5 conventions version used
  auto default_odim_version() -> pair<int, int>;

  /// Convert separate ODIM date and time strings into a timestamp
  auto strings_to_time(const string& date, const string& time) -> timestamp;

  // Internal - RAII object for managing an HDF5 API hid_t
  /* The HDF5 API defines a type 'hid_t' which is used as the native handle for all API calls.  Ideally we
   * could just use that type directly here - but instead we have the very strange looking union below.
   * The reason we don't just use hid_t direclty is because we want to avoid exposing the user to the
   * HDF5 headers all for the sake of a single typedef.  This used to be simple because hid_t used to be
   * a typedef of int, and we could just use an int here, and then static_assert to check that hid_t was
   * actually an int in the .cc file.  Unfortunately HDF5 changed the definition of hid_t to an int64_t
   * at some point.  This means that our header now needs to support hid_t being either an int or an
   * int64_t, still without including the HDF5 header at this point.  The only way to achieve this is to
   * either set some defines in the build system (yuk), or use a union here so that either way works.
   *
   * The trick is that all the member functions are defined in the .cc file where we can use "if constexpr"
   * conditions to set which member of union is used at runtime.  These are all set as inline functions so
   * we should not be paying any additional runtime cost.  Then we just expose both types as implicit
   * conversions (e.g. operator int) so that when the object is passed to an HDF5 API function it
   * automatically reads from the appropriate member as well. */
  struct handle
  {
    union
    {
      int     id_int;     // old hdf5 api
      int64_t id_int64;   // new hdf5 api
    };

    handle() noexcept;
    handle(int id) noexcept : id_int{id} { }
    handle(int64_t id) noexcept : id_int64{id} { }
    handle(const handle& rhs);
    handle(handle&& rhs) noexcept;
    auto operator=(const handle& rhs) -> handle&;
    auto operator=(handle&& rhs) noexcept -> handle&;
    ~handle();

    operator int() const noexcept     { return id_int; }
    operator int64_t() const noexcept { return id_int64; }
    operator bool() const noexcept;
  };

  /// Exception thrown to indicate I/O errors
  class error : public std::runtime_error
  {
  public:
    error(const char* what);
  };

  /// Attribute handle
  class attribute
  {
  public:
    /// Attribute data types
    enum class data_type
    {
        uninitialized     ///< New attribute which has not been set yet
      , unknown           ///< Unknown data type
      , boolean           ///< Indicates bool
      , integer           ///< Indicates long
      , real              ///< Indicates double
      , string            ///< Indicates string
      , integer_array     ///< Indicates vector<long>
      , real_array        ///< Indicates vector<double>
    };

  public:
    /// Get attribute name
    auto name() const -> const string&                     { return name_; }

    /// Get data type of attribute
    auto type() const -> data_type;

    /// Get the attribute as a bool
    auto get_boolean() const -> bool;
    /// Get the attribute as a long
    auto get_integer() const -> long;
    /// Get the attribute as a double
    auto get_real() const -> double;
    /// Get the attribute as a string
    auto get_string() const -> string;
    /// Get the attribute as a vector of longs
    auto get_integer_array() const -> vector<long>;
    /// Get the attribute as a vector of doubles
    auto get_real_array() const -> vector<double>;

    /// Set the attribute
    auto set(bool val) -> void;
    /// Set the attribute
    auto set(long val) -> void;
    /// Set the attribute
    auto set(double val) -> void;
    /// Set the attribute
    auto set(const char* val) -> void;
    /// Set the attribute
    auto set(const string& val) -> void;
    /// Set the attribute
    auto set(const vector<long>& val) -> void;
    /// Set the attribute
    auto set(const vector<double>& val) -> void;

  private:
    attribute(const handle* parent, string name, bool existing);
    auto open(handle* type_out = nullptr) const -> handle;
    auto open_or_create(data_type type, size_t size, handle* type_out = nullptr) -> handle;

  private:
    const handle*     parent_;
    string            name_;
    mutable data_type type_;
    mutable size_t    size_;      // number of elements in array or characters in string

    friend class attribute_store;
    friend class data;
    friend class file;
  };

  /// Interface to metadata attributes at a particular level
  class attribute_store
  {
  private:
    typedef vector<attribute> store_impl;

  public:
    /// Iterator used for traversing the store
    typedef store_impl::iterator iterator;
    /// Constant iterator used for traversing the store
    typedef store_impl::const_iterator const_iterator;
    /// Iterator used for traversing the store in reverse
    typedef store_impl::reverse_iterator reverse_iterator;
    /// Constant iterator used for traversing the store in reverse
    typedef store_impl::const_reverse_iterator const_reverse_iterator;

  public:
    /// Get the number of attributes in the store
    auto size() const noexcept -> size_t                        { return attrs_.size(); }

    /// Get an iterator to the first attribute in the store
    auto begin() noexcept -> iterator                           { return attrs_.begin(); }
    /// Get an iterator to the first attribute in the store
    auto begin() const noexcept -> const_iterator               { return attrs_.begin(); }
    /// Get an iterator to the first attribute in the store
    auto cbegin() const noexcept -> const_iterator              { return attrs_.begin(); }
    /// Get an iterator to the first attribute in the store (reversed)
    auto rbegin() noexcept -> reverse_iterator                  { return attrs_.rbegin(); }
    /// Get an iterator to the first attribute in the store (reversed)
    auto rbegin() const noexcept -> const_reverse_iterator      { return attrs_.rbegin(); }
    /// Get an iterator to the first attribute in the store (reversed)
    auto crbegin() const noexcept -> const_reverse_iterator     { return attrs_.rbegin(); }

    /// Get an iterator referring to the past-the-end attribute in the store
    auto end() noexcept -> iterator                             { return attrs_.end(); }
    /// Get an iterator referring to the past-the-end attribute in the store
    auto end() const noexcept -> const_iterator                 { return attrs_.end(); }
    /// Get an iterator referring to the past-the-end attribute in the store
    auto cend() const noexcept -> const_iterator                { return attrs_.end(); }
    /// Get an iterator referring to the past-the-end attribute in the store (reversed)
    auto rend() noexcept -> reverse_iterator                    { return attrs_.rend(); }
    /// Get an iterator referring to the past-the-end attribute in the store (reversed)
    auto rend() const noexcept -> const_reverse_iterator        { return attrs_.rend(); }
    /// Get an iterator referring to the past-the-end attribute in the store (reversed)
    auto crend() const noexcept -> const_reverse_iterator       { return attrs_.rend(); }

    /// Find an attribute by name
    auto find(const char* name) noexcept -> iterator;
    /// Find an attribute by name
    auto find(const char* name) const noexcept -> const_iterator;
    /// Find an attribute by name
    auto find(const string& name) noexcept -> iterator;
    /// Find an attribute by name
    auto find(const string& name) const noexcept -> const_iterator;

    /// Get an attribute by name and create if not found
    auto operator[](const char* name) -> attribute&;
    /// Get an attribute by name and throw if not found
    auto operator[](const char* name) const -> const attribute&;
    /// Get an attribute by name and create if not found
    auto operator[](const string& name) -> attribute&      { return operator[](name.c_str()); }
    /// Get an attribute by name and throw if not found
    auto operator[](const string& name) const -> const attribute& { return operator[](name.c_str()); }

    /// Erase an attribute from the store
    auto erase(iterator i) -> void;
    /// Erase an attribute from the store
    auto erase(const string& name) -> void;

  protected:
    attribute_store(handle hnd, bool existing);
    attribute_store(const handle& parent, const char* name, size_t index, bool existing);

    attribute_store(const attribute_store& rhs);
    attribute_store(attribute_store&& rhs) noexcept;

    auto operator=(const attribute_store& rhs) -> attribute_store&;
    auto operator=(attribute_store&& rhs) noexcept -> attribute_store&;

    auto fix_attribute_parents(const attribute_store& old) -> void;

  protected:
    handle      hnd_;
    handle      what_;
    handle      where_;
    handle      how_;
    store_impl  attrs_;
  };

  /// Base class for ODIM_H5 objects with 'what', 'where' and 'how' attributes
  class group : protected attribute_store
  {
  public:
    virtual ~group();

    /// Get the attributes stored at this level
    auto attributes() -> attribute_store&                       { return *this; }
    /// Get the attributes stored at this level
    auto attributes() const -> const attribute_store&           { return *this; }

    /// Determine whether the named attribute is directly accessible through the object API
    virtual auto is_api_attribute(const string& name) const -> bool;

  protected:
    group(handle hnd, bool existing);
    group(const handle& parent, const char* name, size_t index, bool existing);
  };

  /// Dataset object
  class data : public group
  {
  public:
    /// Storage type for data
    enum class data_type
    {
        unknown   ///< Unsupported storage type
      , i8        ///< 8 bit signed integer
      , u8        ///< 8 bit unsigned integer
      , i16       ///< 16 bit signed integer
      , u16       ///< 16 bit unsigned integer
      , i32       ///< 32 bit signed integer
      , u32       ///< 32 bit unsigned integer
      , i64       ///< 64 bit signed integer
      , u64       ///< 64 bit unsigned integer
      , f32       ///< 32 bit float
      , f64       ///< 64 bit float
    };

    /// Maximum supported dataset rank
    constexpr static size_t max_rank = 32;

    /// Default compression level
    constexpr static int default_compression = 6;

  public:
    /// Get the number of quality layers
    auto quality_count() const -> size_t                        { return size_quality_; }
    /// Open a quality layer
    auto quality_open(size_t i) const -> data;
    /// Append a new quality layer
    auto quality_append(
          data_type type
        , size_t rank
        , const size_t* dims
        , int compression = default_compression
        ) -> data;

    /// Get the type used to store dataset in file
    auto type() const -> data_type;
    /// Get the number of dimensions used by the dataset
    auto rank() const -> size_t;
    /// Get the size of each dataset dimension
    auto dims(size_t* val) const -> size_t;
    /// Get the total number of points in the dataset
    auto size() const -> size_t;

    /// Get the quantity identifier
    auto quantity() const -> string;
    /// Set the quantity identifier
    auto set_quantity(const string& val) -> void;

    /// Get the gain used to unpack values
    auto gain() const -> double;
    /// Set the gain used to unpack values
    auto set_gain(double val) -> void;

    /// Get the offset used to unpack values
    auto offset() const -> double;
    /// Set the offset used to unpack values
    auto set_offset(double val) -> void;

    /// Get the packed value of the no data indicator
    auto nodata() const -> double;
    /// Set the packed value of the no data indicator
    auto set_nodata(double val) -> void;

    /// Get the packed value of the no detection indicator
    auto undetect() const -> double;
    /// Set the packed value of the no detection indicator
    auto set_undetect(double val) -> void;

    auto is_api_attribute(const string& name) const -> bool;

    // all the POD types except for bool and pointers are supported by read() and write()

    /// Read the dataset without unpacking
    template <typename T>
    auto read(T* data) const -> void;

    /// Unpack and read the dataset, replace nodata and undetect with user values
    template <typename T>
    auto read_unpack(T* data, T undetect, T nodata) const -> void;

    /// Write the dataset without packing
    template <typename T>
    auto write(const T* data) -> void;

    /// Pack and write the dataset, use passed functors to test for undetect and nodata
    template <typename T, class UndetectTest, class NoDataTest>
    auto write_pack(const T* data, UndetectTest is_undetect, NoDataTest is_nodata) -> void;

  protected:
    data(const handle& parent, bool quality, size_t index);
    data(
          const handle& parent
        , bool quality
        , size_t index
        , data_type type
        , size_t rank
        , const size_t* dims
        , int compression);

  protected:
    size_t  size_quality_;
    handle  data_;

    friend class dataset;
  };

  template <typename T>
  auto data::read_unpack(T* data, T undetect, T nodata) const -> void
  {
    const auto size = this->size();
    const auto ndf = this->nodata();
    const auto udf = this->undetect();
    const auto a = gain();
    const auto b = offset();

    auto impl = [&](auto dummy)
    {
      using ptype = decltype(dummy);

      /* since nodata and undetect are always double, a crazy user could set them to values outside the range that
       * can be represented by the packed data type.  if we don't manually detect this situation, these values
       * would end up modulus by the packed type, and map to some random value causing nearly impossible to find
       * bugs.  so the correct thing to do is ensure that a nodata or undetect value outside the encoded range is
       * treated as if it will never occur.  rather than checking the ranges, we just check if the double value is
       * exactly representable.  this means we can also cope with the crazy user situation where nodata is set to
       * a non-integer value even though an integer type is used for the packing.
       *
       * technically if the value overflows then the casts below area already undefined behaviour.  our sane checks
       * are also questionable.  maybe in the future we could implement a perfectly safe an standards compliant
       * function for "is_exactly_representable<T>(val)".  this is non-trivial though so we use the naive check
       * of double -> int -> double == original double for now. */
      const auto nd = static_cast<ptype>(ndf);
      const auto ud = static_cast<ptype>(udf);
      const auto sane_nd = (ndf == static_cast<decltype(ndf)>(nd));
      const auto sane_ud = (udf == static_cast<decltype(ndf)>(ud));

      if (!sane_nd)
        trace::warning("odim nodata value {} is not representable in packed type {} and cannot occur", ndf, type());
      if (!sane_ud)
        trace::warning("odim undetect value {} is not representable in packed type {} and cannot occur", udf, type());

      auto buf = std::make_unique<ptype[]>(size);
      read(buf.get());

      for (size_t i = 0; i < size; ++i)
      {
        if (sane_ud && buf[i] == ud)
          data[i] = undetect;
        else if (sane_nd && buf[i] == nd)
          data[i] = nodata;
        else
          data[i] = buf[i] * a + b;
      }
    };

    switch (type())
    {
    case data_type::unknown:
      throw std::runtime_error{"odim error: unknown data type"};
    case data_type::i8:  impl(int8_t(0)); break;
    case data_type::u8:  impl(uint8_t(0)); break;
    case data_type::i16: impl(int16_t(0)); break;
    case data_type::u16: impl(uint16_t(0)); break;
    case data_type::i32: impl(int32_t(0)); break;
    case data_type::u32: impl(uint32_t(0)); break;
    case data_type::i64: impl(int64_t(0)); break;
    case data_type::u64: impl(uint64_t(0)); break;
    case data_type::f32: impl(float(0)); break;
    case data_type::f64: impl(double(0)); break;
    }
  }

  template <typename T, class UndetectTest, class NoDataTest>
  auto data::write_pack(const T* data, UndetectTest is_undetect, NoDataTest is_nodata) -> void
  {
    const auto size = this->size();
    const auto ndf = this->nodata();
    const auto udf = this->undetect();
    const auto a = gain();
    const auto b = offset();

    auto impl = [&](auto dummy)
    {
      using ptype = decltype(dummy);

      const auto nd = static_cast<ptype>(ndf);
      const auto ud = static_cast<ptype>(udf);
      const auto sane_nd = (ndf == static_cast<decltype(ndf)>(nd));
      const auto sane_ud = (udf == static_cast<decltype(ndf)>(ud));

      auto buf = std::make_unique<ptype[]>(size);
      for (size_t i = 0; i < size; ++i)
      {
        if (is_undetect(data[i]))
        {
          if (!sane_ud)
            throw std::runtime_error{fmt::format("odim error: undetect value {} is not representable in packed type {}", udf, type())};
          buf[i] = ud;
        }
        else if (is_nodata(data[i]))
        {
          if (!sane_nd)
            throw std::runtime_error{fmt::format("odim error: nodata value {} is not representable in packed type {}", ndf, type())};
          buf[i] = nd;
        }
        else if constexpr (std::is_integral_v<ptype>)
        {
          auto val = std::lround((data[i] - b) / a);
          // detect out-of-range conversion to integral ptype
          // TODO consider checking whether we've mapped to nd or ud, too
          if (val > std::numeric_limits<ptype>::max() || val < std::numeric_limits<ptype>::min())
            throw std::runtime_error{fmt::format("odim error: value {} is out-of-range in packed type {}", data[i], type())};
          buf[i] = val;
        }
        else
          buf[i] = (data[i] - b) / a;
      }

      write(buf.get());
    };

    switch (type())
    {
    case data_type::unknown:
      throw std::runtime_error{"odim error: unknown data type"};
    case data_type::i8:  impl(int8_t(0)); break;
    case data_type::u8:  impl(uint8_t(0)); break;
    case data_type::i16: impl(int16_t(0)); break;
    case data_type::u16: impl(uint16_t(0)); break;
    case data_type::i32: impl(int32_t(0)); break;
    case data_type::u32: impl(uint32_t(0)); break;
    case data_type::i64: impl(int64_t(0)); break;
    case data_type::u64: impl(uint64_t(0)); break;
    case data_type::f32: impl(float(0)); break;
    case data_type::f64: impl(double(0)); break;
    }
  }

  /// Dataset group which contains data and optional quality layers
  class dataset : public group
  {
  public:
    /// Get the number of data layers
    auto data_count() const -> size_t                           { return size_data_; }
    /// Open a data layer
    auto data_open(size_t i) const -> data;
    /// Append a data layer
    auto data_append(
          data::data_type type
        , size_t rank
        , const size_t* dims
        , int compression = data::default_compression
        ) -> data;

    /// Get the number of quality layers
    auto quality_count() const -> size_t                        { return size_quality_; }
    /// Open a quality layer
    auto quality_open(size_t i) const -> data;
    /// Append a new quality layer
    auto quality_append(
          data::data_type type
        , size_t rank
        , const size_t* dims
        , int compression = data::default_compression
        ) -> data;

  protected:
    dataset(const handle& parent, size_t index, bool existing);

  protected:
    size_t  size_data_;
    size_t  size_quality_;

    friend class file;
  };

  /// Generic ODIM_H5 file
  class file : public group
  {
  public:
    /// ODIM_H5 file scope object types
    enum class object_type
    {
        unknown
      , polar_volume
      , cartesian_volume
      , polar_scan
      , polar_ray
      , azimuthal_object
      , elevational_object
      , cartesian_image
      , composite_image
      , vertical_cross_section
      , vertical_profile
      , graphical_image
    };

  public:
    /// Open or create an ODIM_H5 file
    /**
     * \param path  Path of file to open
     * \param mode  Mode of open
     */
    file(const string& path, io_mode mode);

    /// Get the io_mode used to open the file
    auto mode() const noexcept -> io_mode                       { return mode_; }

    /// Ensure all write actions have been synced to disk
    auto flush() -> void;

    /// Get the number of datasets in the file
    auto dataset_count() const -> size_t                        { return size_; }
    /// Open a dataset
    auto dataset_open(size_t i) const -> dataset                { return dset_open_as<dataset>(i); }
    /// Append a new dataset
    auto dataset_append() -> dataset                            { return dset_make_as<dataset>(); }
    /// Append a new dataset by copying it from another file
    auto dataset_copy(dataset const& ds) -> dataset;

    /// Get the ODIM_H5 conventions string
    auto conventions() const -> string;
    /// Set the ODIM_H5 conventions string
    auto set_conventions(const string& val) -> void;

    /// Get the file object type
    auto object() const -> object_type                          { return type_; }
    /// Set the file object type
    auto set_object(object_type type) -> void;

    /// Get the ODIM_H5 version number
    auto version() const -> pair<int, int>;
    /// Set the ODIM_H5 version number
    auto set_version(int major, int minor) -> void;

    /// Get the product date string
    auto date() const -> string;
    /// Set the product date string
    auto set_date(const string& val) -> void;

    /// Get the product time string
    auto time() const -> string;
    /// Set the product time string
    auto set_time(const string& val) -> void;

    /// Get the product date and time
    auto date_time() const -> timestamp;
    /// Set the product date and time using a timestamp
    auto set_date_time(timestamp val) -> void;

    /// Get the product source identifier string
    auto source() const -> string;
    /// Set the proudct source identifier string
    auto set_source(const string& val) -> void;

    auto is_api_attribute(const string& name) const -> bool;

  protected:
    template <class T> auto dset_open_as(size_t i) const -> T;
    template <class T> auto dset_make_as() -> T;

  protected:
    io_mode     mode_;
    object_type type_;
    size_t      size_;
  };

  //----------------------------------------------------------------------------
  // product specific APIs (wrappers around the above classes):

  /// Polar scan object (datasetX level)
  class scan : public dataset
  {
  public:
    /// Get the antenna elevation angle
    auto elevation_angle() const -> double;
    /// Set the antenna elevation angle
    auto set_elevation_angle(double val) -> void;

    /// Get the number of range bins in each ray
    auto bin_count() const -> long;
    /// Set the number of range bins in each ray
    auto set_bin_count(long val) -> void;

    /// Get the range of the start of the first range bin (km)
    auto range_start() const -> double;
    /// Set the range of the start of the first range bin (km)
    auto set_range_start(double val) -> void;

    /// Get the distance between sucessive range bins (m)
    auto range_scale() const -> double;
    /// Set the distance between sucessive range bins (m)
    auto set_range_scale(double val) -> void;

    /// Get the number of azimuth gates in the scan
    auto ray_count() const -> long;
    /// Set the number of azimuth gates in the scan
    auto set_ray_count(long val) -> void;

    /// Get the azimuthal offset of the CCW edge of the first ray from north (degrees)
    auto ray_start() const -> double;
    /// Set the azimuthal offset of the CCW edge of the first ray from north (degrees)
    auto set_ray_start(double val) -> void;

    /// Get the index of the first azimuth gate radiated
    auto first_ray_radiated() const -> long;
    /// Set the index of the first azimuth gate radiated
    auto set_first_ray_radiated(long val) -> void;

    /// Get the scan start date string
    auto start_date() const -> string;
    /// Set the scan start date string
    auto set_start_date(const string& val) -> void;

    /// Get the scan start time string
    auto start_time() const -> string;
    /// Set the scan start time string
    auto set_start_time(const string& val) -> void;

    /// Get the scan start date and time as a timestamp
    auto start_date_time() const -> timestamp;
    /// Set the scan start date and time using a timestamp
    auto set_start_date_time(timestamp val) -> void;

    /// Get the scan end date string
    auto end_date() const -> string;
    /// Set the scan end date string
    auto set_end_date(const string& val) -> void;

    /// Get the scan end time string
    auto end_time() const -> string;
    /// Set the scan end time string
    auto set_end_time(const string& val) -> void;

    /// Get the scan end date and time as a timestamp
    auto end_date_time() const -> timestamp;
    /// Set the scan end date and time using a timestamp
    auto set_end_date_time(timestamp val) -> void;

    auto is_api_attribute(const string& name) const -> bool;

  protected:
    scan(const handle& parent, size_t index, bool existing) : dataset(parent, index, existing) { }
    friend class file;
  };

  /// Polar volume ODIM_H5 file
  class polar_volume : public file
  {
  public:
    /// Open or create a polar volume ODIM_H5 file
    polar_volume(const string& path, io_mode mode);
    /// Cast an open ODIM_H5 file to a polar volume handle
    polar_volume(file f);

    /// Get the number of scans in the volume
    auto scan_count() const -> size_t                           { return dataset_count(); }
    /// Open a scan
    auto scan_open(size_t i) const -> scan                      { return dset_open_as<scan>(i); }
    /// Append a new scan
    auto scan_append() -> scan                                  { return dset_make_as<scan>(); }

    /// Get the longitude of the antenna
    auto longitude() const -> double;
    /// Set the longitude of the antenna
    auto set_longitude(double val) -> void;

    /// Get the latitude of the antenna
    auto latitude() const -> double;
    /// Set the latitude of the antenna
    auto set_latitude(double val) -> void;

    /// Get the height above sea level of the antenna center
    auto height() const -> double;
    /// Set the height above sea level of the antenna center
    auto set_height(double val) -> void;

    auto is_api_attribute(const string& name) const -> bool;
  };

  /// Vertical profile object (datasetX level)
  class profile : public dataset
  {
  public:
    /// Get the profile start date string
    auto start_date() const -> string;
    /// Set the profile start date string
    auto set_start_date(const string& val) -> void;

    /// Get the profile start time string
    auto start_time() const -> string;
    /// Set the profile start time string
    auto set_start_time(const string& val) -> void;

    /// Get the profile start date and time as a timestamp
    auto start_date_time() const -> timestamp;
    /// Set the profile start date and time using a timestamp
    auto set_start_date_time(timestamp val) -> void;

    /// Get the profile end date string
    auto end_date() const -> string;
    /// Set the profile end date string
    auto set_end_date(const string& val) -> void;

    /// Get the profile end time string
    auto end_time() const -> string;
    /// Set the profile end time string
    auto set_end_time(const string& val) -> void;

    /// Get the profile end date and time as a timestamp
    auto end_date_time() const -> timestamp;
    /// Set the profile end date and time using a timestamp
    auto set_end_date_time(timestamp val) -> void;

    auto is_api_attribute(const string& name) const -> bool;

  protected:
    profile(const handle& parent, size_t index, bool existing) : dataset(parent, index, existing) { }
    friend class file;
  };

  /// Vertical profile ODIM_H5 file
  class vertical_profile : public file
  {
  public:
    /// Open or create a polar volume ODIM_H5 file
    vertical_profile(const string& path, io_mode mode);
    /// Cast an open ODIM_H5 file to a polar volume handle
    vertical_profile(file f);

    /// Get the number of profiles in the volume
    auto profile_count() const -> size_t                        { return dataset_count(); }
    /// Open a profile
    auto profile_open(size_t i) const -> profile                { return dset_open_as<profile>(i); }
    /// Append a new profile
    auto profile_append() -> profile                            { return dset_make_as<profile>(); }

    /// Get the longitude of the antenna
    auto longitude() const -> double;
    /// Set the longitude of the antenna
    auto set_longitude(double val) -> void;

    /// Get the latitude of the antenna
    auto latitude() const -> double;
    /// Set the latitude of the antenna
    auto set_latitude(double val) -> void;

    /// Get the height above sea level of the antenna center
    auto height() const -> double;
    /// Set the height above sea level of the antenna center
    auto set_height(double val) -> void;

    /// Get the number of levels in the profile
    auto level_count() const -> long;
    /// Set the number of levels in the profile
    auto set_level_count(long val) -> void;

    /// Get the vertical distance between levels
    auto interval() const -> double;
    /// Set the vertical distance between levels
    auto set_interval(double val) -> void;

    /// Get the minimum height above sea level
    auto min_height() const -> double;
    /// Set the minimum height above sea level
    auto set_min_height(double val) -> void;

    /// Get the maximum height above sea level
    auto max_height() const -> double;
    /// Set the maximum height above sea level
    auto set_max_height(double val) -> void;

    auto is_api_attribute(const string& name) const -> bool;
  };

  /* efficient use of library:
   *
   * // best...
   * polar_volume vol("path.vol.h5", io_mode::read_only);
   *
   * // next best...
   * file f("path.vol.h5", io_mode::read_only);
   * if (f.object() == object_type::polar_volume)
   * {
   *   polar_volume vol{std::move(f)};
   *   ...
   * }
   *
   * // still okay (extra H5Iinc/decs)
   * file f("path.vol.h5", io_mode::read_only);
   * if (f.object() == object_type::polar_volume)
   * {
   *   polar_volume vol{f};
   *   do_something(static_cast<polar_volume>(f));
   * }
   */
}

namespace bom
{
  BOM_DECLARE_ENUM_TRAITS(io::odim::data::data_type, unknown, i8, u8, i16, u16, i32, u32, i64, u64, f32, f64);
}

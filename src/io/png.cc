/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "png.h"
#include <png.h>
#include <bom/file_utils.h>
#include <bom/trace.h>
#include <cstddef>

using namespace bom;
using namespace bom::io;

namespace
{
  // ensure that colour array elements pack to 4 bytes.  this is what we expect in the libpng code below
  struct dummy { colour foo[2]; };
  static_assert(offsetof(dummy, foo[1]) - offsetof(dummy, foo[0]) == 4, "colours in array do not pack to 4 bytes!");
}

static void png_user_error_fn(png_structp png_ptr, png_const_charp error_msg)
{
  trace::error("error in libpng: {}", error_msg);
}

static void png_user_warning_fn(png_structp png_ptr, png_const_charp warning_msg)
{
  trace::warning("error in libpng: {}", warning_msg);
}

static void png_user_read_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
  FILE* fp = static_cast<FILE*>(png_get_io_ptr(png_ptr));
  if (fread(data, length, 1, fp) != 1)
    png_error(png_ptr, "libpng read error");
}

auto bom::io::png_read(string const& path) -> image
{
  // open our actual file to read
  auto fp = file_stream_hnd{fopen(path.c_str(), "rb")};
  if (!fp)
    throw std::runtime_error("unable to open file for png read: " + path);

  // setup library for reading
  png_structp png_ptr = png_create_read_struct(
        PNG_LIBPNG_VER_STRING
      , nullptr
      , png_user_error_fn
      , png_user_warning_fn);
  if (!png_ptr)
    throw std::runtime_error("unable to init png_struct");

  png_set_read_fn(png_ptr, fp, png_user_read_data);

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
  {
    png_destroy_read_struct(&png_ptr, (png_infopp) nullptr, (png_infopp) nullptr);
    throw std::runtime_error("unable to init png_info");
  }

  png_infop end_info = png_create_info_struct(png_ptr);
  if (!end_info)
  {
    png_destroy_read_struct(&png_ptr, (png_infopp) &info_ptr, (png_infopp) nullptr);
    throw std::runtime_error("unable to init png_info");
  }

  // setup error handling
  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    throw std::runtime_error("png read failed: " + path);
  }

  // read header info
  png_read_info(png_ptr, info_ptr);

  // get our size and format
  auto size_x = png_get_image_width(png_ptr, info_ptr);
  auto size_y = png_get_image_height(png_ptr, info_ptr);
  auto bit_depth = png_get_bit_depth(png_ptr, info_ptr);
  auto color_type = png_get_color_type(png_ptr, info_ptr);

  // convert all formats to 8-bit RGBA
  if (bit_depth == 16)
    png_set_strip_16(png_ptr);
  if (color_type == PNG_COLOR_TYPE_PALETTE)
    png_set_palette_to_rgb(png_ptr);
  if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
    png_set_expand_gray_1_2_4_to_8(png_ptr);
  if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
    png_set_tRNS_to_alpha(png_ptr);
  if (color_type == PNG_COLOR_TYPE_RGB || color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_PALETTE)
    png_set_filler(png_ptr, 0xff, PNG_FILLER_AFTER);
  if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
    png_set_gray_to_rgb(png_ptr);

  // flush our format transformations into the info structure and sanity check our stride
  png_read_update_info(png_ptr, info_ptr);
  if (png_get_rowbytes(png_ptr, info_ptr) != size_x * 4)
    png_error(png_ptr, "libpng unexpected stride");

  image img{{size_x, size_y}};

  // initialize the row pointers
  unique_ptr<png_byte*[]> rows(new png_byte*[img.extents().y]);
  for (size_t i = 0; i < img.extents().y; ++i)
    rows[i] = (png_byte*) img[i];

  // acutal reading
  png_read_image(png_ptr, rows.get());

  // cleanup
  png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);

  return img;
}

auto bom::io::png_write(string const& path, image const& img, vector<pair<string, string>> metadata) -> void
{
  auto fp = file_stream_hnd{fopen(path.c_str(), "wb")};
  if (!fp)
    throw std::runtime_error("unable to open file for png write: " + path);

  png_structp png_ptr = png_create_write_struct(
        PNG_LIBPNG_VER_STRING
      , nullptr
      , png_user_error_fn
      , png_user_warning_fn);
  if (!png_ptr)
    throw std::runtime_error("unable to init png_struct");

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
  {
    png_destroy_write_struct(&png_ptr, (png_infopp) nullptr);
    throw std::runtime_error("unable to init png_info");
  }

  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_write_struct(&png_ptr, &info_ptr);
    throw std::runtime_error("png write failed");
  }

  png_init_io(png_ptr, fp);

  png_set_IHDR(
        png_ptr
      , info_ptr
      , img.extents().x
      , img.extents().y
      , 8
      , PNG_COLOR_TYPE_RGB_ALPHA
      , PNG_INTERLACE_NONE
      , PNG_COMPRESSION_TYPE_DEFAULT
      , PNG_FILTER_TYPE_DEFAULT);

  vector<png_text> meta(metadata.size());
  std::memset(meta.data(), sizeof(png_text), meta.size());
  for (size_t i = 0; i < meta.size(); ++i)
  {
    /* we can't use ITXT (which is UTF-8) with libpng < 1.6 because it incorrectly insists on setting the language
     * and translated keyword tags.  until we are sure everyone uses libpng 1.6 or better we use tEXt blocks instead
     * of the much nicer iTXt blocks. */
    //meta[i].compression = PNG_ITXT_COMPRESSION_NONE;
    meta[i].compression = PNG_TEXT_COMPRESSION_NONE;
    meta[i].key = const_cast<char*>(metadata[i].first.c_str());
    meta[i].text = const_cast<char*>(metadata[i].second.c_str());
  }
  png_set_text(png_ptr, info_ptr, meta.data(), meta.size());

  png_write_info(png_ptr, info_ptr);

  // initialize the row pointers
  unique_ptr<png_byte*[]> rows(new png_byte*[img.extents().y]);
  for (size_t i = 0; i < img.extents().y; ++i)
    rows[i] = (png_byte*) img[i];

  // dump it out!
  png_write_image(png_ptr, rows.get());
  png_write_end(png_ptr, nullptr);
  png_destroy_write_struct(&png_ptr, &info_ptr);
}

auto bom::io::png_write(vector<char>& buf, image const& img, vector<pair<string, string>> metadata) -> void
{
  png_structp png_ptr = png_create_write_struct(
        PNG_LIBPNG_VER_STRING
      , nullptr
      , png_user_error_fn
      , png_user_warning_fn);
  if (!png_ptr)
    throw std::runtime_error("unable to init png_struct");

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
  {
    png_destroy_write_struct(&png_ptr, (png_infopp) nullptr);
    throw std::runtime_error("unable to init png_info");
  }

  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_write_struct(&png_ptr, &info_ptr);
    throw std::runtime_error("png write failed");
  }

  /* we tweak the capacity of buf to minimise overallocation of the buffer:
   * - small initial allocation of 128 is enough to cover the headers which are typically a sequence of writes which
   *   are 8, 8, 13, 4, 8 bytes long.  added some extra for safety in future versions.
   * - there are typically only a few big writes which depend on the size of the image itself.  we always allocate
   *   to this size plus an extra 32 bytes to minimize the doubling growth of std::vector which will always result in
   *   almost double the amount of memory as necessary being allocated.
   * - the big writes are followed by small footers which are usually 4, 8, 4 bytes, so the extra 32 means we avoid
   *   an extra reallocation just to fit the footers. */
  buf.reserve(128); // reserve enough for the header writes (typically 8 + 8 + 13 + 4 + 8)
  auto cb_write = [](png_structp png_ptr, png_bytep data, png_size_t length) -> void
  {
    auto bufptr = static_cast<vector<char>*>(png_get_io_ptr(png_ptr));
    if (bufptr->capacity() < bufptr->size() + length)
      bufptr->reserve(bufptr->size() + length + 32);
    bufptr->insert(bufptr->end(), data, data + length);
  };
  auto cb_flush = [](png_structp png_ptr) { };
  png_set_write_fn(png_ptr, &buf, cb_write, cb_flush);

  png_set_IHDR(
        png_ptr
      , info_ptr
      , img.extents().x
      , img.extents().y
      , 8
      , PNG_COLOR_TYPE_RGB_ALPHA
      , PNG_INTERLACE_NONE
      , PNG_COMPRESSION_TYPE_DEFAULT
      , PNG_FILTER_TYPE_DEFAULT);

  vector<png_text> meta(metadata.size());
  std::memset(meta.data(), sizeof(png_text), meta.size());
  for (size_t i = 0; i < meta.size(); ++i)
  {
    /* we can't use ITXT (which is UTF-8) with libpng < 1.6 because it incorrectly insists on setting the language
     * and translated keyword tags.  until we are sure everyone uses libpng 1.6 or better we use tEXt blocks instead
     * of the much nicer iTXt blocks. */
    //meta[i].compression = PNG_ITXT_COMPRESSION_NONE;
    meta[i].compression = PNG_TEXT_COMPRESSION_NONE;
    meta[i].key = const_cast<char*>(metadata[i].first.c_str());
    meta[i].text = const_cast<char*>(metadata[i].second.c_str());
  }
  png_set_text(png_ptr, info_ptr, meta.data(), meta.size());

  png_write_info(png_ptr, info_ptr);

  // initialize the row pointers
  unique_ptr<png_byte*[]> rows(new png_byte*[img.extents().y]);
  for (size_t i = 0; i < img.extents().y; ++i)
    rows[i] = (png_byte*) img[i];

  // dump it out!
  png_write_image(png_ptr, rows.get());
  png_write_end(png_ptr, nullptr);
  png_destroy_write_struct(&png_ptr, &info_ptr);
}

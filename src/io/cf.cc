/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "cf.h"
#include "../unit.h"
#include <bom/array2.h>
#include <bom/ellipsoid.h>
#include <bom/trace.h>
#include <bom/unit_test.h>

using namespace bom;
using namespace bom::io;
using namespace bom::io::cf;

/* CF mandates that latitude/longitude coordinates MUST have the following units set.
 * You can also use axis and standard_name to help identify, but since these are
 * optional and units is mandatory we just check for that. */
static auto is_latitude_cv(nc::variable const& var) -> bool
{
  auto units = var.att_get_as("units", string{});
  return
       units == "degrees_north"
    || units == "degree_north"
    || units == "degree_N"
    || units == "degrees_N"
    || units == "degreeN"
    || units == "degreesN";
}

static auto is_longitude_cv(nc::variable const& var) -> bool
{
  auto units = var.att_get_as("units", string{});
  return
       units == "degrees_east"
    || units == "degree_east"
    || units == "degree_E"
    || units == "degrees_E"
    || units == "degreeE"
    || units == "degreesE";
}

// TODO - use UDUNITS to perform this function to ensure support for full set of representations
auto bom::io::cf::parse_time_units(string const& str) -> pair<time_unit, timestamp>
{
  pair<time_unit, timestamp> ret;

  // find the first space (end of unit)
  auto uend = str.find(' ');

  // determine the units
  if (str.compare(0, uend, "seconds") == 0)
    ret.first = time_unit::seconds;
  else if (str.compare(0, uend, "minutes") == 0)
    ret.first = time_unit::minutes;
  else if (str.compare(0, uend, "hours") == 0)
    ret.first = time_unit::hours;
  else if (str.compare(0, uend, "days") == 0)
    ret.first = time_unit::days;
  else
    throw std::runtime_error{fmt::format("unsupported CF time unit: {}", str)};

  // find the second space (end of 'since')
  auto send = str.find(' ', uend + 4); // + 4 since shortest units is days and we can safely skip these characters

  // ensure 'since' was present
  if (str.compare(uend, send - uend, " since") != 0)
    throw std::runtime_error{fmt::format("unsupported CF time unit: {}", str)};

  // strip an optional ' UTC' from the end of the timestamp
  auto tend = str.compare(str.size() - 4, 4, " UTC") == 0 ? str.size() - 4 : str.size();

  // parse the timestamp
  ret.second = string_conversions<timestamp>::read_string(str.c_str() + send + 1, tend - send - 1);

  return ret;
}

auto bom::io::cf::read_time(nc::variable const& var, span<size_t const> start) -> timestamp
{
  double val;
  packer{}.read(var, make_span(&val, 1), nan<double>(), start);
  auto units_epoch = parse_time_units(var.att_get_as<string>("units"));
  return units_epoch.second + duration_from_time_units(val, units_epoch.first);
}

auto bom::io::cf::read_times(
      nc::variable const& var
    , span<timestamp> data
    , span<size_t const> start
    , span<size_t const> count
    ) -> void
{
  array1d val{size_t(data.size())};
  packer{}.read(var, val, nan<double>(), start, count);
  auto units_epoch = parse_time_units(var.att_get_as<string>("units"));
  for (decltype(data.size()) i = 0; i < data.size(); ++i)
    data[i] = units_epoch.second + duration_from_time_units(val[i], units_epoch.first);
}

auto bom::io::cf::find_spatial_dimensions(nc::variable const& var) -> optional<vec2z>
{
  // do we have an explicit grid mapping?
  if (auto gm = var.att_get_as<optional<string>>("grid_mapping"))
  {
    auto gmv = var.parent()->find_variable(*gm);
    if (!gmv)
      throw std::runtime_error{fmt::format("grid_mapping variable '{}' not found", *gm)};

    // determine what standard names we should be searching for
    auto sny = "latitude"; auto snx = "longitude";
    if (auto gmn = gmv->att_get_as<optional<string>>("grid_mapping_name"))
    {
      if (*gmn == "rotated_latitude_longitude")
      {
        sny = "grid_latitude";
        snx = "grid_longitude";
      }
      else if (*gmn != "latitude_longitude")
      {
        sny = "projection_y_coordinate";
        snx = "projection_x_coordinate";
      }
    }

    // search for the two standard names, y dimension _must_ preceed x (all our software relies on this)
    // we search backwards because in the overwhelming majority or cases x and y are the last two dimensions
    size_t x = 0; // safe to use 0 as sentinel since x dim can't be the outermost (i.e. 0)
    for (auto i = var.dimensions().size(); i-- > 0; )
    {
      if (auto cv = var.parent()->find_variable(var.dimensions()[i]->name()))
      {
        if (x != 0)
        {
          if (cv->att_get_as("standard_name", string{}) == sny)
            return vec2z{x, i};
        }
        else
        {
          if (cv->att_get_as("standard_name", string{}) == snx)
            x = i;
        }
      }
    }
  }
  // no grid mapping so assume we are locating geodetic coordinates
  else
  {
    // we search backwards because in the overwhelming majority or cases x and y are the last two dimensions
    size_t x = 0; // safe to use 0 as sentinel since x dim can't be the outermost (i.e. 0)
    for (auto i = var.dimensions().size(); i-- > 0; )
    {
      if (auto cv = var.parent()->find_variable(var.dimensions()[i]->name()))
      {
        if (x != 0)
        {
          if (is_latitude_cv(*cv))
            return vec2z{x, i};
        }
        else
        {
          if (is_longitude_cv(*cv))
            x = i;
        }
      }
    }
  }

  return nullopt;
}

auto bom::io::cf::lookup_spatial_dimensions(nc::variable const& var) -> vec2z
{
  if (auto ret = find_spatial_dimensions(var))
    return *ret;
  throw std::runtime_error{fmt::format("unable to determine spatial dimensions for variable '{}'", var.name())};
}

auto bom::io::cf::find_coordinates(nc::dimension const& dim) -> nc::variable const*
{
  // don't use find variable because it ascends to parents.  we only want to check if the variable exists
  // at the same level as the dimension
  for (auto& var : dim.parent()->variables())
  {
    if (var.name() == dim.name())
    {
      if (var.dimensions().size() != 1)
        throw std::runtime_error{fmt::format(
                  "coordinate variable for dimension {} is rank {} (expected 1)"
                , dim.name()
                , var.dimensions().size())};
      if (var.dimensions()[0] != &dim)
        throw std::runtime_error{fmt::format(
                  "coordinate variable for dimension {} is indexed on incorrect dimension {}"
                , dim.name()
                , var.dimensions()[0]->name())};
      return &var;
    }
  }
  return nullptr;
}

auto bom::io::cf::lookup_coordinates(nc::dimension const& dim) -> nc::variable const&
{
  if (auto ret = find_coordinates(dim))
    return *ret;
  throw std::runtime_error{fmt::format("coordinate variable for dimension {} not found", dim.name())};
}

auto bom::io::cf::copy_dimension_and_coordinates(nc::dimension const& dim, nc::group& target) -> nc::dimension&
{
  // copy the dimension first
  auto& ret = target.create_dimension(dim.name(), dim.size());

  // copy coordinates if they exist
  if (auto var = find_coordinates(dim))
  {
    target.copy_variable(*var);

    // copy bounds if they exist
    if (auto bname = var->att_get_as<optional<string>>("bounds"))
      target.copy_variable(var->parent()->lookup_variable(*bname));
  }

  return ret;
}

auto bom::io::cf::determine_proj4_definition(nc::variable const& var) -> string
try
{
  // sanity checks
  if (var.dimensions().size() < 2)
    throw std::runtime_error{"variable is not a grid"};

  // determine the grid mapping variable name (if there is one)
  auto grid_mapping = var.att_get_as<string>("grid_mapping", {});

  // if we don't have a formal grid mapping try to interpret it as a geodetic grid
  if (grid_mapping.empty())
  {
    // find the coordinate variables
    // we assume the last two dimensions here - could be more sophisticated if needed
    auto cv_lats = var.parent()->find_variable(var.dimensions()[var.dimensions().size() - 2]->name());
    auto cv_lons = var.parent()->find_variable(var.dimensions()[var.dimensions().size() - 1]->name());
    if (   !cv_lats || !is_latitude_cv(*cv_lats)
        || !cv_lons || !is_longitude_cv(*cv_lons))
      throw std::runtime_error{"no grid_mapping supplied and not a geodetic grid"};
    return fmt::format("+proj=latlong +a={} +b={}", wgs84.semi_major_axis(), wgs84.semi_minor_axis());
  }

  // if this is an extended format grid_mapping attribute then ignore the coordinate names
  if (auto pos = grid_mapping.find(':'); pos != string::npos)
  {
    // we can't cope with multiple grid mappings applied to a single variable (yet)
    if (grid_mapping.find(':', pos + 1) != string::npos)
      throw std::runtime_error{"multiple grid mapping variables unsupported"};

    // just ignore the remaining part of the string - it lists the coordinates
    grid_mapping.resize(pos);
  }

  // we have a formal grid mapping
  auto projection = var.parent()->find_variable(grid_mapping);
  if (!projection)
    throw std::runtime_error{fmt::format("unable to locate grid mapping variable '{}'", grid_mapping)};

  // build up the projection string from the parameters
  string out;
  auto iout = std::back_inserter(out);
  auto& prj = *projection;
  auto name = prj.att_get_as<string>("grid_mapping_name");
  if (name == "albers_conical_equal_area")
  {
    std::array<double, 2> sp;
    auto sp_count = prj.att_size("standard_parallel");
    if (sp_count > sp.size())
      throw std::runtime_error{"too many standard parallels provided"};
    prj.att_get("standard_parallel", make_span(sp.data(), sp_count));

    fmt::format_to(iout, "+proj=aea +lat_1={}", sp[0]);
    if (sp_count > 1)
      fmt::format_to(iout, " +lat_2={}", sp[1]);
    fmt::format_to(iout, " +lon_0={} +lat_0={}"
        , prj.att_get_as<double>("longitude_of_central_meridian")
        , prj.att_get_as<double>("latitude_of_projection_origin"));
  }
  else if (name == "azimuthal_equidistant")
  {
    fmt::format_to(iout, "+proj=aeqd +lon_0={} +lat_0={}"
        , prj.att_get_as<double>("longitude_of_projection_origin")
        , prj.att_get_as<double>("latitude_of_projection_origin"));
  }
  else if (name == "geostationary")
  {
    fmt::format_to(iout, "+proj=geos +lon_0={} +h={}"
        , prj.att_get_as<double>("longitude_of_projection_origin")
        , prj.att_get_as<double>("perspective_point_height"));
    if (prj.att_exists("sweep_angle_axis"))
      fmt::format_to(iout, " +sweep={}", prj.att_get_as<string>("sweep_angle_axis"));
    else if (prj.att_exists("fixed_angle_axis"))
      fmt::format_to(iout, " +sweep={}", prj.att_get_as<string>("fixed_angle_axis") == "x" ? "y" : "x");
  }
  else if (name == "lambert_azimuthal_equal_area")
  {
    fmt::format_to(iout, "+proj=laea +lon_0={} +lat_0={}"
        , prj.att_get_as<double>("longitude_of_projection_origin")
        , prj.att_get_as<double>("latitude_of_projection_origin"));
  }
  else if (name == "lambert_conformal_conic")
  {
    std::array<double, 2> sp;
    auto sp_count = prj.att_size("standard_parallel");
    if (sp_count > sp.size())
      throw std::runtime_error{"too many standard parallels provided"};
    prj.att_get("standard_parallel", make_span(sp.data(), sp_count));

    fmt::format_to(iout, "+proj=lcc +lat_1={}", sp[0]);
    if (sp_count > 1)
      fmt::format_to(iout, " +lat_2={}", sp[1]);
    fmt::format_to(iout, " +lon_0={} +lat_0={}"
        , prj.att_get_as<double>("longitude_of_central_meridian")
        , prj.att_get_as<double>("latitude_of_projection_origin"));
  }
  else if (name == "lambert_cylindrical_equal_area")
  {
    fmt::format_to(iout, "+proj=cea +lon_0={}", prj.att_get_as<double>("longitude_of_central_meridian"));
    if (prj.att_exists("standard_parallel"))
      fmt::format_to(iout, " +lat_ts={}", prj.att_get_as<double>("standard_parallel"));
    if (prj.att_exists("scale_factor_at_projection_origin"))
      fmt::format_to(iout, " +k_0={}", prj.att_get_as<double>("scale_factor_at_projection_origin"));
  }
  else if (name == "latitude_longitude")
  {
    fmt::format_to(iout, "+proj=latlong");
  }
  else if (name == "mercator")
  {
    fmt::format_to(iout, "+proj=merc +lon_0={}", prj.att_get_as<double>("longitude_of_projection_origin"));
    if (prj.att_exists("standard_parallel"))
      fmt::format_to(iout, " +lat_ts={}", prj.att_get_as<double>("standard_parallel"));
    if (prj.att_exists("scale_factor_at_projection_origin"))
      fmt::format_to(iout, " +k_0={}", prj.att_get_as<double>("scale_factor_at_projection_origin"));
  }
  else if (name == "oblique_mercator")
  {
    fmt::format_to(iout, "+proj=omerc +lat_0={} +lonc={} +alpha={}"
        , prj.att_get_as<double>("latitude_of_projection_origin")
        , prj.att_get_as<double>("longitude_of_projection_origin")
        , prj.att_get_as<double>("azimuth_of_central_line"));
    if (prj.att_exists("scale_factor_at_projection_origin"))
      fmt::format_to(iout, " +k_0={}", prj.att_get_as<double>("scale_factor_at_projection_origin"));
  }
  else if (name == "orthographic")
  {
    fmt::format_to(iout, "+proj=ortho +lon_0={} +lat_0={}"
        , prj.att_get_as<double>("longitude_of_projection_origin")
        , prj.att_get_as<double>("latitude_of_projection_origin"));
  }
  else if (name == "polar_stereographic")
  {
    fmt::format_to(iout, "+proj=stere +lon_0={} +lat_0={}"
        , prj.att_get_as<double>("straight_vertical_longitude_from_pole")
        , prj.att_get_as<double>("latitude_of_projection_origin"));
    if (prj.att_exists("standard_parallel"))
      fmt::format_to(iout, " +lat_ts={}", prj.att_get_as<double>("standard_parallel"));
    if (prj.att_exists("scale_factor_at_projection_origin"))
      fmt::format_to(iout, " +k_0={}", prj.att_get_as<double>("scale_factor_at_projection_origin"));
  }
  else if (name == "rotated_latitude_longitude")
  {
    throw std::runtime_error{"rotated_latitude_longitude projection is not supported"};
  }
  else if (name == "sinusoidal")
  {
    fmt::format_to(iout, "+proj=sinu +lon_0={}", prj.att_get_as<double>("longitude_of_projection_origin"));
  }
  else if (name == "stereographic")
  {
    fmt::format_to(iout, "+proj=stere +lon_0={} +lat_0={}"
        , prj.att_get_as<double>("longitude_of_central_meridian")
        , prj.att_get_as<double>("latitude_of_projection_origin"));
    if (prj.att_exists("scale_factor_at_projection_origin"))
      fmt::format_to(iout, " +k_0={}", prj.att_get_as<double>("scale_factor_at_projection_origin"));
  }
  else if (name == "transverse_mercator")
  {
    fmt::format_to(iout, "+proj=tmerc");
    if (prj.att_exists("scale_factor_at_central_meridian"))
      fmt::format_to(iout, " +k_0={}", prj.att_get_as<double>("scale_factor_at_central_meridian"));
    fmt::format_to(iout, " +lon_0={} +lat_0={}"
        , prj.att_get_as<double>("longitude_of_central_meridian")
        , prj.att_get_as<double>("latitude_of_projection_origin"));
  }
  else if (name == "vertical_perspective")
  {
    fmt::format_to(iout, "+proj=nsper +lon_0={} +lat_0={} +h={}"
        , prj.att_get_as<double>("longitude_of_projection_origin")
        , prj.att_get_as<double>("latitude_of_projection_origin")
        , prj.att_get_as<double>("perspective_point_height"));
  }
  else if (name == "gnomonic")
  {
    fmt::format_to(iout, "+proj=gnom +lon_0={} +lat_0={}"
        , prj.att_get_as<double>("longitude_of_projection_origin")
        , prj.att_get_as<double>("latitude_of_projection_origin"));
  }
  else
    throw std::runtime_error{fmt::format("unknown grid_mapping_name '{}'", name)};

  // false easting and northing have to be converted from the coordinate units into meters
  if (prj.att_exists("false_easting"))
  {
    // get the value itself
    auto value = prj.att_get_as<double>("false_easting");

    // find units used by the x coordiante variable
    string units;
    for (auto& dim : var.dimensions())
    {
      if (auto cv = var.parent()->find_variable(dim->name()))
      {
        if (cv->att_get_as<string>("standard_name", {}) == "projection_x_coordinate")
        {
          cv->att_get("units", units);
          break;
        }
      }
    }
    if (units.empty())
      throw std::runtime_error{"unable to determine units for false_easting"};

    // transform the value to meters if needed
    // its worth only converting if we absolutely have to - initializing udunits is expensive
    if (units != "m")
      value = unit::transform{unit{units}, unit{"m"s}}(value);

    // apply to the projection
    fmt::format_to(iout, " +x_0={}", value);
  }
  if (prj.att_exists("false_northing"))
  {
    // get the value itself
    auto value = prj.att_get_as<double>("false_northing");

    // find units used by the y coordinate variables
    string units;
    for (auto& dim : var.dimensions())
    {
      if (auto cv = var.parent()->find_variable(dim->name()))
      {
        if (cv->att_get_as<string>("standard_name", {}) == "projection_y_coordinate")
        {
          cv->att_get("units", units);
          break;
        }
      }
    }
    if (units.empty())
      throw std::runtime_error{"unable to determine units for false_northing"};

    // transform the value to meters if needed
    // its worth only converting if we absolutely have to - initializing udunits is expensive
    if (units != "m")
      value = unit::transform{unit{units}, unit{"m"s}}(value);

    // apply to the projection
    fmt::format_to(iout, " +y_0={}", value);
  }

  // make the units explicit (just to be on the safe side)
  if (name != "latitude_longitude")
    fmt::format_to(iout, " +units=m");

  // append the spheroid parameters
  // no need to convert units here - CF dictates these attributes are in meters
  if (!prj.att_exists("semi_major_axis"))
    fmt::format_to(iout, " +a={} +b={}", wgs84.semi_major_axis(), wgs84.semi_minor_axis());
  else if (prj.att_exists("semi_minor_axis"))
    fmt::format_to(iout, " +a={} +b={}", prj.att_get_as<double>("semi_major_axis"), prj.att_get_as<double>("semi_minor_axis"));
  else
  {
    auto sph = ellipsoid{prj.att_get_as<double>("semi_major_axis"), prj.att_get_as<double>("inverse_flattening")};
    fmt::format_to(iout, " +a={} +b={}", sph.semi_major_axis(), sph.semi_minor_axis());
  }

  // wgs84 datum shift parameters
  if (prj.att_exists("towgs84"))
  {
    std::array<double, 7> vals;
    auto size = prj.att_size("towgs84");
    if (size != 3 && size != 6 && size != 7)
      throw std::runtime_error{"unexpected number of towgs84 elements"};
    prj.att_get("towgs84", make_span(vals.data(), size));
    fmt::format_to(iout, " +towgs84={}", vals[0]);
    for (size_t i = 1; i < size; ++i)
      fmt::format_to(iout, ",{}", vals[i]);
    // netcdf allows a 6 parameter form which proj.4 does not.  fill the 7th with a zero if needed as order by CF
    if (size == 6)
      fmt::format_to(iout, ",0");
  }

  trace::debug("proj4 string for variable '{}' is: {}", var.name(), out);

  return out;
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("failed to determine proj.4 definition from variable '{}'", var.name())});
}

auto bom::io::cf::determine_wkt_definition(nc::variable const& var) -> string
try
{
  // sanity checks
  if (var.dimensions().size() < 2)
    throw std::runtime_error{"variable is not a grid"};

  // determine the grid mapping variable name (if there is one)
  auto grid_mapping = var.att_get_as<string>("grid_mapping", {});

  // if we don't have a formal grid mapping then there's no crs_wkt attribute to find
  if (grid_mapping.empty())
    return {};

  // if this is an extended format grid_mapping attribute then ignore the coordinate names
  if (auto pos = grid_mapping.find(':'); pos != string::npos)
  {
    // we can't cope with multiple grid mappings applied to a single variable (yet)
    if (grid_mapping.find(':', pos) != string::npos)
      throw std::runtime_error{"multiple grid mapping variables unsupported"};

    // just ignore the remaining part of the string - it lists the coordinates
    grid_mapping.resize(pos);
  }

  // we have a formal grid mapping
  auto projection = var.parent()->find_variable(grid_mapping);
  if (!projection)
    return {};

  // return the stored WKT, if any
  return projection->att_get_as<string>("crs_wkt", {});
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("failed to determine WKT specification from variable '{}'", var.name())});
}

auto bom::io::cf::determine_cell_edges(nc::variable const& var) -> array1d
try
{
  // sanity checks
  if (var.dimensions().size() != 1)
    throw std::runtime_error{fmt::format("unexpected variable rank ({} != 1)", var.dimensions().size())};

  // cope with degenerate case (record dimension with no entries)
  if (var.dimensions()[0]->size() == 0)
    return {};

  // allocate return array
  array1d edges{var.dimensions()[0]->size() + 1};

  // do we have an explicit bounds attribute?
  if (var.att_exists("bounds"))
  {
    // lookup the bounds variable
    auto bounds_name = var.att_get_as<string>("bounds");
    auto bv = var.parent()->find_variable(bounds_name);
    if (!bv)
      throw std::runtime_error{fmt::format("missing bounds variable '{}'", bounds_name)};

    // sanity check it
    if (   bv->dimensions().size() != 2
        || bv->dimensions()[0] != var.dimensions()[0]
        || bv->dimensions()[1]->size() != 2)
      throw std::runtime_error{fmt::format("invalid bounds variable '{}'", bounds_name)};

    // read it in
    // TODO - should we use scale_factor/add_offset from the parent variable?
    array2d bounds{{2, bv->dimensions()[0]->size()}};
    packer{}.read(*bv, bounds);

    // store our edges and sanity check that it is contiguous
    edges[0] = bounds[0][0];
    edges[1] = bounds[0][1];
    for (size_t i = 1; i < bounds.extents().y; ++i)
    {
      if (std::fabs(bounds[i - 1][1] - bounds[i][0]) > 0.001)
        throw std::runtime_error{fmt::format("bounds variable '{}' not contiguous", bounds_name)};
      edges[i + 1] = bounds[i][1];
    }
  }
  // otherwise approximate the cell edges using the centers by assuming contiguous cells with midpoint boundaries
  else
  {
    // read the cell centers
    array1d centers{var.dimensions()[0]->size()};
    packer{}.read(var, centers);

    // convert to edges
    edges[0] = centers[0] - 0.5 * (centers[1] - centers[0]);
    for (size_t i = 0; i < centers.size() - 1; ++i)
      edges[i + 1] = centers[i] + 0.5 * (centers[i + 1] - centers[i]);
    edges[centers.size()] = centers[centers.size() - 1] + 0.5 * (centers[centers.size() - 1] - centers[centers.size() - 2]);
  }

  return edges;
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("failed to determine cell bounds from coordinate variable '{}'", var.name())});
}

auto bom::io::cf::create_spatial_dimension(
      io::nc::group& cdf
    , string const& name
    , string const& standard_name
    , string const& units
    , array1d const& edges
    , int compression_level
    ) -> io::nc::dimension&
{
  // get or create the N=2 dimension needed for 1D bounds
  // done first for asthetic reasons - it ensures that nv2 does not appear between the x and y dims in ncdump
  string name_nv{"n2"};
  io::nc::dimension* nv = cdf.find_dimension(name_nv);
  while (nv && nv->size() != 2)
  {
    name_nv.append("x");
    nv = cdf.find_dimension(name_nv);
  }
  if (!nv)
    nv = &cdf.create_dimension(name_nv, 2);

  // create the dimension
  auto& dim = cdf.create_dimension(name, edges.size() - 1);

  // determine cell coordinates (i.e. cell centers)
  // allocate twice needed buffer size for sake of bounds later
  array1d buf{dim.size() * 2};
  for (size_t i = 0; i < dim.size(); ++i)
    buf[i] = (edges[i] + edges[i + 1]) * 0.5;

  // create the coordinate variable (cell centers)
  auto& coords = cdf.create_variable(
        name
      , io::nc::data_type::f64
      , {&dim}
      , {edges.size() - 1}
      , compression_level);
  coords.att_set("standard_name", standard_name);
  coords.att_set("units", units);
  coords.att_set("bounds", name + "_bounds");
  coords.write(make_span(buf.data(), dim.size()));

  // determine cell bounds (i.e. cell edges)
  for (size_t i = 0; i < dim.size(); ++i)
  {
    buf[i * 2] = edges[i];
    buf[i * 2 + 1] = edges[i + 1];
  }

  // create the bounds variable (cell edges)
  auto& bounds = cdf.create_variable(
        name + "_bounds"
      , io::nc::data_type::f64
      , {&dim, nv}
      , {edges.size() - 1, 2}
      , compression_level);
  bounds.write(buf);

  return dim;
}

auto bom::io::cf::create_grid_mapping(
      io::nc::group& cdf
    , string const& name
    , string const& proj4_definition
    , string const& wkt_definition
    , string const& units_y
    , string const& units_x
    ) -> io::nc::variable&
try
{
  // create our grid mapping variable
  auto& var = cdf.create_variable(name, io::nc::data_type::i8);

  // tokenize the parameters
  std::map<string, string, std::less<>> parameters;
  for (auto&& token : tokenize<string>(proj4_definition, " \t\r\n"))
  {
    auto pos = token.find("=");
    if (pos != string::npos)
      parameters.emplace(token.substr(0, pos), token.substr(pos + 1, string::npos));
    else
      parameters.emplace(std::move(token), string{});
  }

  // find the projection type first since the names of other attributes are context sensitive to this
  auto paramd = [&](char const* name)
  {
    auto i = parameters.find(name);
    if (i == parameters.end())
      throw std::runtime_error{fmt::format("missing mandatory parameter '{}'", name)};
    return from_string<double>(i->second);
  };
  auto params = [&](char const* name) -> string const&
  {
    auto i = parameters.find(name);
    if (i == parameters.end())
      throw std::runtime_error{fmt::format("missing mandatory parameter '{}'", name)};
    return i->second;
  };

  bool cartesian = true;
  auto& proj = parameters["+proj"];
  if (proj == "aea")
  {
    var.att_set("grid_mapping_name", "albers_conical_equal_area");
    if (parameters.find("+lat_2") != parameters.end())
    {
      std::array<double, 2> vals{paramd("+lat_1"), paramd("+lat_2")};
      var.att_set("standard_parallel", make_span(vals));
    }
    else
      var.att_set("standard_parallel", paramd("+lat_1"));
    var.att_set("longitude_of_central_meridian", paramd("+lon_0"));
    var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
  }
  else if (proj == "aeqd")
  {
    var.att_set("grid_mapping_name", "azimuthal_equidistant");
    var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
    var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
  }
  else if (proj == "geos")
  {
    var.att_set("grid_mapping_name", "geostationary");
    var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
    var.att_set("perspective_point_height", paramd("+h"));
    if (parameters.find("+sweep") != parameters.end())
      var.att_set("sweep_angle_axis", params("+sweep"));
  }
  else if (proj == "laea")
  {
    var.att_set("grid_mapping_name", "lambert_azimuthal_equal_area");
    var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
    var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
  }
  else if (proj == "lcc")
  {
    var.att_set("grid_mapping_name", "lambert_conformal_conic");
    if (parameters.find("+lat_2") != parameters.end())
    {
      std::array<double, 2> vals{paramd("+lat_1"), paramd("+lat_2")};
      var.att_set("standard_parallel", make_span(vals));
    }
    else
      var.att_set("standard_parallel", paramd("+lat_1"));
    var.att_set("longitude_of_central_meridian", paramd("+lon_0"));
    var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
  }
  else if (proj == "cea")
  {
    var.att_set("grid_mapping_name", "lambert_cylindrical_equal_area");
    var.att_set("longitude_of_central_meridian", paramd("+lon_0"));
    if (parameters.find("+lat_ts") != parameters.end())
      var.att_set("standard_parallel", paramd("+lat_ts"));
    else if (parameters.find("+k_0") != parameters.end())
      var.att_set("scale_factor_at_projection_origin", paramd("+k_0"));
    else
      throw std::runtime_error{"missing mandatory one of +lat_ts or +k_0 parameter"};
  }
  else if (proj == "latlong") // rotated lat/lon
  {
    cartesian = false;
    var.att_set("grid_mapping_name", "latitude_longitude");
  }
  else if (proj == "merc")
  {
    var.att_set("grid_mapping_name", "mercator");
    var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
    if (parameters.find("+lat_ts") != parameters.end())
      var.att_set("standard_parallel", paramd("+lat_ts"));
    else if (parameters.find("+k_0") != parameters.end())
      var.att_set("scale_factor_at_projection_origin", paramd("+k_0"));
    else
      throw std::runtime_error{"missing mandatory one of +lat_ts or +k_0 parameter"};
  }
  else if (proj == "omerc")
  {
    var.att_set("grid_mapping_name", "oblique_mercator");
    var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
    var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
    var.att_set("azimuth_of_central_line", paramd("+alpha"));
    if (parameters.find("+k_0") != parameters.end())
      var.att_set("scale_factor_at_projection_origin", paramd("+k_0"));
  }
  else if (proj == "ortho")
  {
    var.att_set("grid_mapping_name", "orthographic");
    var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
    var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
  }
  else if (proj == "sinu")
  {
    var.att_set("grid_mapping_name", "sinusoidal");
    var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
  }
  else if (proj == "stere")
  {
    auto lat = paramd("+lat_0");
    if (std::fabs(lat - 90.0) < 0.0001 || std::fabs(lat - -90.0) < 0.0001)
    {
      var.att_set("grid_mapping_name", "polar_stereographic");
      var.att_set("straight_vertical_longitude_from_pole", paramd("+lon_0"));
      var.att_set("latitude_of_projection_origin", lat);
      if (parameters.find("+lat_ts") != parameters.end())
        var.att_set("standard_parallel", paramd("+lat_ts"));
      else if (parameters.find("+k_0") != parameters.end())
        var.att_set("scale_factor_at_projection_origin", paramd("+k_0"));
      else
        throw std::runtime_error{"missing mandatory one of +lat_ts or +k_0 parameter"};
    }
    else
    {
      var.att_set("grid_mapping_name", "stereographic");
      var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
      var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
      if (parameters.find("+k_0") != parameters.end())
        var.att_set("scale_factor_at_projection_origin", paramd("+k_0"));
    }
  }
  else if (proj == "tmerc")
  {
    var.att_set("grid_mapping_name", "transverse_mercator");
    var.att_set("longitude_of_central_meridian", paramd("+lon_0"));
    var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
    if (parameters.find("+k_0") != parameters.end())
      var.att_set("scale_factor_at_projection_origin", paramd("+k_0"));
  }
  else if (proj == "nsper")
  {
    var.att_set("grid_mapping_name", "vertical_perspective");
    var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
    var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
    var.att_set("perspective_point_height", paramd("+h"));
  }
  else if (proj == "gnom")
  {
    var.att_set("grid_mapping_name", "gnomonic");
    var.att_set("latitude_of_projection_origin", paramd("+lat_0"));
    var.att_set("longitude_of_projection_origin", paramd("+lon_0"));
  }
  else if (proj == "")
    throw std::runtime_error{"missing mandatory +proj parameter"};
  else
    throw std::runtime_error{"unknown or unsupported +proj value '{}'"};

  // false easting/northing
  if (cartesian)
  {
    // +x_0 and +y_0 are always in meters, regardless of any +units parameter
    if (parameters.find("+x_0") != parameters.end())
    {
      auto value = paramd("+x_0");
      if (units_x != "m")
        value = unit::transform{unit{"m"s}, unit{units_x}}(value);
      var.att_set("false_easting", value);
    }
    else
      var.att_set("false_easting", 0.0);
    if (parameters.find("+y_0") != parameters.end())
    {
      auto value = paramd("+y_0");
      if (units_y != "m")
        value = unit::transform{unit{"m"s}, unit{units_y}}(value);
      var.att_set("false_northing", value);
    }
    else
      var.att_set("false_northing", 0.0);
  }

  // ellipsoid parameters
  if (parameters.find("+a") != parameters.end())
    var.att_set("semi_major_axis", paramd("+a"));
  if (parameters.find("+b") != parameters.end())
    var.att_set("semi_minor_axis", paramd("+b"));

  // towgs84 parameters
  if (parameters.find("+towgs84") != parameters.end())
  {
    auto vals = tokenize<double>(params("+towgs84"), ",");
    if (vals.size() != 3 && vals.size() != 6 && vals.size() != 7)
      throw std::runtime_error{"unexpected number of towgs84 elements"};
    var.att_set("towgs84", make_span(vals));
  }

  // explicit WKT definition
  if (!wkt_definition.empty())
    var.att_set("crs_wkt", wkt_definition);

  return var;
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("failed to determine CF grid mapping for proj.4 definition '{}'", proj4_definition)});
}

auto bom::io::cf::determine_optimal_packing(double min, double max, nc::data_type type, bool fill_value) -> pair<double, double>
{
  pair<double, double> ret;

  auto calc = [&](int bits, bool sign)
  {
    if (fill_value)
    {
      ret.first = (max - min) / ((2ull << (bits -1)) - 2);
      ret.second = sign ? (min + max) / 2 : min - ret.first;
    }
    else
    {
      ret.first = (max - min) / ((2ull << (bits - 1)) - 1);
      ret.second = sign ? min + (2ull << (bits - 2)) * ret.first : min;
    }
  };

  switch (type)
  {
  case nc::data_type::i8:
    calc(8, true);
    break;
  case nc::data_type::u8:
    calc(8, false);
    break;
  case nc::data_type::i16:
    calc(16, true);
    break;
  case nc::data_type::u16:
    calc(16, false);
    break;
  case nc::data_type::i32:
    calc(32, true);
    break;
  case nc::data_type::u32:
    calc(32, false);
    break;
  case nc::data_type::i64:
    calc(64, true);
    break;
  case nc::data_type::u64:
    calc(64, false);
    break;
  case nc::data_type::f32:
    ret.first = 1.0;
    ret.second = 0.0;
    break;
  case nc::data_type::f64:
    ret.first = 1.0;
    ret.second = 0.0;
    break;
  case nc::data_type::string:
    throw std::logic_error{"cannot pack string types"};
  };

  return ret;
}

// LCOV_EXCL_START
TEST_CASE("io::cf""determine_optimal_packing")
{
  pair<double, double> so;

  // test the cases where no packing should be needed
  so = determine_optimal_packing(-128, 127, nc::data_type::i8, false);
  CHECK(so.first == approx(1.0));
  CHECK(so.second == approx(0.0));
  so = determine_optimal_packing(-127, 127, nc::data_type::i8, true);
  CHECK(so.first == approx(1.0));
  CHECK(so.second == approx(0.0));

  so = determine_optimal_packing(0, 255, nc::data_type::u8, false);
  CHECK(so.first == approx(1.0));
  CHECK(so.second == approx(0.0));
  so = determine_optimal_packing(1, 255, nc::data_type::u8, true);
  CHECK(so.first == approx(1.0));
  CHECK(so.second == approx(0.0));

  so = determine_optimal_packing(-100, 100, nc::data_type::f32, false);
  CHECK(so.first == approx(1.0));
  CHECK(so.second == approx(0.0));
  so = determine_optimal_packing(-100, 100, nc::data_type::f64, true);
  CHECK(so.first == approx(1.0));
  CHECK(so.second == approx(0.0));

  // a few other cases
  so = determine_optimal_packing(0, 254, nc::data_type::u8, true);
  CHECK(so.first == approx(1.0));
  CHECK(so.second == approx(-1.0));

  so = determine_optimal_packing(0, 127, nc::data_type::u8, true);
  CHECK(so.first == approx(0.5));
  CHECK(so.second == approx(-0.5));
}
TEST_CASE("io::cf::packbuf - no fill value")
{
  packer pk;
  array2d ddata{{2, 2}};

  nc::file file{"packtest1.nc", io_mode::create};
  auto& dy = file.create_dimension("y", ddata.extents().y);
  auto& dx = file.create_dimension("x", ddata.extents().x);
  auto& var = file.create_variable("var", nc::data_type::i32, {&dy, &dx});
  var.att_set("scale_factor", 5.0);
  var.att_set("add_offset", 10.0);

  // pack and write the data
  ddata[0][0] = 10.0;
  ddata[0][1] = 110.0;
  ddata[1][0] = 210.0;
  ddata[1][1] = 310.0;
  pk.write(var, ddata);

  // check the packed values
  array2i idata{ddata.extents()};
  idata.fill(-1);
  var.read(idata);
  CHECK(idata[0][0] == 0);
  CHECK(idata[0][1] == 20);
  CHECK(idata[1][0] == 40);
  CHECK(idata[1][1] == 60);

  // check the unpacked result
  array2d rdata{ddata.extents()};
  rdata.fill(-1.0);
  pk.read(var, rdata);
  CHECK(rdata[0][0] == approx(10.0));
  CHECK(rdata[0][1] == approx(110.0));
  CHECK(rdata[1][0] == approx(210.0));
  CHECK(rdata[1][1] == approx(310.0));
}

TEST_CASE("io::cf::(un)pack - fill value")
{
  packer pk;
  array2d ddata{{2, 2}};

  nc::file file{"packtest2.nc", io_mode::create};
  auto& dy = file.create_dimension("y", ddata.extents().y);
  auto& dx = file.create_dimension("x", ddata.extents().x);
  auto& var = file.create_variable("var", nc::data_type::i32, {&dy, &dx});
  var.att_set("scale_factor", 5.0);
  var.att_set("add_offset", 10.0);
  var.att_set("_FillValue", -999);

  // pack and write the data
  ddata[0][0] = nan<double>();
  ddata[0][1] = 110.0;
  ddata[1][0] = 210.0;
  ddata[1][1] = 310.0;
  pk.write(var, ddata);

  // check the packed values
  array2i idata{ddata.extents()};
  idata.fill(-1);
  var.read(idata);
  CHECK(idata[0][0] == -999);
  CHECK(idata[0][1] == 20);
  CHECK(idata[1][0] == 40);
  CHECK(idata[1][1] == 60);

  // check the unpacked result
  array2d rdata{ddata.extents()};
  rdata.fill(-1.0);
  pk.read(var, rdata);
  CHECK(is_nan(rdata[0][0]));
  CHECK(rdata[0][1] == approx(110.0));
  CHECK(rdata[1][0] == approx(210.0));
  CHECK(rdata[1][1] == approx(310.0));
}
// LCOV_EXCL_STOP

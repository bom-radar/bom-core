/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <bom/array1.h>
#include <bom/array2.h>
#include <bom/file_utils.h>
#include <bom/raii.h>
#include <bom/string_utils.h>
#include <bom/timestamp.h>

/// Rapic I/O classes
namespace bom::io::rapic
{
  /// Available message types
  enum class message_type
  {
      comment   ///< Comment line starting with '/' (used for IMAGE headers in volume files)
    , mssg      ///< Administration message (MSSG)
    , status    ///< Status message (RDRSTAT)
    , permcon   ///< Semipermanent connection message (RPQUERY: SEMIPERMANENT CONNECTION)
    , query     ///< Data request message (RPQUERY)
    , filter    ///< Filter specification message (RPFILTER)
    , scan      ///< Rapic scan message
  };

  /// Possible scan types for queries and filters
  /* note: the numbers here are chosen to match Rowlf (since Rowlf sends numeric values instead of strings) */
  enum class scan_type
  {
      any = -1
    , ppi = 0
    , rhi = 1
    , comp_ppi = 2
    , image = 3
    , volume = 4
    , rhi_set = 5
    , merge = 6
    , scan_error = 7
  };

  /// Possible query types, largely unused
  enum class query_type
  {
      latest
    , to_time
    , from_time
    , center_time
  };

  /// Parse a station id string ('ANY' will output 0)
  auto parse_station_id(char const* in) -> int;
  /// Parse a scan type including product number (e.g. VOLUME1)
  auto parse_scan_type(char const* in) -> pair<scan_type, int>;
  /// Parse a query type string
  auto parse_query_type(char const* in) -> query_type;
  /// Parse a list of moments string
  auto parse_data_types(char const* in) -> vector<string>;

  /// Parse the VOLUMETRIC header to extract the product timestamp
  auto parse_volumetric_header(string const& product) -> timestamp;
  /// Parse the TIMESTAMP header to extract the timestamp
  auto parse_timestamp_header(char const* str) -> timestamp;

  /// Error thrown upon failure to decode the rapic message stream
  class decode_error : public std::runtime_error
  {
  public:
    decode_error(char const* desc, uint8_t const* in, size_t size);
    decode_error(message_type type, uint8_t const* in, size_t size);
  };

  /// Buffer for raw message data
  class buffer
  {
  public:
    /// Construct a buffer of the given size
    buffer(size_t size, size_t max_size = std::numeric_limits<size_t>::max());

    /// Copy constructor
    buffer(buffer const& rhs);

    /// Move constructor
    buffer(buffer&& rhs) noexcept = default;

    /// Copy assignment operator
    auto operator=(buffer const& rhs) -> buffer&;

    /// Move assignment operator
    auto operator=(buffer&& rhs) noexcept -> buffer& = default;

    /// Get the total size of the buffer
    auto size() const -> size_t;

    /// Clear any unread contents in the buffer
    auto clear() -> void;

    /// Change the buffer capacity
    /** Unlike array1, this call will preserve the contents of the buffer to ensure that no data corruption occurs.
     *  An exception will be thrown if the buffer currently contains more unread data than the desired new size. */
    auto resize(size_t size) -> void;

    /// Shift unread data to the front of the buffer
    auto optimize() -> void;

    /// Get a pointer to the write position of the buffer and the amount of contiguous space available for writing
    auto write_acquire(size_t min_space) -> pair<uint8_t*, size_t>;

    /// Advance the write position after having written len bytes into the pointer returned by write_acquire()
    auto write_advance(size_t len) -> void;

    /// Get a pointer to the read position of the buffer and the amount of contiguous space available for reading
    /** This function is used to allow direct reading from the buffer.  This can be useful in applications where
     *  there is no requirement to actually decode the rapic data (such as data logging). */
    auto read_acquire() const -> pair<uint8_t const*, size_t>;

    /// Advance the read position by len bytes
    auto read_advance(size_t len) -> void;

    /// Determine whether there is a complete message that can be read from the buffer, its type and length
    /** This function should be used to detect a message ready for decoding in a buffer.  If a message is ready for
     *  reading then the function will return true.  At this point the user may choose to decode the message using
     *  the decode() function of the appropriate concrete messag type.  The user must call read_advance() on the
     *  buffer passing in the length which is output from this function to advance past the current message to the
     *  next message in the buffer.  If read_advance() is not called then read_detect() and decode() will repeatedly
     *  detect and decode the same message. */
    auto read_detect(message_type& type, size_t& len) const -> bool;

  private:
    array1<uint8_t> data_;
    size_t          wpos_;
    size_t          rpos_;
    size_t          max_size_;
  };

  /// Abstract base class for message types
  class message
  {
  public:
    virtual ~message();

    /// Get the type of this message
    virtual auto type() const -> message_type = 0;

    /// Reset the message to a default state
    virtual auto reset() -> void = 0;

    /// Encode the message into the wire format
    virtual auto encode(buffer& out) const -> void = 0;

    /// Decode the message from the wire format
    /** It is the user's responsibility to ensure that the concrete type of the message object matches the encoded
     *  message currently at the front of the buffer.  This would normally be ensured by first calling the
     *  read_detect() function of the input buffer. */
    virtual auto decode(buffer const& in) -> void = 0;
  };

  /// Comment message
  /** This message type is generally only found in rapic files where multiple rapic scans have been concatenated
   *  into a single volume file.  Comments start with a forward slash '/' and are used to implement meta-headers
   *  such as IMAGE, RXTIME, IMAGESCANS, IMAGESIZE, IMAGEHEADER etc.  These headers provide information to
   *  3D-Rapic which allow it to index directly into the file without parsing every scan.  Messages of this type
   *  are never sent by Bureau radar transmitters over the wire. */
  class comment : public message
  {
  public:
    /// Construct an empty comment message
    comment();
    /// Construct a comment message by decoding it from a buffer
    comment(buffer const& in);

    auto type() const -> message_type override;
    auto reset() -> void override;
    auto encode(buffer& out) const -> void override;
    auto decode(buffer const& in) -> void override;

    /// Get the message string
    auto text() const -> string const&                  { return text_; }
    /// Set the message string
    auto set_text(string const& val) -> void            { text_ = val; }

  private:
    string text_;
  };

  /// MSSG status message
  class mssg : public message
  {
  public:
    /// Construct an empty status message
    mssg();
    /// Construct a status message by decoding it from a buffer
    mssg(buffer const& in);

    auto type() const -> message_type override;
    auto reset() -> void override;
    auto encode(buffer& out) const -> void override;
    auto decode(buffer const& in) -> void override;

    /// Get the message number
    auto number() const -> int                          { return number_; }
    /// Set the message number
    auto set_number(int val) -> void                    { number_ = val; }

    /// Get the message string
    auto text() const -> string const&                  { return text_; }
    /// Set the message string
    auto set_text(string const& val) -> void            { text_ = val; }

  private:
    int    number_;
    string text_;
  };

  /// RDRSTAT status message
  /** This message is used as a keepalive for rapic protocol connections.  It contains no actual useful data. */
  class status : public message
  {
  public:
    /// Construct an empty status message
    status();
    /// Construct a status message by decoding it from a buffer
    status(buffer const& in);

    auto type() const -> message_type override;
    auto reset() -> void override;
    auto encode(buffer& out) const -> void override;
    auto decode(buffer const& in) -> void override;

    /// Get the message string
    auto text() const -> string const&                  { return text_; }
    /// Set the message string
    auto set_text(string const& val) -> void            { text_ = val; }

  private:
    string text_;
  };

  /// Semi-permanent connection message
  class permcon : public message
  {
  public:
    /// Construct an empty permanent connection message
    permcon();
    /// Construct a permanent connection message by decoding it from a buffer
    permcon(buffer const& in);

    auto type() const -> message_type override;
    auto reset() -> void override;
    auto encode(buffer& out) const -> void override;
    auto decode(buffer const& in) -> void override;

    /// Get whether txcompletescans is set
    auto tx_complete_scans() const -> bool              { return tx_complete_scans_; }
    /// Set whether txcompletescans is set
    auto set_tx_complete_scans(bool val) -> void        { tx_complete_scans_ = val; }

  private:
    bool tx_complete_scans_;
  };

  /// RPQUERY message
  class query : public message
  {
  public:
    /// Construct an empty query
    query();
    /// Construct a query by decoding it from a buffer
    query(buffer const& in);

    auto type() const -> message_type override;
    auto reset() -> void override;
    auto encode(buffer& out) const -> void override;
    auto decode(buffer const& in) -> void override;

    /// Get the station identifier (0 = any)
    auto station_id() const -> int                      { return station_id_; }

    /// Get the scan type
    auto scan_type() const -> rapic::scan_type          { return scan_type_; }

    /// Get the volume id (-1 = any or not volume)
    auto volume_id() const -> int                       { return volume_id_; }

    /// Get the selected angle (-1 = default)
    auto angle() const -> float                         { return angle_; }

    /// Get the repeat count (-1 = default)
    auto repeat_count() const -> int                    { return repeat_count_; }

    /// Get the query type (latest by default)
    auto query_type() const -> rapic::query_type        { return query_type_; }

    /// Get the image time (0 = latest image)
    auto time() const -> time_t                         { return time_; }

    /// Get the data types
    auto data_types() const -> vector<string> const&    { return data_types_; }

    /// Get the video resolution
    auto video_resolution() const -> int                { return video_res_; }

  private:
    int               station_id_;
    rapic::scan_type  scan_type_;
    int               volume_id_;
    float             angle_;
    int               repeat_count_;
    rapic::query_type query_type_;
    time_t            time_;
    vector<string>    data_types_;
    int               video_res_;
  };

  /// RPFILTER message
  class filter : public message
  {
  public:
    /// Construct an empty filter
    filter();
    /// Construct a filter by decoding it from a buffer
    filter(buffer const& in);

    auto type() const -> message_type override;
    auto reset() -> void override;
    auto encode(buffer& out) const -> void override;
    auto decode(buffer const& in) -> void override;

    /// Get the station identifier (0 = any)
    auto station_id() const -> int                      { return station_id_; }
    /// Set the station identifier (0 = any)
    auto set_station_id(int val) -> void                { station_id_ = val; }

    /// Get the scan type
    auto scan_type() const -> rapic::scan_type          { return scan_type_; }
    /// Set the scan type
    auto set_scan_type(rapic::scan_type val) -> void    { scan_type_ = val; }

    /// Get the volume id (-1 = any or not volume)
    auto volume_id() const -> int                       { return volume_id_; }
    /// Set the volume id (-1 = any or not volume)
    auto set_volume_id(int val) -> void                 { volume_id_ = val; }

    /// Get the video resolution
    auto video_resolution() const -> int                { return video_res_; }
    /// Set the video resolution
    auto set_video_resolution(int val) -> void          { video_res_ = val; }

    /// Get the source identifier ("-1" = default)
    auto source() const -> string const&                { return source_; }
    /// Set the source identifier ("-1" = default)
    auto set_source(string const& val) -> void          { source_ = val; }

    /// Get the data types
    auto data_types() const -> vector<string> const&    { return data_types_; }
    /// Get the data types
    auto data_types() -> vector<string>&                { return data_types_; }
    /// Set the data types
    auto set_data_types(vector<string> val) -> void     { data_types_ = std::move(val); }

  private:
    int               station_id_;
    rapic::scan_type  scan_type_;
    int               volume_id_;
    int               video_res_;
    string            source_;
    vector<string>    data_types_;
  };

  /// Radar product message
  class scan : public message
  {
  public:
    /// Header used by a scan message
    class header
    {
    public:
      header(string name)
        : name_(std::move(name))
      { }
      header(string name, string value)
        : name_(std::move(name)), value_(std::move(value))
      { }

      /// Get the name of the header
      auto name() const -> string const&                { return name_; }
      /// Set the name of the header
      auto set_name(string const& name) -> void         { name_ = name; }

      /// Get the header value
      auto value() const -> string const&               { return value_; }
      /// Get the header value
      auto set_value(string const& value) -> void       { value_ = value; }

      /// Get the header value as a bool
      auto get_boolean() const -> bool;
      /// Set the header value from a bool
      auto set_boolean(bool val) -> void;

      /// Get the header value as a long
      auto get_integer() const -> long;
      /// Set the header value from a long
      auto set_integer(long val) -> void;

      /// Get the header value as a double
      auto get_real() const -> double;
      /// Set the header value from a double
      auto set_real(double val) -> void;

      /// Get the header value as a array of longs
      auto get_integer_array() const -> vector<long>;
      /// Set the header value from an aray of longs
      auto set_integer_array(vector<long> const& val) -> void;

      /// Get the header value as a array of doubles
      auto get_real_array() const -> vector<double>;
      /// Set the header value from an array of doubles
      auto set_real_array(vector<double> const& val) -> void;

    private:
      string name_;
      string value_;
    };

    /// Information about a single ray
    class ray_header
    {
    public:
      ray_header()
        : azimuth_{std::numeric_limits<float>::quiet_NaN()}
        , elevation_{std::numeric_limits<float>::quiet_NaN()}
        , time_offset_{-1}
      { }

      ray_header(float azimuth)
        : azimuth_{azimuth}, elevation_{std::numeric_limits<float>::quiet_NaN()}, time_offset_{-1}
      { }

      ray_header(float azimuth, float elevation, int time_offset)
        : azimuth_{azimuth}, elevation_{elevation}, time_offset_{time_offset}
      { }

      /// Get the azimuth at the center of this ray (degrees)
      auto azimuth() const -> float                     { return azimuth_; }

      /// Get the elevation at the center of this ray (degrees)
      auto elevation() const -> float                   { return elevation_; }

      /// Get the time offset from the start of scan to this ray (seconds)
      auto time_offset() const -> int                   { return time_offset_; }

    private:
      float azimuth_;
      float elevation_;
      int   time_offset_;
    };

  public:
    /// Construct an empty scan
    scan();
    /// Construct a scan by decoding it from a buffer
    scan(buffer const& in);
    /// Construct a scan from a set of headers, ray headers and level data
    scan(vector<header> headers, vector<ray_header> ray_headers, array2<uint8_t> level_data);

    auto type() const -> message_type override;
    auto reset() -> void override;
    auto encode(buffer& out) const -> void override;
    auto decode(buffer const& in) -> void override;

    /// Get the station identifier
    auto station_id() const -> int                      { return station_id_; }
    /// Set the station identifier
    //auto set_station_id(int val) -> void;

    /// Get the volume identifier
    /** If there is no volume identifier this function returns -1 */
    auto volume_id() const -> int                       { return volume_id_; }
    /// Set the volume identifier
    //auto set_volume_id(int val) -> void;

    /// Get the product string
    /** This value is normally unique to each complete product which is built from multiple scan messages.  For
     *  example, a volume product contains many passes which each share this string. */
    auto product() const -> string const&               { return product_; }
    /// Set the product string
    /** It is the user's responsibility to ensure this string is a valid rapic PRODUCT header. */
    //auto set_product(string const& val) -> void;

    /// Get the pass number
    /** If the pass number is unavailable this function returns -1 */
    auto pass() const -> int                            { return pass_; }
    /// Get the number of passes in the containing product
    /** If the pass count is unavailable this function returns -1 */
    auto pass_count() const -> int                      { return pass_count_; }
    /// Set the pass number and count
    //auto set_pass(int pass, int count) -> void;

    /// Get the minimum angle for the scan
    /** This value is normally 0 (for a complete sweep), ANGLE1 from the product header (for ascending RHIs or
     *  CW sector sweeps) or ANGLE2 from the product header (for descending RHIs or CCW sector sweeps). */
    auto angle_min() const -> float                     { return angle_min_; }
    // no set_angle_min() - derived from the product header

    /// Get the maximum angle for the scan
    /** This value is normally 360 (for a complete sweep), ANGLE2 from the product header (for ascending RHIs or
     *  CW sector sweeps) or ANGLE1 from the product header (for descending RHIs or CCW sector sweeps). */
    auto angle_max() const -> float                     { return angle_max_; }
    // no set_angle_max() - derived from the product header

    /// Get the angular resolution for the scan
    /** This value represents the angular sweep width of a single ray. */
    auto angle_resolution() const -> float              { return angle_resolution_; }
    /// Set the angular resolution for the scan
    //auto set_angle_resolution(float val) -> void;

    /// Access all the scan headers
    /** Note that all rapic headers are available via the returned container including those which are exposed
     *  explicitly via other functions. */
    auto headers() const -> vector<header> const&               { return headers_; }

    /// Find a specific header
    /** Returns nullptr if the header is not present. */
    auto find_header(string const& name) const -> header const* { return find_header(name.c_str()); }
    auto find_header(char const* name) const -> header const*;

    /// Find a specific header or insert it if it does not yet exist
    auto find_or_insert_header(string const& name) -> header&   { return find_or_insert_header(name.c_str()); }
    auto find_or_insert_header(char const* name) -> header&;

    /// Access the information about each ray
    /** The number of ray headers may be less than the number of rays in the level data array.  This simply means
     *  that some rays for the scan were not sent and should be treated as missing data. */
    auto ray_headers() const -> vector<ray_header> const&       { return ray_headers_; }

    /// Get the number of rays (ie: rows) in the level data array
    /** This is the number of rays that are conceptually in the scan.  The number of rays actually stored and
     *  available may be smaller than this.  The number of rays available is indicated by the number of ray
     *  headers. */
    auto rays() const -> size_t                                 { return level_data_.extents().y; }

    /// Get the number of bins (ie: columns) in the level data array
    auto bins() const -> size_t                                 { return level_data_.extents().x; }

    /// Access the scan data encoded as levels
    /** The size of this array is rays() * bins(), however only ray_headers().size() * bins() of data is actually
     *  valid. */
    auto level_data() const -> array2<uint8_t> const&           { return level_data_; }

  private:
    auto get_header_string(char const* name) const -> string const&;
    auto get_header_integer(char const* name) const -> long;
    auto get_header_real(char const* name) const -> double;
    auto initialize_rays() -> void;

  private:
    vector<header>     headers_;     // scan headers
    vector<ray_header> ray_headers_; // ray headers
    array2<uint8_t>    level_data_;  // level encoded scan data

    // these are cached from the headers structure due to likelyhood of frequent access
    int     station_id_;
    int     volume_id_;
    string  product_;
    int     pass_;
    int     pass_count_;
    bool    is_rhi_;
    float   angle_min_;
    float   angle_max_;
    float   angle_resolution_;
  };

  /// Possible states for a rapic connection
  enum class connection_state
  {
      disconnected    ///< Not connected
    , in_progress     ///< Socket is active but connection is still being established
    , established     ///< Connection with server is established
  };

  /// Rapic socket connection helper
  /*
   * Basic usage of this class is shown below:
   *
   *    // create a connection and connect to server
   *    connection con{"myhost", "15555"};
   *    con.connect();
   *
   *    // send the permcon message to signal persistance to the server
   *    con.enqueue(permcon{});
   *
   *    // ask for some radar data
   *    filter flt;
   *    flt.set_station_id(2);
   *    flt.set_scan_type(scan_type::any);
   *    flt.set_volume_id(0);
   *    con.enqueue(flt);
   *
   *    // wait for data to arrive
   *    while (con.connection_state() != connection_state::disconnected)
   *    {
   *      con.poll();
   *
   *      // process messages from server
   *      bool again = true;
   *      while (again) {
   *        again = con.process_traffic();
   *
   *        // dequeue each message
   *        while (auto msg = con.dequeue())
   *        {
   *          // decode and handle interesting messages
   *          if (msg->type() == message_type::scan)
   *          {
   *            scan& s = *static_cast<scan*>(msg.get());
   *            ...
   *          }
   *        }
   *      }
   *    }
   *
   * For asynchronous usage, the user should use the pollable_fd(), poll_read() and poll_write() functions to
   * setup the appropriate multiplexed polling function for their application.
   */
  class connection
  {
  public:
    /// Construct a new connection
    /** The connection will throw an exception rather that allowing either the read or write buffer to grow
     *  beyond the given size in bytes. */
    connection(
          string address
        , string service
        , duration keepalive_period = 40s
        , duration inactivity_timeout = 120s
        , size_t max_buffer_size = std::numeric_limits<size_t>::max());

    /// Establish the connection to the remote host
    auto connect() -> void;

    /// Disconnect from the remote host
    auto disconnect() -> void;

    /// Get the hostname or address of the remote server
    auto& address() const           { return address_; }

    /// Get the service or port name for the connection
    auto& service() const           { return service_; }

    /// Get the keepalive period for the connection
    auto keepalive_period() const   { return keepalive_period_; }

    /// Get the inactivity timeout for the connection
    auto inactivity_timeout() const { return inactivity_timeout_; }

    /// Return the current state of the socket connection to the server
    auto state() const              { return state_; }

    /// Get the time of the last connection attempt
    auto last_connect() const       { return last_connect_; }

    /// Get the time of the last keepalive message being sent
    auto last_keepalive() const     { return last_keepalive_; }

    /// Get the time of the last data reception
    auto last_activity() const      { return last_activity_; }

    /// Get the file descriptor of the socket which may be used for multiplexed polling
    /** This function along with poll_read and poll_write are useful in an asynchronous I/O environment and you
     *  would like to block on multiple I/O sources.  The file descriptor returned by this function may be passed
     *  to pselect or a similar function.  The poll_read and poll_write functions return true if you should wait
     *  for read and write availability respectively. */
    auto& pollable_fd() const       { return socket_; }

    /// Get whether the socket file descriptor should be monitored for read availability
    auto poll_read() const -> bool;

    /// Get whether the socket file descriptor should be montored for write availability
    auto poll_write() const -> bool;

    /// Wait (block) on the socket until some traffic arrives for processing
    /** The optional timeout parameter may be supplied to force the function to return after a certain time.
     *  The default is 10 seconds.  The timeout works on whole second values.  Timeouts with partial seconds
     *  will be truncated to the lower whole second value. */
    auto poll(duration timeout = 10000s) const -> void;

    /// Process traffic on the socket (may cause new messages to be available for dequeue)
    /** This function will read from the server connection and queue any available messages in a buffer.  The
     *  messages may subsequently be retrieved by calling deqeue repeatedly until it returns a nullptr.
     *
     *  If this function returns false then there is no more data currently available on the socket.  This
     *  behaviour can be used in an asynchronous I/O environment when deciding whether to continue processing
     *  traffic on this socket, or allow entry to a multiplexed wait (such as pselect). */
    auto process_traffic() -> bool;

    /// Enqueue a message to send
    auto enqueue(message const& msg) -> void;

    /// Dequeue the next received message
    auto dequeue(bool skip_broken = true) -> unique_ptr<message>;

    /// Directly enqueue a pre-encoded buffer in rapic format
    auto enqueue_direct(buffer const& buf) -> void;

    /// Directly enqueue a pre-encoded string in rapic format
    auto enqueue_direct(string const& buf) -> void;

    /// Directly access the write queue buffer
    /** This is used for advanced purposes such as serializing the connection state and releasing the socket so
     *  that it can be later be deserialized and resumed via the resume_connection() function. */
    auto& write_buffer() { return wbuffer_; }

    /// Directly access the read queue buffer
    /** This is used for advanced purposes such as serializing the connection state and releasing the socket so
     *  that it can be later be deserialized and resumed via the resume_connection() function. */
    auto& read_buffer() { return rbuffer_; }

    /// Release ownership of the socket to the caller without closing it
    /** This is used for advanced purposes such as serializing the connection state and releasing the socket so
     *  that it can be later be deserialized and resumed via the resume_connection() function. */
    auto release_connection() -> socket_desc_hnd;

    /// Resume a connection by directly passing an existing open socket
    /** This is used for advanced purposes such as serializing the connection state and releasing the socket so
     *  that it can be later be deserialized and resumed via the resume_connection() function. */
    auto resume_connection(
          socket_desc_hnd socket
        , connection_state state
        , timestamp last_connect
        , timestamp last_keepalive
        , timestamp last_activity
        ) -> void;

  private:
    string            address_;             // remote hostname or address
    string            service_;             // remote service or port number
    duration          keepalive_period_;    // time between sending keepalives
    duration          inactivity_timeout_;  // drop connection after this long without incoming data

    socket_desc_hnd   socket_;              // socket handle
    connection_state  state_;               // current connection state
    timestamp         last_connect_;        // time of last connection attempt
    timestamp         last_keepalive_;      // time of last keepalive send
    timestamp         last_activity_;       // time of last data received

    buffer            wbuffer_;             // buffer of encoded data waiting to be sent
    buffer            rbuffer_;             // buffer of received data waiting to be decoded
  };
}

namespace bom
{
  BOM_DECLARE_ENUM_TRAITS(io::rapic::message_type, comment, mssg, status, permcon, query, filter, scan);
  BOM_DECLARE_ENUM_TRAITS(io::rapic::scan_type, any, ppi, rhi, comp_ppi, image, volume, rhi_set, merge, scan_error);
  BOM_DECLARE_ENUM_TRAITS(io::rapic::connection_state, disconnected, in_progress, established);
}

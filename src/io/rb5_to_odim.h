/*---------------------------------------------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2023 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "rb5.h"
#include <bom/io/configuration.h>
#include <bom/span.h>
#include <filesystem>

namespace bom::io
{
  /* The only_sweep and only_type values allow the user to restrict the output to a single sweep and/or a
   * single data type.  This is useful for "live" front-ends that want to output data progressively rather
   * than waiting for a complete volume. */
  auto rb5_to_odim(
        std::filesystem::path const& path
      , span<rb5::file const*> scan_set             // may be in any order
      , span<pair<string, string> const> data_types // .first = rb5 name, .second = odim quantity
      , io::configuration const& metadata_static    // used to supply values like 'source'
      , int odim_compression                        // suggest passing odim::data::default_compression
      , int only_sweep = -1
      , int only_type = -1
      ) -> void;
}

/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <bom/image.h>
#include <bom/string_utils.h>

namespace bom::io
{
  /// Read a PNG image from file
  auto png_read(string const& path) -> image;

  /// Write a PNG image to file
  auto png_write(string const& path, image const& img, vector<pair<string, string>> metadata = {}) -> void;

  /// Write a PNG image to an in memory buffer
  auto png_write(vector<char>& buf, image const& img, vector<pair<string, string>> metadata = {}) -> void;
}

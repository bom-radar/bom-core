/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "nc.h"

#include <bom/math_utils.h>
#include <bom/span.h>
#include <bom/timestamp.h>
#include <bom/vec2.h>

namespace bom::io::cf
{
  /// Default functor used to check whether a value is 'nodata' and should be packed using the fill_value
  template <typename T>
  struct default_is_nodata
  {
    auto operator()(T val) const { return is_nan(val); }
  };

  /* The read() functions return a pair of optional<T> which represent the valid_min and valid_max (or valid_range) values
   * of the variable, as they would appear in the unpacked type (i.e. with any offset and scale applied).  This allows users
   * to easily detect values outside the valid_range but which are different from the _FillValue.  This can be useful for
   * variables where it is desireable to flag multiple types of missing data since only one can be _FillValue. */
  class packer
  {
  public:
    packer()
      : size_{0}
    { }

#define BOM_IO_CF_PACKER_FUNCS(T, ND) \
    void read(nc::variable const& var, span<T> data, T nodata = ND, span<size_t const> start = {}, span<size_t const> count = {}) \
    { \
      read_impl(var, data, nodata, start, count); \
    } \
    void read(nc::variable const& var, span<T> data, T nodata, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) \
    { \
      return read_impl(var, data, nodata, {start.begin(), start.end()}, {count.begin(), count.end()}); \
    } \
    template <class IsNoData = default_is_nodata<T>> \
    void write(nc::variable& var, span<T const> data, span<size_t const> start = {}, span<size_t const> count = {}) \
    { \
      write_impl(var, data, start, count); \
    } \
    template <class IsNoData = default_is_nodata<T>> \
    void write(nc::variable& var, span<T const> data, std::initializer_list<size_t> start, std::initializer_list<size_t> count = {}) \
    { \
      write_impl(var, data, {start.begin(), start.end()}, {count.begin(), count.end()}); \
    }

    BOM_IO_CF_PACKER_FUNCS(float, nan<float>())
    BOM_IO_CF_PACKER_FUNCS(double, nan<double>())
    BOM_IO_CF_PACKER_FUNCS(int, 0)
    BOM_IO_CF_PACKER_FUNCS(short, 0)
    BOM_IO_CF_PACKER_FUNCS(long, 0)
    BOM_IO_CF_PACKER_FUNCS(long long, 0)

#undef BOM_IO_CF_PACKER_FUNCS

  private:
    template <typename T>
    void read_impl(nc::variable const& var, span<T> data, T nodata, span<size_t const> start, span<size_t const> count)
    {
      auto scale = var.att_get_as<T>("scale_factor", 1);
      auto offset = var.att_get_as<T>("add_offset", 0);

      auto impl = [&](auto dummy)
      {
        using ptype = decltype(dummy);
        auto fill = var.att_get_as<optional<ptype>>("_FillValue");

        // ensure buffer is large enough
        auto bufsize = sizeof(ptype) * data.size();
        if (size_ < bufsize)
        {
          buffer_ = make_unique<char[]>(bufsize);
          size_ = bufsize;
        }
        auto buf = reinterpret_cast<ptype*>(buffer_.get());

        // read the data into our raw buffer
        var.read(make_span(buf, data.size()), start, count);

        // what combination of missing data conditions do we have?
        if (fill)
        {
          for (decltype(data.size()) i = 0; i < data.size(); ++i)
            data[i] = buf[i] == *fill ? nodata : buf[i] * scale + offset;
        }
        else
        {
          for (decltype(data.size()) i = 0; i < data.size(); ++i)
            data[i] = buf[i] * scale + offset;
        }
      };

      switch (var.type())
      {
      case nc::data_type::i8:  impl(int8_t(0)); break;
      case nc::data_type::u8:  impl(uint8_t(0)); break;
      case nc::data_type::i16: impl(int16_t(0)); break;
      case nc::data_type::u16: impl(uint16_t(0)); break;
      case nc::data_type::i32: impl(int32_t(0)); break;
      case nc::data_type::u32: impl(uint32_t(0)); break;
      case nc::data_type::i64: impl(int64_t(0)); break;
      case nc::data_type::u64: impl(uint64_t(0)); break;
      case nc::data_type::f32: impl(float(0)); break;
      case nc::data_type::f64: impl(double(0)); break;
      case nc::data_type::string:
        throw std::runtime_error{fmt::format("Packing used with string variable {}", var.name())};
      }
    }

    template <typename T, class IsNoData = default_is_nodata<T>>
    void write_impl(nc::variable& var, span<T const> data, span<size_t const> start, span<size_t const> count)
    {
      auto is_nodata = IsNoData{};
      auto scale = var.att_get_as<T>("scale_factor", 1);
      auto offset = var.att_get_as<T>("add_offset", 0);

      auto impl = [&](auto dummy)
      {
        using ptype = decltype(dummy);
        auto fill = var.att_get_as<optional<ptype>>("_FillValue");

        // ensure buffer is large enough
        auto bufsize = sizeof(ptype) * data.size();
        if (size_ < bufsize)
        {
          buffer_ = make_unique<char[]>(bufsize);
          size_ = bufsize;
        }
        auto buf = reinterpret_cast<ptype*>(buffer_.get());

        // pack based on scale, offset and fill
        // if packing into integral types we must round to nearest to ensure maximum accuracy on unpack
        if (fill)
        {
          if (std::is_integral<ptype>::value)
            for (decltype(data.size()) i = 0; i < data.size(); ++i)
              buf[i] = is_nodata(data[i]) ? *fill : std::lround((data[i] - offset) / scale);
          else
            for (decltype(data.size()) i = 0; i < data.size(); ++i)
              buf[i] = is_nodata(data[i]) ? *fill : (data[i] - offset) / scale;
        }
        else
        {
          if (std::is_integral<ptype>::value)
            for (decltype(data.size()) i = 0; i < data.size(); ++i)
              buf[i] = std::lround((data[i] - offset) / scale);
          else
            for (decltype(data.size()) i = 0; i < data.size(); ++i)
              buf[i] = (data[i] - offset) / scale;
        }

        // write the data into our raw buffer
        var.write(make_span(buf, data.size()), start, count);
      };

      switch (var.type())
      {
      case nc::data_type::i8:  impl(int8_t(0)); break;
      case nc::data_type::u8:  impl(uint8_t(0)); break;
      case nc::data_type::i16: impl(int16_t(0)); break;
      case nc::data_type::u16: impl(uint16_t(0)); break;
      case nc::data_type::i32: impl(int32_t(0)); break;
      case nc::data_type::u32: impl(uint32_t(0)); break;
      case nc::data_type::i64: impl(int64_t(0)); break;
      case nc::data_type::u64: impl(uint64_t(0)); break;
      case nc::data_type::f32: impl(float(0)); break;
      case nc::data_type::f64: impl(double(0)); break;
      case nc::data_type::string:
        throw std::runtime_error{fmt::format("Packing used with string variable {}", var.name())};
      }
    }

  private:
    unique_ptr<char[]>  buffer_;
    size_t              size_;
  };

  template <typename T>
  auto read_valid_range(nc::variable const& var) -> pair<optional<T>, optional<T>>
  {
    auto impl = [&](auto dummy) -> pair<optional<T>, optional<T>>
    {
      using ptype = decltype(dummy);

      // return the converted valid range if given
      if (var.att_exists("valid_range"))
      {
        auto scale = var.att_get_as<T>("scale_factor", 1);
        auto offset = var.att_get_as<T>("add_offset", 0);
        auto valid_range = std::array<ptype, 2>();
        var.att_get("valid_range", span{valid_range.data(), 2});
        return { T(valid_range[0] * scale + offset), T(valid_range[1] * scale + offset) };
      }
      else if (auto has_min = var.att_exists("valid_min"), has_max = var.att_exists("valid_min"); has_min || has_max)
      {
        auto scale = var.att_get_as<T>("scale_factor", 1);
        auto offset = var.att_get_as<T>("add_offset", 0);
        return
        {
            has_min ? optional<T>{var.att_get_as<ptype>("valid_min") * scale + offset} : std::nullopt
          , has_max ? optional<T>{var.att_get_as<ptype>("valid_max") * scale + offset} : std::nullopt
        };
      }

      return { std::nullopt, std::nullopt };
    };

    switch (var.type())
    {
    case nc::data_type::i8:  return impl(int8_t(0));
    case nc::data_type::u8:  return impl(uint8_t(0));
    case nc::data_type::i16: return impl(int16_t(0));
    case nc::data_type::u16: return impl(uint16_t(0));
    case nc::data_type::i32: return impl(int32_t(0));
    case nc::data_type::u32: return impl(uint32_t(0));
    case nc::data_type::i64: return impl(int64_t(0));
    case nc::data_type::u64: return impl(uint64_t(0));
    case nc::data_type::f32: return impl(float(0));
    case nc::data_type::f64: return impl(double(0));
    case nc::data_type::string:
      throw std::runtime_error{fmt::format("Packing used with string variable {}", var.name())};
    }

    throw std::logic_error{"Unreachable"};
  }

  /// Parse the CF style timestamp string
  auto parse_time_units(string const& str) -> pair<time_unit, timestamp>;

  /// Read a scalar time from a variable
  auto read_time(nc::variable const& var, span<size_t const> start = {}) -> timestamp;

  /// Read a set of times from a variable
  auto read_times(
        nc::variable const& var
      , span<timestamp> data
      , span<size_t const> start = {}
      , span<size_t const> count = {}
      ) -> void;

  /// Determine the indexes of the x and y spatial dimensions for a variable
  /** If this function is able to location spatial dimensions, it is guaranteed that those dimensions
   *  contain coordinate variable. */
  auto find_spatial_dimensions(nc::variable const& var) -> optional<vec2z>;

  /// Determine the indexes of the x and y spatial dimensions for a variable and throw if not found
  auto lookup_spatial_dimensions(nc::variable const& var) -> vec2z;

  /// Find the coordinate variable associated with a dimension (if any)
  /** This function will ensure that the coordinate variable has the correct rank and size. */
  auto find_coordinates(nc::dimension const& dim) -> nc::variable const*;
  inline auto find_coordinates(nc::dimension& dim) -> nc::variable*
  {
    return const_cast<nc::variable*>(find_coordinates(const_cast<nc::dimension const&>(dim)));
  }

  /// Find the coordinate variable associated with a dimension and throw on failure
  auto lookup_coordinates(nc::dimension const& dim) -> nc::variable const&;
  inline auto lookup_coordinates(nc::dimension& dim) -> nc::variable&
  {
    return const_cast<nc::variable&>(lookup_coordinates(const_cast<nc::dimension const&>(dim)));
  }

  /// Copy a dimension and any associated coordinate variable
  auto copy_dimension_and_coordinates(nc::dimension const& dim, nc::group& target) -> nc::dimension&;

  template <typename Func>
  auto for_each_spatial(nc::variable const& var, vec2z ixy, Func&& func) -> void
  {
    size_t buf[nc::max_var_dims];
    auto start = make_span(buf, var.dimensions().size());
    for (decltype(start.size()) i = 0; i < start.size(); ++i)
      start[i] = (i == ixy.x || i == ixy.y) ? nc::all : size_t{0};

    while (true)
    {
      // run the functor for this image
      func(start);

      // increment to the next image
      size_t idim = start.size() - 1;
      while (true)
      {
        if (idim != ixy.x && idim != ixy.y)
        {
          ++start[idim];
          if (start[idim] < var.dimensions()[idim]->size())
            break;
          start[idim] = 0;
        }
        if (idim == 0)
          return;
        --idim;
      }
    }
  }

  /// Determine a Proj.4 projection string based on the passed variable
  auto determine_proj4_definition(nc::variable const& var) -> string;

  /// Determine a WKT projection string based on the passed variable
  /** If no WKT string is stored, then this function will return an empty string. */
  auto determine_wkt_definition(nc::variable const& var) -> string;

  /// Extract cell boundaries from a coordinate variable
  /** If the variable does not provide explicit bounds, then they are are estimated by assuming the mid-point
   *  between the cell centers that are explicitly provided by the coordinate variable itself.
   *
   *  Note that this function only supports 1D coordinate variables which represent contiguous areas.  That
   *  is, the upper edge of cell N must exactly equal the lower edge of cell N + 1.  If these conditions are
   *  not met the function will fail with an exception. */
  auto determine_cell_edges(nc::variable const& var) -> array1d;

  /// Create a dimension and coordinate variable for a contiguous dimension
  auto create_spatial_dimension(
        io::nc::group& cdf
      , string const& name
      , string const& standard_name
      , string const& units
      , array1d const& edges
      , int compression_level = 5
      ) -> io::nc::dimension&;

  /// Create a projection variable from a proj.4 string and coordinate units
  /** The proj4_definition is the primary source of truth and must be valid.  It is parsed to create the
   *  attributes for the grid mapping variable as specified by the CF Conventions.  If the WKT string
   *  corresponding to the proj4_definition is not known then an empty string should be passed.  It is up
   *  to the user to ensure that any passed WKT is consistent with the proj4_definition.*/
  auto create_grid_mapping(
        io::nc::group& cdf
      , string const& name
      , string const& proj4_definition
      , string const& wkt_definition
      , string const& units_y
      , string const& units_x
      ) -> io::nc::variable&;

  /// Determine the optimal packing parameters
  /** This will determine the optimal scale_factor and add_offset values that should be used when packing data
   *  with the given minimum and maximum representable values into the specified type.  The fill_value
   *  parameter is used to specify whether a fill value should be reserved in the packed representation.  If
   *  it is true, then the packed value of '0' will be reserved for the fill value.
   *
   *  The return value pair is {scale_factor, add_offset}. */
  auto determine_optimal_packing(double min, double max, nc::data_type type, bool fill_value) -> pair<double, double>;
}

/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "rapic.h"
#include "odim.h"
#include <bom/span.h>
#include <bom/timestamp.h>

namespace bom::io
{
  /// Write a list of rapic scans as an ODIM_H5 polar volume file
  /**
   * This function assumes the following preconditions about the scan_set:
   * - All scans are of the VOLUMETRIC product type
   * - All scans belong to the same product instance
   * - The list is sorted by pass order such that all passess associated with a tilt are grouped together
   *
   * The tilts and passess will be written out in the order of the list.  That is, the first scan will be
   * written to the ODIM group dataset1/data1.
   *
   * A custom function may be provided in the third argument to provide a mechanism for reporting warnings
   * during the conversion process.
   */
  auto rapic_to_odim(string const& path, span<rapic::scan const*> scan_set) -> timestamp;
}

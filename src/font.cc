/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "font.h"
#include <bom/file_utils.h>
#include <bom/trace.h>
#include <mutex>

#include <ft2build.h>
#include FT_FREETYPE_H

using namespace bom;

BOM_DEFINE_ENUM_TRAITS(font::alignment);

static_assert(sizeof(FT_Face) == sizeof(void*), "size of FT_Face should match void*");

using ft_library_handle = unique_hnd<FT_Library, functor<FT_Done_FreeType>, nullptr>;

/* for simplicity we just maintain a global instance of freetype and ensure that it is only accessed by a single
 * thread at a time.  we only really the library the first time we need to render each character so the pain of
 * managing multiple instances to allow simultaneous character loads from multiple threads is just not worth it. */
static std::mutex         mut_ft_library_;
static ft_library_handle  ft_library_;

// this function assumes you already own mut_ft_library_
static auto ft_init_library() -> void
{
  if (!ft_library_)
  {
    FT_Library hnd = nullptr;
    if (auto err = FT_Init_FreeType(&hnd))
      throw std::runtime_error{fmt::format("FT_Init_FreeType failed: {}", err)};
    ft_library_.reset(hnd);
  }
}

/* TODO - we have to ensure no other fonts are alive if we want to keep this function! */
auto font::cleanup_loading_resources() -> void
{
  std::lock_guard<std::mutex> lock(mut_ft_library_);
  ft_library_.release();
}

font::font(string const& path, int size)
  : path_(path)
  , size_{size}
  , face_{nullptr}
  , line_spacing_{size_}
{
  std::lock_guard<std::mutex> lock{mut_ft_library_};

  // ensure the library is loaded
  ft_init_library();

  // acquire the face
  {
    FT_Face hnd = nullptr;
    if (auto err = FT_New_Face(ft_library_, path.c_str(), 0, &hnd))
      throw std::runtime_error{fmt::format("failed to initialize font {} error {}", path, err)};
    face_ = hnd;
  }

  /* it would be nice to use raii to avoid the explicit cleanup for the face, but we need to lock the library
   * during FT_Done_Face and it would be easy to forget that using raii.  it's less confusing to be explicit
   * in this case. */
  try
  {
    // setup pixel size
    if (auto err = FT_Set_Pixel_Sizes(static_cast<FT_Face>(face_), 0, size_))
      throw std::runtime_error{fmt::format("failed to select size {} from font {}, error {}", size_, path_, err)};

    // get the default line spacing
    if (FT_IS_SCALABLE(static_cast<FT_Face>(face_)))
      line_spacing_ = static_cast<FT_Face>(face_)->size->metrics.height >> 6;
    else
      trace::warning("font {} is not scalable. unable to determine appropriate line spacing", path_);
  }
  catch (...)
  {
    FT_Done_Face(static_cast<FT_Face>(face_));
    throw;
  }
}

font::font(font&& rhs) noexcept
  : path_(std::move(rhs.path_))
  , size_{rhs.size_}
  , face_{rhs.face_}
  , line_spacing_{rhs.line_spacing_}
  , glyphs_{std::move(rhs.glyphs_)}
{
  rhs.face_ = nullptr;
}

auto font::operator=(font&& rhs) noexcept -> font&
{
  if (face_)
  {
    std::lock_guard<std::mutex> lock{mut_ft_library_};
    FT_Done_Face(static_cast<FT_Face>(face_));
  }

  path_ = std::move(rhs.path_);
  size_ = rhs.size_;
  face_ = rhs.face_;
  line_spacing_ = rhs.line_spacing_;
  glyphs_ = std::move(rhs.glyphs_);

  rhs.face_ = nullptr;

  return *this;
}

font::~font()
{
  if (face_)
  {
    std::lock_guard<std::mutex> lock{mut_ft_library_};
    FT_Done_Face(static_cast<FT_Face>(face_));
  }
}

auto font::render(
      vec2i position
    , alignment align
    , vertical_alignment valign
    , colour foreground
    , string const& text
    , image& img
    ) const -> box2i
{
  struct layout
  {
    glyph_data const* glyph;
    vec2i             offset;
  };

  // reserve some planning space
  vector<layout> plan;
  plan.reserve(text.size());

  // collect up our sequence of characters to render
  // laying out as if we are drawing left aligned
  auto width = 0;
  auto line_start = size_t(0);
  auto pen = vec2i{0, 0};
  auto utf8 = text.c_str();
  while (auto utf32 = utf8_next_character(utf8))
  {
    if (utf32 == '\n')
    {
      // adjust previous line for horizontal alignment
      if (align != alignment::left)
      {
        auto delta = pen.x * (align == alignment::centre ? -0.5f : -1.0f);
        for (auto i = line_start; i < plan.size(); ++i)
          plan[i].offset.x += delta;
      }

      // setup for next line
      width = std::max(width, pen.x);
      line_start = plan.size();
      pen.x = 0;
      pen.y += line_spacing_;
    }
    else
    {
      auto& glyph = acquire_glyph(utf32);

      plan.push_back(layout{&glyph, pen});
      pen.x += glyph.advance;
    }
  }

  // adjust previous line for horizontal alignment
  if (align != alignment::left)
  {
    auto delta = pen.x * (align == alignment::centre ? -0.5f : -1.0f);
    for (auto i = line_start; i < plan.size(); ++i)
      plan[i].offset.x += delta;
  }

  width = std::max(width, pen.x);
  auto height = pen.y;

  // adjust for vertical position
  if (valign != vertical_alignment::top)
    position.y -= height * (valign == vertical_alignment::middle ? 0.5 : 1.0);

  // copy our glyphs onto the final image
  auto bbox = box2i::empty_box();
  for (auto& ch : plan)
  {
    auto tl = position + ch.offset + ch.glyph->offset;

    bbox.expand(tl);
    bbox.expand(tl + ch.glyph->data.extents());

    // TODO - make this better!
    for (size_t y = 0; y < ch.glyph->data.extents().y; ++y)
    {
      auto oy = tl.y + int(y);
      for (size_t x = 0; x < ch.glyph->data.extents().x; ++x)
      {
        auto ox = tl.x + int(x);
        if (oy >= 0 && oy < img.extents().y && ox >= 0 && ox < img.extents().x)
        {
          img[oy][ox] = colour::lerp_rgb(img[oy][ox], foreground, ch.glyph->data[y][x]);
        }
      }
    }
  }

  return bbox;
}

auto font::acquire_glyph(char32_t utf32) const -> glyph_data const&
{
  std::lock_guard<std::mutex> lock{mut_glyphs_};

  // see if we already have the glyph loaded
  auto i = glyphs_.find(utf32);
  if (i != glyphs_.end())
    return i->second;

  std::lock_guard<std::mutex> lock2{mut_ft_library_};

  // load the glyph from the font file and render it as a bitmap internally
  auto face = static_cast<FT_Face>(face_);
  if (auto err = FT_Load_Char(face, utf32, FT_LOAD_RENDER))
    throw std::runtime_error{fmt::format("Failed to load font glyph:\n  font: {}\n  glyph: {}\n  err: {}", path_, (uint32_t) utf32, err)};

  auto& glyph = glyphs_[utf32];

  // note the relevant metadata
  auto slot = face->glyph;
  glyph.offset = vec2i{slot->bitmap_left, -slot->bitmap_top};
  glyph.advance = slot->advance.x >> 6;

  // copy the bitmap into our array of blend weights
  auto& bitmap = slot->bitmap;
  glyph.data.resize(vec2z{bitmap.width, bitmap.rows});
  for (size_t y = 0; y < glyph.data.extents().y; ++y)
    for (size_t x = 0; x < glyph.data.extents().x; ++x)
      glyph.data[y][x] = static_cast<float>(bitmap.buffer[y * bitmap.pitch + x]) / (bitmap.num_grays - 1);

  return glyph;
}

/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <bom/raii.h>
#include <bom/span.h>
#include <bom/string_utils.h>
#include <bom/vec2.h>
#include <bom/vec3.h>
#include <mutex>

namespace bom
{
  /// Map projection used to tranform between various coordinate systems
  class map_projection
  {
  public:
    class error;
    class transform_context;

    /// Handle for a context
    /** To ensure thread safe use of the projection class in a multi-threaded environment each projection instance
     *  must be created using a context that is unique to the thread in which the projection will be used. */
    using context_hnd = shared_ptr<transform_context>;

  public:
    /// Return the underlying proj.4 version number
    static auto proj_version() -> int;

    /// Create a new context
    /** Context handles are reference counted.  The context returned by this function will be automatically destroyed
     *  once all copies of the handle and all projections using it are destoryed. */
    static auto create_context() -> context_hnd;

    /// Default context for use in single threaded application
    /** This context may also be used in multi-threaded applications provided that it is only ever used from a single
     *  thread (such as the main application thread). */
    static constexpr auto default_context = nullptr;

    /// Create a lat/lon projection based on WGS84
    static auto make_wgs84_projection(context_hnd context) -> map_projection;

  public:
    /// Initialize a new projection
    /** The definition string is expected to be a valid Proj.4 projection definition.  The WKT string is currently
     *  optional and exists only as metadata.  In the future we may desire to allow true initialization of a projection
     *  based entirey on its WKT.  */
    map_projection(context_hnd context, string definition, string wkt = string{});

    /// Copy an existing projection
    map_projection(map_projection const& rhs);

    /// Move construct a projection
    map_projection(map_projection&& rhs) noexcept;

    /// Copy assign projection
    auto operator=(map_projection const& rhs) -> map_projection&;

    /// Move assign projection
    auto operator=(map_projection&& rhs) noexcept -> map_projection&;

    /// Destroy projection
    ~map_projection();

    /// Get the context used by this projection
    auto context() const { return context_; }

    /// Get the proj definition string for this projection
    auto& definition() const { return definition_; }

    /// Get the WKT definition string for this projection (if known)
    auto& wkt() const { return wkt_; }

    /// Get the units expected for the projection coordinates
    /** Note that the units string here is expressed in the UDUNITS2 naming schema rather than the original Proj.4
     *  unit names supplied within the definition. */
    auto& units() const { return units_; }

  public:
    /// Transform 2d coordinates from one projection system to another
    static auto transform(map_projection const& from, map_projection const& to, span<double> x, span<double> y) -> void;

    /// Transform 3d coordinates from one projection system to another
    static auto transform(map_projection const& from, map_projection const& to, span<double> x, span<double> y, span<double> z) -> void;

    /// Transform 2d coordinates from one projection system to another
    static auto transform(map_projection const& from, map_projection const& to, span<vec2d> xy) -> void;

    /// Transform 3d coordinates from one projection system to another
    static auto transform(map_projection const& from, map_projection const& to, span<vec3d> xyz) -> void;

  private:
    context_hnd context_;
    string      definition_;
    string      wkt_;
    string      units_;
    void*       handle_;
  };

  /// Error thrown to indicate projection failures
  class map_projection::error : public std::runtime_error
  {
  public:
    /// Construct an error for a projection involving a single projection
    error(string definition, context_hnd context);

    /// Construct an error for a projection involving a two projections
    error(string definition_from, string definition_to, context_hnd context);

    // only needed for legacy api
    error(string definition_from, string definition_to, int err);
  };
}

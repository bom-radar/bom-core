/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <bom/box2.h>
#include <bom/image.h>
#include <bom/raii.h>
#include <bom/string_utils.h>
#include <bom/thread_utils.h>
#include <bom/utf.h>
#include <bom/vec2.h>
#include <unordered_map>

namespace bom
{
  /// Font used to render text
  class font
  {
  public:
    /// Text alignment styles
    enum class alignment
    {
        left
      , centre
      , right
    };

    /// Vertical text alignment styles
    enum class vertical_alignment
    {
        top
      , middle
      , bottom
    };

    /// Release font resources that are only needed while loading fonts
    /** To construct a font object the FreeType2 library is used.  This library is initialized automatically the first
     *  time a font is constructed, but is not release automatically.  If you know that you have finished loading all
     *  the fonts needed by the application then you can call this function to cleanup the underlying FreeType2 library.
     *
     *  Is is always safe to call this function.  If a new font is subsequently constructed then the FreeType2 library
     *  will be initialized again (requiring another call to this function to clean up).  If this function has been
     *  called an existing font needs to load font data (because a character outside the charset is requested), then
     *  FreeType2 will be initialized and then uninitialized once the missing data is loaded. */
    static auto cleanup_loading_resources() -> void;

  public:
    /// Construct a font from a font file
    /** If a relative path is provided it will be interpreted relative to the 'fonts' sub-directory of the static
     *  data area.
     *
     *  The resolution parameter determines the desired size in texels of the glyphs which will be written into the font
     *  texture.  This size corresponds to the EM size of the font, so individual glyphs may be smaller or larger
     *  than the size given.  Also there is a border applied to every glyph which is required by the font shader
     *  for effects such as bold and glow.  This border is added on top of the resolution parameter given here.
     *
     *  The border parameter specifies the size of the border region around the glyph when it is packed into the
     *  texture.  The size of this border sets the limit for the size of glow and weight effects that can be rendered
     *  by the font shader.  The larger the border is, the larger the effect may be.  Leaving this parameter to the
     *  default of -1 will cause a default border of resolution / 3 to be used.
     *
     *  The initial_charset parameter, if provided, must point to a UTF8 encoded string which contains exactly one
     *  character corresponding to each character that is expected to be rendered by the font.  If this argument is
     *  not supplied then the default character set will be used.
     *
     *  If a font is requested to render a glyph which it does not have loaded into its character set it will attempt
     *  to load the character into the font before rendering.  This may carry a heavy runtime penalty since the
     *  entire font texture may need to be regenerated.  You should rely on this facility only as a fallback in the
     *  case of unexpected input text.
     *
     *  If a requested character is not available as a glyph in the font then a warning will be traced and a default
     *  glyph will be substituded any time that character is requested for layout. */
    font(string const& path, int size);

    font(font const& rhs) = delete;
    font(font&& rhs) noexcept;
    auto operator=(font const& rhs) -> font& = delete;
    auto operator=(font&& rhs) noexcept -> font&;
    ~font();

    /// Get the path of the font file
    auto path() const -> string const&
    {
      return path_;
    }

    /// Get the optimal spacing between lines
    /** The value returned here represents the best guess of how much to increment the y coordinate of the anchor
     *  vector between lines when laying out multiple lines of text. */
    auto line_spacing() const -> int
    {
      return line_spacing_;
    }

    /// Render text onto an image
    /** The return value of the function is a bounding box indicating the bounds of the rendered text. */
    auto render(
          vec2i position
        , alignment align           ///< Text alignment style
        , vertical_alignment valign
        , colour foreground
        , string const& text        ///< UTF8 encoded text to layout
        , image& img
        ) const -> box2i;

  private:
    struct glyph_data
    {
      array2f data;
      vec2i   offset;
      float   advance;
    };
    using glyph_store = std::unordered_map<char32_t, glyph_data>;

  private:
    auto acquire_glyph(char32_t utf32) const -> glyph_data const&;

  private:
    string              path_;
    int                 size_;
    void*               face_;          // we use void* and cast so we don't impose freetype headers on our user
    int                 line_spacing_;  // default line spacing
    mutable std::mutex  mut_glyphs_;
    mutable glyph_store glyphs_;        // protected by above
  };

#if 0
  template <typename V>
  auto font::layout(
        context& ctx
      , vec2f anchor
      , vec2f offset
      , alignment align
      , float size
      , string const& text
      , std::vector<V>& vertices
      ) -> box2f
  {
    auto scale = size / resolution_;

    auto first = vertices.size();
    auto line_start = first;
    auto pen = vec2f{0,0};
    box2f bbox = box2f::empty_box;

    // loop through each character
    auto utf8 = text.c_str();
    while (auto utf32 = utf8_next_character(utf8))
    {
      // utf32 linefeed == utf8 linefeed == ascii linefeed == 0x0a
      if (utf32 == '\n')
      {
        // adjust for horizontal alignment
        if (align != alignment::left)
        {
          auto delta = pen.x * (align == alignment::centre ? -0.5f : -1.0f);
          for (auto i = line_start; i < vertices.size(); ++i)
            vertices[i].metrics.x += delta;
        }

        // setup for next line
        line_start = vertices.size();
        pen.x = 0.0f;
        pen.y -= line_spacing_ * scale;
      }
      else
      {
        auto& glyph = glyphs_[utf32];

        // if the glyph is not in the character set load it dynamically
        if (!glyph.loaded)
          load_glyph(ctx, utf32, glyph);

        // allocate six vertices for our two triangles for this glyph
        vertices.emplace_back();
        auto& v = vertices.back();

        // scale the metrics to achieve the desired pixel width of the em square
        v.metrics.x = pen.x + glyph.offset.x * scale;
        v.metrics.y = pen.y + glyph.offset.y * scale;
        v.metrics.z = glyph.size.x * scale;
        v.metrics.w = glyph.size.y * scale;
        v.uv0 = glyph.texcoords;
        pen.x += glyph.advance * scale;
      }
    }

    // adjust for horizontal alignment
    if (align != alignment::left)
    {
      auto delta = pen.x * (align == alignment::centre ? -0.5f : -1.0f);
      for (auto i = line_start; i < vertices.size(); ++i)
        vertices[i].metrics.x += delta;
    }

    // determine the bounding box
    // this can't easily be done without the second pass due to glyph borders and baseline offset issues (trust me)
    bbox = box2f::empty_box;
    for (size_t i = first; i != vertices.size(); ++i)
    {
      bbox.expand(vec2f{vertices[i].metrics.x, vertices[i].metrics.y});
      bbox.expand(vec2f{vertices[i].metrics.x + vertices[i].metrics.z, vertices[i].metrics.y - vertices[i].metrics.w});
    }

    // determine appropriate adjustment for anchor position
    /* extra min.x and max.y to adjust for glyph border region and baseline offset in glyph coordinates
     * may want to manually disable this when laying out multiple baseline aligned text elements such as GUI controls */
    offset -= anchor.scale(bbox.max - bbox.min) + bbox.min;

    // adjust for the anchor and offset
    for (size_t i = first; i < vertices.size(); ++i)
    {
      vertices[i].metrics.x += offset.x;
      vertices[i].metrics.y += offset.y;
    }
    bbox.min += offset;
    bbox.max += offset;

    return bbox;
  }
#endif

  BOM_DECLARE_ENUM_TRAITS(font::alignment, left, centre, right);
}
